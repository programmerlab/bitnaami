 === Pisces ===
Contributors: LA Studio
Requires at least: WordPress 4.8
Tested up to: WordPress 4.8
Version: 2.0.2
Tags: one-column, two-columns, three-columns, left-sidebar, right-sidebar,  custom-background, custom-header, custom-menu, featured-images, flexible-header, full-width-template, post-formats, sticky-post, theme-options, translation-ready

== Description ==

Pisces - Multi Concept Creative Theme

* Mobile-first, Responsive Layout
* Custom Colors
* Custom Header
* Social Links
* Mega Menu
* Post Formats

== Installation ==

1. Navigate to Appearance → Themes in your WordPress admin dashboard.
2. Click the Add New button at the top of the page then go for the Theme Upload option.
3. For the file upload, pick Theme Files / pisces.zip in the theme package downloaded from ThemeForest and click Install Now.
4. Click Activate once the upload has finished.

== Copyright ==

Copyright 2015-2017 La-StudioWeb.com

== Changelog ==


------------ Version 2.0.2 Release [October 17, 2017]  ------------
#Fixed display problem on IE11
#Fixed icon text gradient display incorrect on IE11
#Fixed LA Maps shortcode

The list files has changed
    wp-content/themes/pisces/assets/js/app.js
    wp-content/themes/pisces/assets/js/min/app.js
    wp-content/themes/pisces/assets/js/enqueue/min/modernizr-custom.js
    wp-content/themes/pisces/style.css
    wp-content/themes/pisces/plugins/plugins.php
    wp-content/themes/pisces/plugins/lastudio-core.zip


------------ Version 2.0.1 Release [October 16, 2017]  ------------
#Tweak RTL language version
* Compatibility with WooCommerce 3.2.1
^ Update version of Visual Composer, LaStudio Core ...
^ Update language file

------------ Version 2.0 Release [October 13, 2017]  ------------
* Release

Initial release