(function($) {
    "use strict";

    $(function(){

        if($('.search-form .sf-fields select.postform').length){
            var $selectCat = $('.search-form .sf-fields select.postform');
            $('<select id="width_tmp_select"><option id="width_tmp_option">'+ $('option:selected', $selectCat).text() +'</option></select>').insertAfter($selectCat);
            $selectCat.width($('#width_tmp_select').width());
            $selectCat.change(function(){
                $('#width_tmp_option').html($('option:selected', $(this)).text());
                $(this).width($('#width_tmp_select').width());
            });
        }

    });
    
})(jQuery);