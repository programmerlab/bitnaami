#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-08-28 08:39+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: LASTUDIO <info@la-studioweb.com>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco - https://localise.biz/"

#. Name of the theme
msgid "Pisces Child"
msgstr ""

#. Description of the theme
msgid "Pisces - Multi Concept Creative Theme"
msgstr ""

#. URI of the theme
msgid "http://themeforest.net/user/LA-Studio/portfolio?ref=LA-Studio"
msgstr ""

#. Author of the theme
msgid "LA Studio"
msgstr ""

#. Author URI of the theme
msgid "http://themeforest.net/user/LA-Studio?ref=LA-Studio"
msgstr ""
