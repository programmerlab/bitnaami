<?php
/**
 * Loop Add to Cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/add-to-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     3.3.0
 */

if (!defined('ABSPATH')) {
    exit;
}


global $acs_cart_view, $product;
if (is_product_category() || is_shop()) {
    if(!is_array($acs_cart_view)){
        $acs_cart_view = array();
    }
    if (in_array($product->get_id(), $acs_cart_view)) {
        foreach (WC()->cart->get_cart() as $key => $val) {
            $_product = $val['data'];
            if ($product->get_id() == $_product->get_id()) {
                $_product_qty = $val['quantity'];
                $product_quantity = woocommerce_quantity_input(array(
                    'input_name' => "cart[$key][qty]",
                    'input_value' => $_product_qty,
                    'max_value' => $product->backorders_allowed() ? '' : $product->get_stock_quantity(),
                    'min_value' => '0'
                ), $product, false); ?>
                <div class="shop-cust-qty" data-item-key="<?php echo $_product->get_id(); ?>">
                    <?php
                    echo apply_filters('woocommerce_cart_item_quantity', $product_quantity, $key, $_product);
                    ?>
                </div>
                <?php
                //echo '<div class="quantity"><span class="qty-minus">-</span><input class="input-text qty text" step="1" min="1" max="140" name="quantity" value='.$_product_qty.' title="Qty" size="4" pattern="[0-9]*" inputmode="numeric" type="number"><span class="qty-plus">+</span></div>';
            }
        }
    }
    $acs_cart_view[] = $product->get_id();
}


echo apply_filters(
    'woocommerce_loop_add_to_cart_link',
    sprintf('<a rel="nofollow" href="%s" title="%s" data-quantity="%s" data-product_title="%s" data-product_id="%s" data-product_sku="%s" class="%s"><span>%s</span></a>',
        esc_url($product->add_to_cart_url()),
        esc_attr($product->add_to_cart_text()),
        esc_attr(isset($quantity) ? $quantity : 1),
        esc_attr($product->get_title()),
        esc_attr($product->get_id()),
        esc_attr($product->get_sku()),
        esc_attr(isset($class) ? $class : 'button'),
        esc_html($product->add_to_cart_text())
    ),
    $product
);
