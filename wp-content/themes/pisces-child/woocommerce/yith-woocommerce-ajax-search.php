<?php
/**
 * YITH WooCommerce Ajax Search template
 *
 * @author  Yithemes
 * @package YITH WooCommerce Ajax Search Premium
 * @version 1.2.3
 */

if ( ! defined( 'YITH_WCAS' ) ) {
	exit;
} // Exit if accessed directly


wp_enqueue_script( 'yith_wcas_frontend' );

$research_post_type = ( get_option( 'yith_wcas_default_research' ) ) ? get_option( 'yith_wcas_default_research' ) : 'product';
?>
<div class="yith-ajaxsearchform-container">
	<form role="search" method="get" class="search-form" id="yith-ajaxsearchform" action="<?php echo esc_url( home_url( '/' ) ) ?>">
		<div class="yith-ajaxsearchform-container">
			<div class="yith-ajaxsearchform-select">
				<?php
				if ( get_option( 'yith_wcas_show_search_list' ) == 'yes' ):
					$selected_search = ( isset( $_REQUEST['post_type'] ) ) ? $_REQUEST['post_type'] : $research_post_type; ?>
					<select class="yit_wcas_post_type selectbox" id="yit_wcas_post_type" name="post_type">
						<option value="product" <?php selected( 'product', $selected_search ) ?>><?php _e( 'Products', 'yith-woocommerce-ajax-search' ) ?></option>
						<option value="any" <?php selected( 'any', $selected_search ) ?>><?php _e( 'All', 'yith-woocommerce-ajax-search' ) ?></option>
					</select>
				<?php else: ?>
					<input type="hidden" name="post_type" class="yit_wcas_post_type" id="yit_wcas_post_type" value="<?php echo $research_post_type ?>" />
				<?php endif; ?>
			</div>
			<div class="sf-fields">
				<div class="sf-field sf-field-select"><?php
					$selected_category = ( isset( $_REQUEST['product_cat'] ) ) ? $_REQUEST['product_cat'] : '';
					wp_dropdown_categories(array(
						'show_option_all'   => esc_attr_x('All Items', 'front-view', 'pisces'),
						'hierarchical'      => true,
						'value_field'       => 'slug',
						'name'				=> 'product_cat',
						'taxonomy'			=> 'product_cat',
						'id'				=> 'product_cat',
						'class'				=> 'search_categories selectbox postform',
						'selected'			=> $selected_category
				)); ?></div>
				<div class="sf-field sf-field-input search-navigation">
					<label class="screen-reader-text" for="yith-s"><?php _e( 'Search for:', 'yith-woocommerce-ajax-search' ) ?></label>
					<input type="search"
						   value="<?php echo get_search_query() ?>"
						   name="s"
						   id="yith-s"
						   class="search-field yith-s"
						   placeholder="<?php echo get_option( 'yith_wcas_search_input_label' ) ?>"
						   data-append-to=".search-navigation"
						   data-loader-icon="<?php echo str_replace( '"', '', apply_filters( 'yith_wcas_ajax_search_icon', '' ) ) ?>"
						   data-min-chars="<?php echo get_option( 'yith_wcas_min_chars' ); ?>" />
				</div>
				<button class="search-button" type="submit"><i class="fa fa-search"></i></button>
			</div>
			<?php if ( defined( 'ICL_LANGUAGE_CODE' ) ) : ?>
				<input type="hidden" name="lang" value="<?php echo ICL_LANGUAGE_CODE ?>" />
			<?php endif ?>
		</div>
	</form>
</div>