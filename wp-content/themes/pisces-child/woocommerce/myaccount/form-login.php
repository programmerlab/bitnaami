<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php wc_print_notices(); ?>

<?php do_action( 'woocommerce_before_customer_login_form' ); ?>
<div class="la-myaccount-page">
	<div class="la-tabs" id="la_tabs_customer_login">
		<div id="la_tab--login" class="la-tab-panel active">
			<div class="la_tab--actions">
				<h3>Have an Account?</h3>
				<div class="form-row">
					<a href="#"><?php echo esc_html_x('Login', 'front-view', 'pisces') ?></a>
				</div>
			</div>
			<div class="la_tab--form">
				<h3>Welcome Back!</h3>
				<form class="woocomerce-form woocommerce-form-login login" method="post">

					<?php do_action( 'woocommerce_login_form_start' ); ?>

					<div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
						<label class="hidden" for="username"><?php echo esc_html_x( 'Username or email address', 'front-view', 'pisces' ); ?> <span class="required">*</span></label>
						<input placeholder="<?php echo esc_html_x( 'Email Address', 'front-view', 'pisces' ); ?>" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
					</div>
					<div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
						<label class="hidden" for="password"><?php echo esc_html_x( 'Password', 'front-view', 'pisces' ); ?> <span class="required">*</span></label>
						<input placeholder="<?php echo esc_html_x( 'Password', 'front-view', 'pisces' ); ?>" class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" />
						<p class="la-checkbox">
							<input type="checkbox" id="showpwdf" />
							<label for="showpwdf" class="inline"><span>Show</span></label>
						</p>
					</div>

					<?php do_action( 'woocommerce_login_form' ); ?>

					<div class="form-row">
						<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
						<input type="submit" class="woocommerce-Button button" name="login" value="<?php echo esc_attr_x( 'Login', 'front-view', 'pisces' ); ?>" />
					</div>
					<div class="form-row la-checkbox">
						<input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" />
						<label for="rememberme" class="woocommerce-form__label woocommerce-form__label-for-checkbox inline"><span><?php echo esc_html_x( 'Remember me', 'front-view', 'pisces' ); ?></span></label>
						<div class="woocommerce-LostPassword lost_password">
							<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php echo esc_html_x( 'Lost your password?', 'front-view', 'pisces' ); ?></a>
						</div>
					</div>
					<?php do_action( 'woocommerce_login_form_end' ); ?>
				</form>
			</div>
		</div>
	<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

		<div id="la_tab--register" class="la-tab-panel">

			<div class="la_tab--actions">
				<h3>New to BuyEasy?</h3>
				<p class="form-row">
					<a href="#"><?php echo esc_html_x('Create Account', 'front-view', 'pisces') ?></a>
				</p>
			</div>

			<div class="la_tab--form">
				<h3>Create Account</h3>

				<form method="post" class="register">

					<?php do_action( 'woocommerce_register_form_start' ); ?>

					<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

						<div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
							<label class="hidden" for="reg_username"><?php echo esc_html_x( 'Username', 'front-view', 'pisces' ); ?> <span class="required">*</span></label>
							<input placeholder="<?php echo esc_html_x( 'Username', 'front-view', 'pisces' ); ?>" type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="reg_username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
						</div>

					<?php endif; ?>

					<div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
						<label class="hidden" for="reg_email"><?php echo esc_html_x( 'Email address', 'front-view', 'pisces' ); ?> <span class="required">*</span></label>
						<input placeholder="<?php echo esc_html_x( 'Email Address', 'front-view', 'pisces' ); ?>" type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>" />
					</div>

					<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

						<div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
							<label class="hidden" for="reg_password"><?php echo esc_html_x( 'Password', 'front-view', 'pisces' ); ?> <span class="required">*</span></label>
							<input placeholder="<?php echo esc_html_x( 'Password', 'front-view', 'pisces' ); ?>" type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password" id="reg_password" />
							<p class="la-checkbox">
								<input type="checkbox" id="showpwdfs" />
								<label for="showpwdfs" class="inline"><span>Show</span></label>
							</p>
						</div>

					<?php endif; ?>

					<?php do_action( 'woocommerce_register_form' ); ?>

					<div class="woocomerce-FormRow form-row">
						<?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
						<input type="submit" class="woocommerce-Button button" name="register" value="<?php echo esc_attr_x( 'Create Account & Continue', 'front-view', 'pisces' ); ?>" />
					</div>

					<?php do_action( 'woocommerce_register_form_end' ); ?>

				</form>

			</div>
		</div>
	<?php endif; ?>
	</div>
</div>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
