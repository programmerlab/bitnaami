<?php
/**
 * Pisces Theme Function
 *
 */

add_action( 'after_setup_theme', 'pisces_child_theme_setup' );
add_action( 'wp_enqueue_scripts', 'pisces_child_enqueue_styles', 20);

if( !function_exists('pisces_child_enqueue_styles') ) {
    function pisces_child_enqueue_styles() {
        wp_enqueue_style( 'pisces-child-style',
            get_stylesheet_directory_uri() . '/style.css',
            array( 'pisces-theme' ),
            wp_get_theme()->get('Version')
        );
wp_enqueue_script( 'pisces-child-app', get_stylesheet_directory_uri() . '/custom.js', array('jquery'), '', true);
    }
}

if( !function_exists('pisces_child_theme_setup') ) {
    function pisces_child_theme_setup() {
        load_child_theme_textdomain( 'pisces-child', get_stylesheet_directory() . '/languages' );
    }
}


$saveforlater = YITH_WC_Save_For_Later_Premium::get_instance();

if( !is_null( $saveforlater ) ) {

 remove_action( 'woocommerce_single_product_summary', array( $saveforlater, 'show_single_product_page' ), 35 );
 add_action( 'woocommerce_single_product_summary', array( $saveforlater, 'show_single_product_page' ), 55 ) ;
}

add_action('wp_head',function(){
?> 
<script>
    jQuery(function($){
        var cnt = 0
        $("#ywdpd-table-discounts td").each(function(i,t){
            $(this).on('click',function(){
                cnt = i;
                if(cnt>=6){
                    cnt = cnt - 6
                }
                console.log(cnt);
                var qty = $("#ywdpd-table-discounts tr:eq(0)").find("td:eq("+cnt+")").data('qtymin');
                var price = $("#ywdpd-table-discounts tr:eq(1)").find("td:eq("+cnt+")").html();
                console.log(qty);
                console.log(price);
                $('input[name=quantity]').val(qty);
                jQuery(".single-price-wrapper .price .ywcrbp_regular_price").html(price);
                jQuery(".single-price-wrapper .price .ywcrbp_your_price").html(price);
//                console.log('hi');
            })
        })
    })
</script>
<?php
});