<?php
/**
 * Pisces Theme Function
 *
 */

add_action('after_setup_theme', 'pisces_child_theme_setup');
add_action('wp_enqueue_scripts', 'pisces_child_enqueue_styles', 20);

if (!function_exists('pisces_child_enqueue_styles')) {
    function pisces_child_enqueue_styles() {
        $js_require = array('jquery','pisces-modernizr-custom');
        if(apply_filters('pisces/filter/force_enqueue_js_external', true)){
            $js_require[] = 'pisces-plugins';
        }
        wp_enqueue_script('pisces-theme', get_stylesheet_directory_uri() . '/assets/js/app.js', $js_require, null, true);
        wp_enqueue_style('pisces-child-style',
            get_stylesheet_directory_uri() . '/style.css',
            array('pisces-theme'),
            wp_get_theme()->get('Version')
        );
        wp_enqueue_script('pisces-child-app', get_stylesheet_directory_uri() . '/assets/js/custom.js', array('pisces-theme'), '', true);
        wp_localize_script('pisces-child-app', 'ajax_object',
            array('ajax_url' => admin_url('admin-ajax.php')));

    }
}

if (!function_exists('pisces_child_theme_setup')) {
    function pisces_child_theme_setup() {
        load_child_theme_textdomain('pisces-child', get_stylesheet_directory() . '/languages');
    }
}

if(class_exists('YITH_WC_Save_For_Later_Premium')){
    $saveforlater = YITH_WC_Save_For_Later_Premium::get_instance();
    if (!is_null($saveforlater)) {
        remove_action('woocommerce_single_product_summary', array($saveforlater, 'show_single_product_page'), 35);
        add_action('woocommerce_single_product_summary', array($saveforlater, 'show_single_product_page'), 55);
    }
}


function pisces_child_allow_update_mini_cart(){
    $data = array();
    $cart_item_key = $_POST['cart_key'];
    $new_amount = $_POST['product_num'];
//    $product_cart_id = WC()->cart->generate_cart_id( $cart_item_key );
//    $in_cart = WC()->cart->find_product_in_cart( $product_cart_id );
    foreach (WC()->cart->get_cart() as $key => $val) {
        $_product = $val['data'];
        if ($cart_item_key == $_product->get_id()) {
            $cart_item_key = $key;
        }
    }

    WC()->cart->set_quantity($cart_item_key, $new_amount);

    if (!defined('WOOCOMMERCE_CART')) {
        define('WOOCOMMERCE_CART', true);
    }
    if ($new_amount == 0) {
        $cart = WC()->session->get('cart', null);
        unset($cart[$cart_item_key]);
        WC()->session->set('cart', $cart);
    }
    ob_start();
    //var_dump($woocommerce->cart->get_cart());die;
    woocommerce_mini_cart();
    $data['template'] = ob_get_clean();
    $data['remove'] = $new_amount;
    $data['id'] = $cart_item_key;
    $data['total'] = WC()->cart->get_cart_contents_count();
    echo json_encode($data);
    die();
}


add_action('wp_ajax_pisces_update_mini_cart', 'pisces_child_allow_update_mini_cart');
add_action('wp_ajax_nopriv_pisces_update_mini_cart', 'pisces_child_allow_update_mini_cart');

add_filter('woocommerce_product_add_to_cart_text',function( $text, $product ){
	if( !$product->is_in_stock() ) {
		$text = 'SOLD OUT';
	}
	return $text;
}, 10, 2);