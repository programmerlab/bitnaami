(function ($) {
    "use strict";

    $.expr[':'].SearchContains = function(a, i, m) {
        return $(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
    };

    function unwrap_widgetbox(){
        $('.sidebar-inner .widget.widget_yith_wcbr_brands_list').each(function(){
            var $parent = $(this);
            if($parent.find('.search-brands').length == 0){
                $parent.prepend('<div class="search-brands"><input type="text" placeholder="Search by brand..."><i class="fa fa-search"></i><i class="pisces-icon-simple-remove"></i></div>');
            }
        })
        $('.sidebar-inner .widget .wb-toggle .ywcca_widget_title').each(function(){
            var $this = $(this);
            $this.removeClass('ywcca_widget_title').addClass('widget-title');
            $this.html('<span>'+$this.text()+'</span><a href="#" class="toggle-wt"></a>');
        });
        $('.sidebar-inner .widget .wb-toggle .widget-title').each(function(){
            $(this).prependTo($(this).closest('.widget'));
        });
    }

    $(function () {
        if ($('.search-form .sf-fields select.postform').length) {
            var $selectCat = $('.search-form .sf-fields select.postform');
            $('<select id="width_tmp_select"><option id="width_tmp_option">' + $('option:selected', $selectCat).text() + '</option></select>').insertAfter($selectCat);
            $selectCat.width($('#width_tmp_select').width());
            $selectCat.change(function () {
                $('#width_tmp_option').html($('option:selected', $(this)).text());
                $(this).width($('#width_tmp_select').width());
            });
        }
        unwrap_widgetbox();
        $(document).on('click', '.sidebar-inner .widget .widget-title .toggle-wt', function(e){
            e.preventDefault();
            var $widget = $(this).closest('.widget');
            if($widget.hasClass('close')){
                $widget.removeClass('close');
                $widget.find('.wb-toggle').slideDown();
            }
            else{
                $widget.addClass('close');
                $widget.find('.wb-toggle').slideUp();
            }

        });
        $(document).on('added_to_cart', function (e, fragments, cart_hash, $button) {
            $('<div class="shop-cust-qty" data-item-key="' + $button.attr("data-product_id") + '"><div class="quantity"><span class="qty-minus">-</span><input class="input-text qty text" step="1" min="0" max="140" name="cart[' + cart_hash + '][qty]" value="1" title="Qty" size="4" pattern="[0-9]*" inputmode="numeric" type="number"><span class="qty-plus">+</span></div></div>').prependTo($button.parent());
            $button.attr("style", "");
        });
        $(document).on('click', '.wrap-addto span', function () {
            var new_qty = 0,
                _old_qty = parseInt($(this).parent().find('.input-text.qty.text').val());
            if ($(this).hasClass('qty-plus')) {
                new_qty = parseInt(_old_qty + 1);
            } else {
                new_qty = parseInt(_old_qty - 1);
            }
            var cart_key = $(this).closest('.shop-cust-qty').data('item-key') || '';
            var data = {
                action: 'pisces_update_mini_cart',
                product_num: new_qty,
                cart_key: cart_key
            };
            console.log(data);
            $.post(ajax_object.ajax_url, data, function (response) {
                var resp = JSON.parse(response);
                if (resp.remove == 0) {
                    $('a[data-product_id=' + resp.id + '].add_to_cart_button').css('display', 'block');
                    $('[data-item-key=' + resp.id + '].shop-cust-qty').remove();
                }
                $(document.body).trigger('wc_fragment_refresh');
                $('.widget_shopping_cart_content').html(resp.template);
                $('.woocommerce-mini-cart__total .amount').html(resp.total);
            });
        });
        $(document).on('click', '.woocommerce-mini-cart .remove_from_cart_button', function () {
            var $id = $(this).data('product_id');
            $('a[data-product_id='+$id+'].add_to_cart_button').css('display', 'block');
            $('[data-item-key=' + $id + '].shop-cust-qty').remove();
        });


        $(document).on('click', '.package__price .price__item a', function(e){
            e.preventDefault();
            var $input_qty = $('input[name="quantity"]'),
                c_qty = parseInt($(this).data('qtymin') || 1),
                c_price = $(this).find('.price__item-detail').html(),
                $ywcrbp_regular_price = $('.single-price-wrapper .price .ywcrbp_regular_price'),
                $ywcrbp_your_price = $('.single-price-wrapper .price .ywcrbp_your_price');

            $(this).parent().addClass('active').siblings().removeClass('active');
            $input_qty.val(c_qty);
            $ywcrbp_regular_price.html(c_price);
            $ywcrbp_your_price.html(c_price);
            if($ywcrbp_your_price.length == 0 && $ywcrbp_regular_price.length == 0){
                $('.single-price-wrapper .price').html(c_price);
            }
        });

        $(document).on('change', '.la-myaccount-page .form-row .la-checkbox input', function(e){
            if($(this).is(':checked')){
                $(this).parent().siblings('input').attr('type','text');
            }
            else{
                $(this).parent().siblings('input').attr('type','password');
            }
        });

        $(document).on('click', '.la-myaccount-page .la-tab-panel .la_tab--actions a', function(e){
            $(this).closest('.la-tab-panel').addClass('active').siblings('.la-tab-panel').removeClass('active');
        });

        $('.essees-custom-menu .custom-header-menu').hover(function(){
            $('body').addClass('active-menu-overlay');
        }, function(){
            $('body').removeClass('active-menu-overlay');
        })

        $(document).on('keyup', '.search-brands input', function(){
            var w = $(this).val(),
                $parent = $(this).parent(),
                $widget = $(this).closest('.widget');
            if (w) {
                $parent.addClass('active');
                $widget.find('li').hide();
                $('li a:SearchContains('+ w +')', $widget).parent().show();
            }
            else {
                $parent.removeClass('active');
                $widget.find('li').show();
            }
        });
        $(document).on('click touchend', '.search-brands.active i:last-child', function(e){
            var $parent = $(this).parent();
            $parent.find('input').val('').trigger('keyup');

        })
    });

    $(window).load(function(){
        LA.utils.eventManager.subscribe('LA:AjaxShopFilter:success', function(e, response, url, element){
            unwrap_widgetbox();
        });
    });

})(jQuery);
