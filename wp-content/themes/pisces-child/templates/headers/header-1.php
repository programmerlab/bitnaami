<?php

$header_layout = Pisces()->layout->get_header_layout();

$header_access_icon = Pisces()->settings->get('header_access_icon');

$show_header_top        = Pisces()->settings->get('enable_header_top');
$header_top_elements    = Pisces()->settings->get('header_top_elements');
$custom_header_top_html = Pisces()->settings->get('use_custom_header_top');

$aside_sidebar_name = apply_filters('pisces/filter/aside_widget_bottom', 'aside-widget');

$enable_header_aside = false;

if(!empty($header_access_icon)){
    foreach($header_access_icon as $component){
        if(isset($component['type']) && $component['type'] == 'aside_header'){
            $enable_header_aside = true;
            break;
        }
    }
}

?>
<?php if($enable_header_aside): ?>
<aside id="header_aside" class="header--aside">
    <div class="header-aside-wrapper">
        <a class="btn-aside-toggle" href="#"><i class="pisces-icon-simple-remove"></i></a>
        <div class="header-aside-inner">
            <?php if(is_active_sidebar($aside_sidebar_name)): ?>
                <div class="header-widget-bottom">
                    <?php
                    dynamic_sidebar($aside_sidebar_name);
                    ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</aside>
<?php endif;?>

<header id="masthead" class="site-header">
    <?php if($show_header_top == 'custom' && !empty($custom_header_top_html) ): ?>
        <div class="site-header-top use-custom-html">
            <div class="container">
                <?php echo Pisces_Helper::remove_js_autop($custom_header_top_html); ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if($show_header_top == 'yes' && !empty($header_top_elements) ): ?>
        <div class="site-header-top use-default">
            <div class="container">
                <div class="header-top-elements">
                    <?php
                    foreach($header_top_elements as $component){
                        if(isset($component['type'])){
                            echo Pisces_Helper::render_access_component($component['type'], $component, 'header_component');
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="site-header-outer">
        <div class="site-header-inner">
            <div class="container">
                <div class="header-main clearfix">
                    <div class="header-component-outer header-left">
                        <div class="site-branding">
                            <a href="<?php echo esc_url( home_url( '/'  ) ); ?>" rel="home">
                                <figure class="logo--normal"><?php Pisces()->layout->render_logo();?></figure>
                                <figure class="logo--transparency"><?php Pisces()->layout->render_transparency_logo();?></figure>
                            </a>
                        </div>
                    </div>
                    <div class="header-component-outer header-middle">
                        <div class="header-component-inner essees-custom-menu clearfix">
                            <div class="custom-header-menu">
                                <div class="custom-toggle-primary-menu">
                                    <span><?php echo esc_html_x('ALL PRODUCTS', 'front-end', 'pisces') ?></span>
                                </div>
                                <nav class="header-aside-nav menu--vertical menu--vertical-<?php echo is_rtl() ? 'right' : 'left' ?> clearfix">
                                    <div class="nav-inner" data-container="#masthead .essees-custom-menu .nav-inner" data-parent-container="#masthead .header-main">
                                        <?php Pisces()->layout->render_main_nav(array(
                                            'menu_class'    => 'main-menu mega-menu isVerticalMenu'
                                        ));?>
                                    </div>
                                </nav>
                            </div>
                            <div class="searchform-wrapper">
                                <?php if(shortcode_exists('yith_woocommerce_ajax_search')): ?>
                                    <?php echo do_shortcode('[yith_woocommerce_ajax_search]');?>
                                <?php else: ?>
                                    <form method="get" class="search-form" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
                                        <?php
                                        $args = array(
                                            'show_option_all'    => esc_attr_x('All Items', 'front-view', 'pisces'),
                                            'name'               => 'cat',
                                            'hierarchical'       => true,
                                            'value_field'        => 'slug'
                                        );
                                        if( function_exists('WC') ) {
                                            $args['name'] = 'product_cat';
                                            $args['taxonomy'] = 'product_cat';
                                        }
                                        ?>
                                        <div class="sf-fields">
                                            <div class="sf-field sf-field-select"><?php wp_dropdown_categories($args); ?></div>
                                            <div class="sf-field sf-field-input">
                                                <input autocomplete="off" type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search&hellip;', 'front-view', 'pisces' ); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'front-view', 'pisces' ); ?>" />
                                                <?php if(function_exists('WC')): ?>
                                                    <input type="hidden" name="post_type" value="product"/>
                                                <?php endif; ?>
                                            </div>
                                            <button class="search-button" type="submit"><i class="fa fa-search"></i></button>
                                        </div>
                                    </form>
                                <?php endif; ?>
                                <!-- .search-form -->
                            </div>
                        </div>
                    </div>
                    <div class="header-component-outer header-right">
                        <div class="header-component-inner clearfix">
                            <?php
                            if(!empty($header_access_icon)){
                                foreach($header_access_icon as $component){
                                    if(isset($component['type'])){
                                        echo Pisces_Helper::render_access_component($component['type'], $component, 'header_component');
                                    }
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="la-header-sticky-height"></div>
    </div>
</header>
<!-- #masthead -->