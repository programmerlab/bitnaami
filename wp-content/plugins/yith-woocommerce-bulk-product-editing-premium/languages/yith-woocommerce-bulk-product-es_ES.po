# Copyright (C) 2015 YIThemes
# This file is distributed under the same license as the YITH Plugin Starter package.
msgid ""
msgstr ""
"Project-Id-Version: YITH WooCommerce Bulk Product Editing Premium\n"
"Report-Msgid-Bugs-To: Your Inspiration Themes <plugins@yithemes.com>\n"
"POT-Creation-Date: 2019-01-28 09:42:15+00:00\n"
"PO-Revision-Date: 2018-05-28 20:28+0100\n"
"Last-Translator: \n"
"Language-Team: Your Inspiration Themes <info@yithemes.com>\n"
"Language: es_ES\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.12\n"
"X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;"
"_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;"
"esc_html_x:1,2c\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-Basepath: ..\n"
"X-Textdomain-Support: yes\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: plugin-fw\n"

#: class.yith-wcbep-admin-premium.php:61 templates/export.php:11
msgid ""
"You do not have sufficient permissions to export the content of this site."
msgstr ""
"No tienes suficientes permisos para exportar el contenido de este sitio."

#: class.yith-wcbep-admin-premium.php:72 class.yith-wcbep-admin-premium.php:88
#: templates/premium/main-tab-importer.php:15
msgid "Import"
msgstr "Importar"

#: class.yith-wcbep-admin-premium.php:73
#: templates/premium/panel/enabled-columns-tab.php:7
msgid "Enabled Columns"
msgstr "Columnas Habilitadas"

#: class.yith-wcbep-admin-premium.php:74
msgid "Settings"
msgstr "Ajustes"

#: class.yith-wcbep-admin-premium.php:82
#: templates/premium/main-tab-importer.php:7
msgid "Import Products"
msgstr "Importar Productos"

#: class.yith-wcbep-admin-premium.php:86
#: templates/premium/main-tab-importer.php:10
msgid "File URL"
msgstr "URL de archivo"

#: class.yith-wcbep-admin-premium.php:97
msgid "Importing..."
msgstr "Importando..."

#: class.yith-wcbep-admin-premium.php:144
msgid "%s product deleted"
msgid_plural "%s products deleted"
msgstr[0] "%s producto borrado"
msgstr[1] "%s productos borrados"

#: class.yith-wcbep-admin-premium.php:834
msgid ""
"The downloadable file %s cannot be used as it does not have an allowed file "
"type. Allowed types include: %s"
msgstr ""
"El archivo descargable %s no puede usarse ya que no tiene un tipo de archivo "
"permitido. Los tipos permitidos son: %s"

#: class.yith-wcbep-admin-premium.php:841
msgid ""
"The downloadable file %s cannot be used as it does not exist on the server."
msgstr ""
"El archivo descargable %s no puede usarse porque no existe en el servidor."

#: class.yith-wcbep-admin.php:190
msgid "%s product edited"
msgid_plural "%s products edited"
msgstr[0] "%s producto editado"
msgstr[1] "%s productos editados"

#: class.yith-wcbep-admin.php:252
msgid "Bulk Product Editing"
msgstr "Bulk Product Editing"

#: class.yith-wcbep-admin.php:256
msgid "Premium Version"
msgstr "Versión premium"

#: class.yith-wcbep-admin.php:322
msgid "No Product Selected: to make a bulk edit, select one or more products"
msgstr ""
"No hay productos seleccionados: para hacer una edición en bloque, selecciona "
"uno o más productos"

#: class.yith-wcbep-admin.php:323
msgid "- - - Empty - - -"
msgstr "- - - Vacío - - -"

#: class.yith-wcbep-admin.php:324
msgid "file"
msgstr "archivo"

#: class.yith-wcbep-admin.php:325
msgid "files"
msgstr "archivos"

#: class.yith-wcbep-admin.php:326
msgid "This field is not editable for this product!"
msgstr "¡Este campo no es editable para este producto!"

#: class.yith-wcbep-admin.php:327
msgid ""
"This field is not editable right now! Please save before trying to edit this "
"field."
msgstr ""
"¡Este campo no es editable ahora mismo! Por favor, guarda antes de intentar "
"editar este campo."

#: class.yith-wcbep-admin.php:328
msgid "ERROR: file not correct!"
msgstr "ERROR: ¡archivo incorrecto!"

#: class.yith-wcbep-admin.php:329
msgid "Confirm Deletion [Selected Products: %s]"
msgstr "Confirmar Borrado [Productos seleccionados: %s]"

#: class.yith-wcbep-admin.php:330
#: templates/premium/main-tab-filters-and-table.php:384
msgid "Delete Selected"
msgstr "Borrar Seleccionados"

#: functions.yith-wcbep-premium.php:130
#: includes/class.yith-wcbep-list-table-premium.php:1024
#: templates/premium/main-tab-bulk-editor.php:638
msgid "Grouped product"
msgstr "Producto agrupado"

#: functions.yith-wcbep-premium.php:133
#: includes/class.yith-wcbep-list-table-premium.php:1025
#: templates/premium/main-tab-bulk-editor.php:639
msgid "External/Affiliate product"
msgstr "Producto externo/afiliado"

#: functions.yith-wcbep-premium.php:136
#: includes/class.yith-wcbep-list-table-premium.php:1026
#: templates/premium/main-tab-bulk-editor.php:640
msgid "Variable product"
msgstr "Producto variable"

#: functions.yith-wcbep-premium.php:139
#: includes/class.yith-wcbep-list-table-premium.php:1023
#: templates/premium/main-tab-bulk-editor.php:637
msgid "Simple product"
msgstr "Producto simple"

#: functions.yith-wcbep-premium.php:155
#: includes/class.yith-wcbep-list-table.php:51
msgid "ID"
msgstr "ID"

#: functions.yith-wcbep-premium.php:156
#: includes/class.yith-wcbep-list-table.php:52
msgid "Title"
msgstr "Título"

#: functions.yith-wcbep-premium.php:157
msgid "Slug"
msgstr "Slug"

#: functions.yith-wcbep-premium.php:158
msgid "Image"
msgstr "Imagen"

#: functions.yith-wcbep-premium.php:159
#, fuzzy
msgid "Product gallery"
msgstr "Galería de Productos"

#: functions.yith-wcbep-premium.php:160
msgid "Description"
msgstr "Descripción"

#: functions.yith-wcbep-premium.php:161
#, fuzzy
msgid "Short description"
msgstr "Descripción corta"

#: functions.yith-wcbep-premium.php:162
#, fuzzy
msgid "Regular price"
msgstr "Precio normal"

#: functions.yith-wcbep-premium.php:163
#, fuzzy
msgid "Sale price"
msgstr "Precio rebajado"

#: functions.yith-wcbep-premium.php:164
#, fuzzy
msgid "Purchase note"
msgstr "Nota de compra"

#: functions.yith-wcbep-premium.php:165
#: includes/class.yith-wcbep-list-table.php:55
msgid "Categories"
msgstr "Categorías"

#: functions.yith-wcbep-premium.php:166
msgid "Tags"
msgstr "Etiquetas"

#: functions.yith-wcbep-premium.php:167
msgid "SKU"
msgstr "SKU"

#: functions.yith-wcbep-premium.php:168
msgid "Weight"
msgstr "Peso"

#: functions.yith-wcbep-premium.php:169
msgid "Height"
msgstr "Altura"

#: functions.yith-wcbep-premium.php:170
msgid "Width"
msgstr "Ancho"

#: functions.yith-wcbep-premium.php:171
msgid "Length"
msgstr "Largo"

#: functions.yith-wcbep-premium.php:172
#, fuzzy
msgid "Stock qty"
msgstr "Ctdad. disponible"

#: functions.yith-wcbep-premium.php:173
#, fuzzy
msgid "Download limit"
msgstr "Límite de descarga"

#: functions.yith-wcbep-premium.php:174
#, fuzzy
msgid "Download expiry"
msgstr "Caducidad de la descarga"

#: functions.yith-wcbep-premium.php:175
#, fuzzy
msgid "Downloadable files"
msgstr "Archivos Descargables"

#: functions.yith-wcbep-premium.php:176
msgid "Menu order"
msgstr "Menú de pedido"

#: functions.yith-wcbep-premium.php:177
msgid "Stock status"
msgstr "Estado de Stock"

#: functions.yith-wcbep-premium.php:178
#, fuzzy
msgid "Manage stock"
msgstr "Administrar Stock"

#: functions.yith-wcbep-premium.php:179
#, fuzzy
msgid "Sold individually"
msgstr "Vender individualmente"

#: functions.yith-wcbep-premium.php:180
msgid "Featured"
msgstr "Destacados"

#: functions.yith-wcbep-premium.php:181
msgid "Virtual"
msgstr "Virtual"

#: functions.yith-wcbep-premium.php:182
msgid "Downloadable"
msgstr "Descargable"

#: functions.yith-wcbep-premium.php:183
#, fuzzy
msgid "Enable reviews"
msgstr "Permitir Reseñas"

#: functions.yith-wcbep-premium.php:184
#, fuzzy
msgid "Tax status"
msgstr "Clase de estado"

#: functions.yith-wcbep-premium.php:185
#, fuzzy
msgid "Tax class"
msgstr "Clase de impuesto"

#: functions.yith-wcbep-premium.php:186
#, fuzzy
msgid "Allow backorders?"
msgstr "¿Permitir repetir pedidos?"

#: functions.yith-wcbep-premium.php:187
msgid "Shipping class"
msgstr "Clase de envío"

#: functions.yith-wcbep-premium.php:188
msgid "Status"
msgstr "Estado"

#: functions.yith-wcbep-premium.php:189
msgid "Catalog visibility"
msgstr "Visibilidad del catálogo"

#: functions.yith-wcbep-premium.php:190
msgid "Download Type"
msgstr "Tipo de descarga"

#: functions.yith-wcbep-premium.php:191
msgid "Product Type"
msgstr "Tipo de producto"

#: functions.yith-wcbep-premium.php:192
#: includes/class.yith-wcbep-list-table.php:56
msgid "Date"
msgstr "Fecha"

#: functions.yith-wcbep-premium.php:193
#, fuzzy
msgid "Sale price from"
msgstr "Desde precio rebajado"

#: functions.yith-wcbep-premium.php:194
#, fuzzy
msgid "Sale price to"
msgstr "Hasta precio rebajado"

#: functions.yith-wcbep-premium.php:195
msgid "Button text"
msgstr "Texto del botón"

#: functions.yith-wcbep-premium.php:196
msgid "Product URL"
msgstr "URL del producto"

#: functions.yith-wcbep-premium.php:197
#, fuzzy
msgid "Upsells"
msgstr "Up-Sells"

#: functions.yith-wcbep-premium.php:198
#, fuzzy
msgid "Cross-sells"
msgstr "Cross-Sells"

#: includes/class.yith-wcbep-custom-fields-manager.php:68
#: templates/premium/main-tab-bulk-editor.php:25
#: templates/premium/panel/custom-fields-tab.php:11
msgid "Custom Fields"
msgstr "Campos personalizados"

#: includes/class.yith-wcbep-custom-fields-manager.php:215
#: includes/class.yith-wcbep-custom-taxonomies-manager.php:348
#: includes/compatibility/class.yith-wcbep-badge-management-compatibility.php:129
#: includes/compatibility/class.yith-wcbep-brands-add-on-compatibility.php:248
#: templates/main-tab.php:116 templates/main-tab.php:131
#: templates/premium/main-tab-bulk-editor.php:37
#: templates/premium/main-tab-bulk-editor.php:53
#: templates/premium/main-tab-bulk-editor.php:69
#: templates/premium/main-tab-bulk-editor.php:85
#: templates/premium/main-tab-bulk-editor.php:104
#: templates/premium/main-tab-bulk-editor.php:123
#: templates/premium/main-tab-bulk-editor.php:142
#: templates/premium/main-tab-bulk-editor.php:223
#: templates/premium/main-tab-bulk-editor.php:246
#: templates/premium/main-tab-bulk-editor.php:279
#: templates/premium/main-tab-bulk-editor.php:314
#: templates/premium/main-tab-bulk-editor.php:377
#: templates/premium/main-tab-bulk-editor.php:392
#: templates/premium/main-tab-bulk-editor.php:410
#: templates/premium/main-tab-bulk-editor.php:423
#: templates/premium/main-tab-bulk-editor.php:503
#: templates/premium/main-tab-bulk-editor.php:519
#: templates/premium/main-tab-bulk-editor.php:535
#: templates/premium/main-tab-bulk-editor.php:551
#: templates/premium/main-tab-bulk-editor.php:599
#: templates/premium/main-tab-bulk-editor.php:692
#: templates/premium/main-tab-bulk-editor.php:709
#: templates/premium/main-tab-bulk-editor.php:747
#: templates/premium/main-tab-bulk-editor.php:765
#: templates/premium/main-tab-bulk-editor.php:783
#: templates/premium/main-tab-bulk-editor.php:800
msgid "Set new"
msgstr "Establecer nuevo"

#: includes/class.yith-wcbep-custom-fields-manager.php:216
#: templates/premium/main-tab-bulk-editor.php:38
#: templates/premium/main-tab-bulk-editor.php:54
#: templates/premium/main-tab-bulk-editor.php:70
#: templates/premium/main-tab-bulk-editor.php:86
#: templates/premium/main-tab-bulk-editor.php:105
#: templates/premium/main-tab-bulk-editor.php:124
#: templates/premium/main-tab-bulk-editor.php:280
#: templates/premium/main-tab-bulk-editor.php:748
#: templates/premium/main-tab-bulk-editor.php:766
#: templates/premium/main-tab-bulk-editor.php:784
#: templates/premium/main-tab-bulk-editor.php:801
msgid "Prepend"
msgstr "Preceder"

#: includes/class.yith-wcbep-custom-fields-manager.php:217
#: templates/premium/main-tab-bulk-editor.php:39
#: templates/premium/main-tab-bulk-editor.php:55
#: templates/premium/main-tab-bulk-editor.php:71
#: templates/premium/main-tab-bulk-editor.php:87
#: templates/premium/main-tab-bulk-editor.php:106
#: templates/premium/main-tab-bulk-editor.php:125
#: templates/premium/main-tab-bulk-editor.php:281
#: templates/premium/main-tab-bulk-editor.php:749
#: templates/premium/main-tab-bulk-editor.php:767
#: templates/premium/main-tab-bulk-editor.php:785
#: templates/premium/main-tab-bulk-editor.php:802
msgid "Append"
msgstr "Anexar"

#: includes/class.yith-wcbep-custom-fields-manager.php:218
#: templates/premium/main-tab-bulk-editor.php:40
#: templates/premium/main-tab-bulk-editor.php:56
#: templates/premium/main-tab-bulk-editor.php:72
#: templates/premium/main-tab-bulk-editor.php:88
#: templates/premium/main-tab-bulk-editor.php:107
#: templates/premium/main-tab-bulk-editor.php:126
#: templates/premium/main-tab-bulk-editor.php:282
#: templates/premium/main-tab-bulk-editor.php:750
#: templates/premium/main-tab-bulk-editor.php:768
#: templates/premium/main-tab-bulk-editor.php:786
#: templates/premium/main-tab-bulk-editor.php:803
msgid "Replace"
msgstr "Sustituir"

#: includes/class.yith-wcbep-custom-fields-manager.php:219
#: templates/main-tab.php:138 templates/premium/main-tab-bulk-editor.php:89
#: templates/premium/main-tab-bulk-editor.php:108
#: templates/premium/main-tab-bulk-editor.php:127
#: templates/premium/main-tab-bulk-editor.php:145
#: templates/premium/main-tab-bulk-editor.php:224
#: templates/premium/main-tab-bulk-editor.php:399
#: templates/premium/main-tab-bulk-editor.php:411
#: templates/premium/main-tab-bulk-editor.php:424
#: templates/premium/main-tab-bulk-editor.php:508
#: templates/premium/main-tab-bulk-editor.php:524
#: templates/premium/main-tab-bulk-editor.php:540
#: templates/premium/main-tab-bulk-editor.php:556
#: templates/premium/main-tab-bulk-editor.php:604
#: templates/premium/main-tab-bulk-editor.php:697
#: templates/premium/main-tab-bulk-editor.php:714
#: templates/premium/main-tab-bulk-editor.php:751
#: templates/premium/main-tab-bulk-editor.php:769
#: templates/premium/main-tab-bulk-editor.php:787
#: templates/premium/main-tab-bulk-editor.php:804
#: templates/premium/main-tab-custom-input.php:38
msgid "Delete"
msgstr "Borrar"

#: includes/class.yith-wcbep-custom-taxonomies-manager.php:70
#: templates/premium/panel/custom-taxonomies-tab.php:25
msgid "Custom Taxonomies"
msgstr "Taxonomías personalizadas"

#: includes/class.yith-wcbep-custom-taxonomies-manager.php:346
#: includes/compatibility/class.yith-wcbep-badge-management-compatibility.php:127
#: includes/compatibility/class.yith-wcbep-brands-add-on-compatibility.php:246
#: templates/premium/main-tab-bulk-editor.php:244
#: templates/premium/main-tab-bulk-editor.php:312
msgid "Add"
msgstr "Añadir"

#: includes/class.yith-wcbep-custom-taxonomies-manager.php:347
#: includes/compatibility/class.yith-wcbep-badge-management-compatibility.php:128
#: includes/compatibility/class.yith-wcbep-brands-add-on-compatibility.php:247
#: templates/premium/main-tab-bulk-editor.php:245
#: templates/premium/main-tab-bulk-editor.php:313
msgid "Remove"
msgstr "Quitar"

#: includes/class.yith-wcbep-importer.php:90
msgid "Importing process completed!"
msgstr "¡Proceso de importación completado!"

#: includes/class.yith-wcbep-importer.php:100
#: includes/class.yith-wcbep-importer.php:107
msgid "Sorry, an error has occurred."
msgstr "Lo sentimos, ha ocurrido un error."

#: includes/class.yith-wcbep-importer.php:101
msgid "The file does not exist, please try again."
msgstr "El archivo no existe, por favor, inténtalo otra vez."

#: includes/class.yith-wcbep-importer.php:141
msgid "Failed to import an attribute because it has no valid name"
msgstr "Ha fallado importar un atributo porque no tiene un nombre válido"

#: includes/class.yith-wcbep-importer.php:160
msgid "Attribute <strong>%s</strong> already exists."
msgstr "El atributo <strong>%s</strong> ya existe."

#: includes/class.yith-wcbep-importer.php:216
msgid "Failed to import %s %s"
msgstr "Ha fallado importar %s %s"

#: includes/class.yith-wcbep-importer.php:237
msgid "Failed to import <strong>%s</strong>: Invalid post type %s"
msgstr ""
"Ha fallado importar <strong>%s</strong>: Tipo de publicación inválida %s"

#: includes/class.yith-wcbep-importer.php:252
msgid "%s <strong>%s</strong> already exists."
msgstr "%s <strong>%s</strong> ya existe"

#: includes/class.yith-wcbep-importer.php:307
#: includes/class.yith-wcbep-importer.php:321
msgid "Failed to import %s <strong>%s</strong>"
msgstr "Ha fallado la importación de %s <strong>%s</strong>"

#: includes/class.yith-wcbep-importer.php:360
msgid "Failed to import %s [%s]"
msgstr "Ha fallado importar %s [%s]"

#: includes/class.yith-wcbep-importer.php:429
msgid "Invalid file type"
msgstr "Tipo de archivo inválido"

#: includes/class.yith-wcbep-importer.php:474
msgid "Remote server did not respond"
msgstr "El servidor remoto no ha respondido"

#: includes/class.yith-wcbep-importer.php:481
msgid "Remote server returned error response %1$d %2$s"
msgstr "El servidor remoto ha devuelto la respuesta de error %1$d %2$s"

#: includes/class.yith-wcbep-importer.php:489
msgid "Remote file with incorrect size"
msgstr "Archivo remoto con tamaño incorrecto"

#: includes/class.yith-wcbep-importer.php:495
msgid "Zero size file downloaded"
msgstr "Archivo de tamaño cero descargado"

#: includes/class.yith-wcbep-importer.php:502
msgid "Remote file is too large, limit is %s"
msgstr "El archivo remoto es demasiado grande, el límite es %s"

#: includes/class.yith-wcbep-list-table-premium.php:921
#: templates/premium/main-tab-bulk-editor.php:437
msgid "Taxable"
msgstr "Imponible"

#: includes/class.yith-wcbep-list-table-premium.php:922
#: templates/premium/main-tab-bulk-editor.php:438
msgid "Shipping only"
msgstr "Sólo envío"

#: includes/class.yith-wcbep-list-table-premium.php:933
#: templates/premium/main-tab-bulk-editor.php:454
msgid "Standard"
msgstr "Estándar"

#: includes/class.yith-wcbep-list-table-premium.php:952
#: templates/premium/main-tab-bulk-editor.php:617
msgid "Do not allow"
msgstr "No permitir"

#: includes/class.yith-wcbep-list-table-premium.php:953
#: templates/premium/main-tab-bulk-editor.php:618
msgid "Allow, but notify customer"
msgstr "Permitir, pero notificar al cliente"

#: includes/class.yith-wcbep-list-table-premium.php:954
#: templates/premium/main-tab-bulk-editor.php:619
msgid "Allow"
msgstr "Permitir"

#: includes/class.yith-wcbep-list-table-premium.php:967
#: templates/premium/main-tab-bulk-editor.php:479
#: templates/premium/main-tab-filters-and-table.php:296
msgid "No shipping class"
msgstr "Ninguna clase de envío"

#: includes/class.yith-wcbep-list-table-premium.php:1004
#: templates/premium/main-tab-bulk-editor.php:729
msgid "Standard Product"
msgstr "Producto estándar"

#: includes/class.yith-wcbep-list-table-premium.php:1005
#: templates/premium/main-tab-bulk-editor.php:730
msgid "Application/Software"
msgstr "Aplicación/Software"

#: includes/class.yith-wcbep-list-table-premium.php:1006
#: templates/premium/main-tab-bulk-editor.php:731
msgid "Music"
msgstr "Música"

#: includes/class.yith-wcbep-list-table-premium.php:1019
msgid "Variation"
msgstr "Variación"

#: includes/class.yith-wcbep-list-table-premium.php:1242
#: includes/class.yith-wcbep-list-table.php:290
#: templates/premium/main-tab-columns-settings.php:33
msgid "Select All"
msgstr "Seleccionar todo"

#: includes/class.yith-wcbep-list-table-premium.php:1347
#: includes/class.yith-wcbep-list-table.php:378
msgid "1 item"
msgid_plural "%s items"
msgstr[0] "1 artículo"
msgstr[1] "%s artículos"

#: includes/class.yith-wcbep-list-table.php:53 templates/main-tab.php:48
#: templates/main-tab.php:112
msgid "Regular Price"
msgstr "Precio normal"

#: includes/class.yith-wcbep-list-table.php:54 templates/main-tab.php:63
#: templates/main-tab.php:127
msgid "Sale Price"
msgstr "Precio rebajado"

#: includes/class.yith-wcbep-xml-parser.php:137
msgid ""
"This does not appear to be a WXR file, missing/invalid WXR version number"
msgstr "Este no parece ser un archivo WXR, número de versión inválido/ausente"

#: includes/compatibility/class.yith-wcbep-badge-management-compatibility.php:66
#: includes/compatibility/class.yith-wcbep-badge-management-compatibility.php:122
msgid "Badge"
msgstr "Insignia"

#: includes/compatibility/class.yith-wcbep-brands-add-on-compatibility.php:83
#: includes/compatibility/class.yith-wcbep-brands-add-on-compatibility.php:134
#: includes/compatibility/class.yith-wcbep-brands-add-on-compatibility.php:241
msgid "Brands"
msgstr "Marcas"

#: includes/compatibility/class.yith-wcbep-deposits-compatibility.php:65
msgid "Enable deposit"
msgstr "Activar depósito"

#: includes/compatibility/class.yith-wcbep-deposits-compatibility.php:67
#: includes/compatibility/class.yith-wcbep-deposits-compatibility.php:76
#: includes/compatibility/class.yith-wcbep-deposits-compatibility.php:85
msgid "Default"
msgstr "Por defecto"

#: includes/compatibility/class.yith-wcbep-deposits-compatibility.php:68
#: templates/premium/main-tab-bulk-editor.php:157
#: templates/premium/main-tab-bulk-editor.php:169
#: templates/premium/main-tab-bulk-editor.php:342
#: templates/premium/main-tab-bulk-editor.php:355
#: templates/premium/main-tab-bulk-editor.php:573
#: templates/premium/main-tab-bulk-editor.php:657
#: templates/premium/main-tab-bulk-editor.php:669
#: templates/premium/main-tab-bulk-editor.php:681
msgid "Yes"
msgstr "Sí"

#: includes/compatibility/class.yith-wcbep-deposits-compatibility.php:69
#: templates/premium/main-tab-bulk-editor.php:158
#: templates/premium/main-tab-bulk-editor.php:170
#: templates/premium/main-tab-bulk-editor.php:343
#: templates/premium/main-tab-bulk-editor.php:356
#: templates/premium/main-tab-bulk-editor.php:574
#: templates/premium/main-tab-bulk-editor.php:658
#: templates/premium/main-tab-bulk-editor.php:670
#: templates/premium/main-tab-bulk-editor.php:682
msgid "No"
msgstr "No"

#: includes/compatibility/class.yith-wcbep-deposits-compatibility.php:74
#: includes/compatibility/class.yith-wcbep-deposits-compatibility.php:77
msgid "Force deposit"
msgstr "Forzar depóstio"

#: includes/compatibility/class.yith-wcbep-deposits-compatibility.php:78
msgid "Allow deposit"
msgstr "Permitir depósito"

#: includes/compatibility/class.yith-wcbep-deposits-compatibility.php:83
msgid "Create balance orders"
msgstr "Crear balances de pedido"

#: includes/compatibility/class.yith-wcbep-deposits-compatibility.php:86
msgid "Let users pay the balance online"
msgstr "Permitir a los usuarios pagar el balance online"

#: includes/compatibility/class.yith-wcbep-deposits-compatibility.php:87
msgid "Customers will pay the balance using other means"
msgstr "Los clientes pagarán el balance usando otros medios"

#: includes/compatibility/class.yith-wcbep-multivendor-compatibility.php:74
msgid "Permission denied!"
msgstr "¡Permiso denegado!"

#: init.php:52
msgid ""
"YITH WooCommerce Bulk Product Editing Premium Version is enabled but not "
"effective. It requires WooCommerce in order to work."
msgstr ""
"YITH WooCommerce Bulk Product Editing Premium Version está habilitado pero "
"no es efectivo. Requiere WooCommerce para funcionar."

#: plugin-options/settings-options.php:9
msgid "General Options"
msgstr "Opciones generales"

#: plugin-options/settings-options.php:16
msgid "Round Prices"
msgstr "Precios redondeados"

#: plugin-options/settings-options.php:20
msgid "If enabled, the prices will be rounded when bulk editing."
msgstr "Si está activado, los precios serán redondeados al editar masivamente."

#: plugin-options/settings-options.php:25
msgid "Use Regular Expressions"
msgstr ""

#: plugin-options/settings-options.php:29
msgid ""
"If enabled, the plugin uses Regular Expressions when searching for texts."
msgstr ""

#: plugin-options/settings-options.php:34
msgid "Use Light Query"
msgstr ""

#: plugin-options/settings-options.php:38
msgid ""
"If enabled, the plugin uses a light query to retrieve products, so it "
"improves your website performance. However, by enabling this option you "
"CANNOT use advanced functionalities: for example, filtering variable "
"products by price will not work. Please note: use it only if you have a huge "
"amount of products."
msgstr ""

#: templates/main-tab.php:10
#: templates/premium/main-tab-filters-and-table.php:15
msgid "Filters"
msgstr "Filtros"

#: templates/main-tab.php:30
msgid "Filter Categories"
msgstr "Filtrar categorías"

#: templates/main-tab.php:78
#: templates/premium/main-tab-filters-and-table.php:324
msgid "Products per page"
msgstr "Productos por página"

#: templates/main-tab.php:85
#: templates/premium/main-tab-filters-and-table.php:342
msgid "Get products"
msgstr "Añadir productos"

#: templates/main-tab.php:86
#: templates/premium/main-tab-filters-and-table.php:344
msgid "Reset filters"
msgstr "Reiniciar filtros"

#: templates/main-tab.php:90
#: templates/premium/main-tab-filters-and-table.php:350
msgid "Products"
msgstr "Productos"

#: templates/main-tab.php:91 templates/premium/main-tab-custom-input.php:19
#: templates/premium/main-tab-custom-input.php:45
#: templates/premium/main-tab-custom-input.php:55
#: templates/premium/main-tab-custom-input.php:67
#: templates/premium/main-tab-filters-and-table.php:354
#: templates/premium/panel/custom-fields-tab.php:27
#: templates/premium/panel/custom-taxonomies-tab.php:45
#: templates/premium/panel/enabled-columns-tab.php:40
msgid "Save"
msgstr "Guardar"

#: templates/main-tab.php:92 templates/main-tab.php:108
#: templates/premium/main-tab-bulk-editor.php:16
#: templates/premium/main-tab-filters-and-table.php:356
msgid "Bulk editing"
msgstr "Editar en bloque"

#: templates/main-tab.php:117 templates/main-tab.php:132
#: templates/premium/main-tab-bulk-editor.php:143
#: templates/premium/main-tab-bulk-editor.php:378
#: templates/premium/main-tab-bulk-editor.php:393
#: templates/premium/main-tab-bulk-editor.php:504
#: templates/premium/main-tab-bulk-editor.php:520
#: templates/premium/main-tab-bulk-editor.php:536
#: templates/premium/main-tab-bulk-editor.php:552
#: templates/premium/main-tab-bulk-editor.php:600
#: templates/premium/main-tab-bulk-editor.php:693
#: templates/premium/main-tab-bulk-editor.php:710
msgid "Increase by value"
msgstr "Aumentar por valor"

#: templates/main-tab.php:118 templates/main-tab.php:133
#: templates/premium/main-tab-bulk-editor.php:144
#: templates/premium/main-tab-bulk-editor.php:379
#: templates/premium/main-tab-bulk-editor.php:394
#: templates/premium/main-tab-bulk-editor.php:505
#: templates/premium/main-tab-bulk-editor.php:521
#: templates/premium/main-tab-bulk-editor.php:537
#: templates/premium/main-tab-bulk-editor.php:553
#: templates/premium/main-tab-bulk-editor.php:601
#: templates/premium/main-tab-bulk-editor.php:694
#: templates/premium/main-tab-bulk-editor.php:711
msgid "Decrease by value"
msgstr "Disminuir por valor"

#: templates/main-tab.php:119
msgid "Increase by percentage"
msgstr "Incrementar por porcentaje"

#: templates/main-tab.php:120
msgid "Decrease by percentage"
msgstr "Disminuir por porcentaje"

#: templates/main-tab.php:134 templates/premium/main-tab-bulk-editor.php:380
#: templates/premium/main-tab-bulk-editor.php:395
#: templates/premium/main-tab-bulk-editor.php:506
#: templates/premium/main-tab-bulk-editor.php:522
#: templates/premium/main-tab-bulk-editor.php:538
#: templates/premium/main-tab-bulk-editor.php:554
#: templates/premium/main-tab-bulk-editor.php:602
#: templates/premium/main-tab-bulk-editor.php:695
#: templates/premium/main-tab-bulk-editor.php:712
msgid "Increase by %"
msgstr "Incrementar por %"

#: templates/main-tab.php:135 templates/premium/main-tab-bulk-editor.php:381
#: templates/premium/main-tab-bulk-editor.php:396
#: templates/premium/main-tab-bulk-editor.php:507
#: templates/premium/main-tab-bulk-editor.php:523
#: templates/premium/main-tab-bulk-editor.php:539
#: templates/premium/main-tab-bulk-editor.php:555
#: templates/premium/main-tab-bulk-editor.php:603
#: templates/premium/main-tab-bulk-editor.php:696
#: templates/premium/main-tab-bulk-editor.php:713
msgid "Decrease by %"
msgstr "Disminuir por %"

#: templates/main-tab.php:136 templates/premium/main-tab-bulk-editor.php:397
msgid "Decrease by value from regular"
msgstr "Disminuir por valor desde normal"

#: templates/main-tab.php:137 templates/premium/main-tab-bulk-editor.php:398
msgid "Decrease by % from regular"
msgstr "Disminuir por % desde normal"

#: templates/main-tab.php:146 templates/premium/main-tab-bulk-editor.php:829
msgid "Apply"
msgstr "Aplicar"

#: templates/main-tab.php:147 templates/premium/main-tab-bulk-editor.php:831
#: templates/premium/main-tab-custom-input.php:20
#: templates/premium/main-tab-custom-input.php:47
#: templates/premium/main-tab-custom-input.php:57
#: templates/premium/main-tab-custom-input.php:69
#: templates/premium/main-tab-importer.php:16
msgid "Cancel"
msgstr "Cancelar"

#: templates/premium/main-tab-bulk-editor.php:18
msgid "General"
msgstr "General"

#: templates/premium/main-tab-bulk-editor.php:19
msgid "Categories, Tags, Attributes"
msgstr "Categorías, Etiquetas, Atributos"

#: templates/premium/main-tab-bulk-editor.php:20
msgid "Pricing"
msgstr "Tarificación"

#: templates/premium/main-tab-bulk-editor.php:21
msgid "Shipping"
msgstr "Envío"

#: templates/premium/main-tab-bulk-editor.php:22
msgid "Stock"
msgstr "Stock"

#: templates/premium/main-tab-bulk-editor.php:23
msgid "Type"
msgstr "Tipo"

#: templates/premium/main-tab-bulk-editor.php:228
msgid "Choose Image"
msgstr "Elegir imagen"

#: templates/premium/main-tab-bulk-editor.php:283
msgid "Delete all"
msgstr "Borrar todo"

#: templates/premium/main-tab-bulk-editor.php:336
#: templates/premium/main-tab-custom-input.php:114
msgid "is visible"
msgstr "es visible"

#: templates/premium/main-tab-bulk-editor.php:349
#: templates/premium/main-tab-custom-input.php:117
msgid "used for variations"
msgstr "usado para variaciones"

#: templates/premium/main-tab-bulk-editor.php:825
msgid "Please note: Bulk Editor edits only enabled columns"
msgstr ""
"Por favor, ten en cuenta: Bulk Editor edita sólo las columnas habilitadas"

#: templates/premium/main-tab-columns-settings.php:11
#: templates/premium/main-tab-filters-and-table.php:361
msgid "Show/Hide Columns"
msgstr "Ocultar/Mostrar columnas"

#: templates/premium/main-tab-columns-settings.php:34
msgid "Unselect"
msgstr "Deseleccionar todos"

#: templates/premium/main-tab-custom-input.php:34
msgid "File Name"
msgstr "Nombre de archivo"

#: templates/premium/main-tab-custom-input.php:35
msgid "http://"
msgstr "http://"

#: templates/premium/main-tab-custom-input.php:37
msgid "Choose file"
msgstr "Elegir archivo"

#: templates/premium/main-tab-custom-input.php:46
msgid "Add File"
msgstr "Añadir Archivo"

#: templates/premium/main-tab-custom-input.php:56
msgid "Add Images to Product Gallery"
msgstr "Añadir imagen a la Galería de Productos"

#: templates/premium/main-tab-custom-input.php:68
msgid "Remove Image"
msgstr "Quitar Imagen"

#: templates/premium/main-tab-custom-input.php:75
msgid "This field is not editable because this is a variation!"
msgstr "¡Este campo no es editable porque es una variación!"

#: templates/premium/main-tab-filters-and-table.php:29
#: templates/premium/main-tab-filters-and-table.php:46
#: templates/premium/main-tab-filters-and-table.php:63
msgid "Contains"
msgstr "Contiene"

#: templates/premium/main-tab-filters-and-table.php:30
#: templates/premium/main-tab-filters-and-table.php:47
#: templates/premium/main-tab-filters-and-table.php:64
msgid "Does not contain"
msgstr "No contiene"

#: templates/premium/main-tab-filters-and-table.php:31
#: templates/premium/main-tab-filters-and-table.php:48
#: templates/premium/main-tab-filters-and-table.php:65
msgid "Starts with"
msgstr "Empieza por"

#: templates/premium/main-tab-filters-and-table.php:32
#: templates/premium/main-tab-filters-and-table.php:49
#: templates/premium/main-tab-filters-and-table.php:66
msgid "Ends with"
msgstr "Acaba por"

#: templates/premium/main-tab-filters-and-table.php:33
#: templates/premium/main-tab-filters-and-table.php:50
#: templates/premium/main-tab-filters-and-table.php:67
#, fuzzy
msgid "Regular Expression"
msgstr "Precio normal"

#: templates/premium/main-tab-filters-and-table.php:311
msgid "Show all product types"
msgstr "Mostrar todos los tipos de producto"

#: templates/premium/main-tab-filters-and-table.php:333
msgid "Include Variations"
msgstr "Incluye variaciones"

#: templates/premium/main-tab-filters-and-table.php:346
msgid "Select on filters"
msgstr "Seleccionar en filtros"

#: templates/premium/main-tab-filters-and-table.php:366
msgid "Undo"
msgstr "Deshacer"

#: templates/premium/main-tab-filters-and-table.php:368
msgid "Redo"
msgstr "Rehacer"

#: templates/premium/main-tab-filters-and-table.php:375
msgid "Export Selected"
msgstr "Exportar seleccionados"

#: templates/premium/main-tab-filters-and-table.php:379
msgid "New Product"
msgstr "Nuevo producto"

#: templates/premium/main-tab-filters-and-table.php:400
msgid "Resize Table"
msgstr "Cambiar tamaño de tabla"

#: templates/premium/main-tab-importer.php:11
msgid "Upload"
msgstr "Subir"

#: templates/premium/panel/custom-taxonomies-tab.php:30
msgid "Choose Taxonomies"
msgstr "Elegir taxonomías"

#: templates/premium/panel/enabled-columns-tab.php:41
msgid "Enable All"
msgstr "Habilitar todos"

#: templates/premium/panel/enabled-columns-tab.php:42
msgid "Disable All"
msgstr "Deshabilitar todos"

#: templates/premium/panel/enabled-columns-tab.php:50
msgid "Saving..."
msgstr "Guardando..."

#. Description of the plugin/theme
msgid ""
"<code><strong>YITH WooCommerce Bulk Product Editing</strong></code> allows "
"you to edit multiple products at the same time. You can easily filter "
"products and edit all fields you need in a massive, simple and fast way. <a "
"href=\"https://yithemes.com/\" target=\"_blank\">Get more plugins for your e-"
"commerce shop on <strong>YITH</strong></a>"
msgstr ""

#: includes/class.yith-wcbep-list-table-premium.php:923
#: templates/premium/main-tab-bulk-editor.php:439
msgctxt "Tax status"
msgid "None"
msgstr "Ninguno"

#~ msgid "Catalog/search"
#~ msgstr "Catálogo/Búsqueda"

#~ msgid "Catalog"
#~ msgstr "Catálogo"

#~ msgid "Search"
#~ msgstr "Buscar"

#~ msgid "Hidden"
#~ msgstr "Oculto"

#~ msgid "%s new product"
#~ msgid_plural "%s new products"
#~ msgstr[0] "%s producto nuevo"
#~ msgstr[1] "%s productos nuevos"

#~ msgid "Plugin Documentation"
#~ msgstr "Documentación del Plugin"

#~ msgid "Menu Order"
#~ msgstr "Orden del menú"

#~ msgid "1 "
#~ msgid_plural "%s "
#~ msgstr[0] "1 "
#~ msgstr[1] "%s "

#~ msgid "None"
#~ msgstr "Ninguno"

#~ msgid "In stock"
#~ msgstr "En inventario"

#~ msgid "Out of stock"
#~ msgstr "Fuera de inventario"
