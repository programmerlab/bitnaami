=== YITH Multiple Shipping Addresses for WooCommerce ===

Contributors: yithemes
Tags: woocommerce, e-commerce, shop, yit, yith, yithemes, multi, multiple, ship, shipping, shipment, address, addresses, location, locations, place, places
Requires at least: 4.0
Tested up to: 5.0.2
Stable tag: 1.0.7
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Changelog ==

= Version 1.0.7 - Released: Dec 27, 2018 =

* New: Local Pickup integration.
* Update: Plugin core.
* Update: Language files.
* Fix: non-alphameric characters on Shipping Identifier.

= Version 1.0.6 - Released: Oct 23, 2018 =

* Update: Plugin Core.

= Version 1.0.5 - Released: Oct 17, 2018 =

* New: Support to WooCommerce 3.5.0 RC2
* Update: Plugin Core 3.0.25
* Update: Language files.

= Version 1.0.4 - Released: May 25, 2018 =

* New: Support to WooCommerce 3.4.0
* New: Support to WordPress 4.9.6
* New: Support to GDPR compliance - Export personal data
* New: Support to GDPR compliance - Erase personal data
* New: Privacy Policy Guide
* Update: Plugin core
* Update: Italian language
* Update: Spanish language
* Update: Dutch language

= Version 1.0.3 - Released: Jan 31, 2018 =

* New: Support to WC 3.3.0
* New: Spanish translation.
* New: Italian translation.
* Updated: Plugin core.
* Updated: Translation files.

= Version 1.0.2 - Released: Jan 09, 2018 =

* Tweak: Fixed the way how the address fields are taken on JS when saving an address.

= Version 1.0.1 - Released: Jan 05, 2018 =

* New: Dutch Translation.
* Update: Plugin Core.

= Version 1.0.0 - Released: Nov 30, 2017 =

* First release


== Suggestions ==

If you have suggestions about how to improve YITH Multiple Shipping Addresses for WooCommerce, you can [write us](mailto:plugins@yithemes.com "Your Inspiration Themes")
so we can bundle them into the next release of the plugin.


== Translators ==

If you have created your own language pack, or have an update for an existing one, 
you can send [gettext PO and MO file](http://codex.wordpress.org/Translating_WordPress "Translating WordPress")
[use](http://yithemes.com/contact/ "Your Inspiration Themes") 
so we can bundle it into YITH Multiple Shipping Addresses for WooCommerce languages.


= Available Languages =

* English