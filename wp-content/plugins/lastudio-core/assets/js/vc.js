;(function ( $ ) {
    'use strict';

    var VcClipBoard = function(){

        this.i18n = {
            copy : 'Copy',
            copy_plus : 'Copy+',
            paste: 'Paste'
        };

        this.initCache = this.initCache.bind(this);
        this.renderToolBar = this.renderToolBar.bind(this);
        this.bindEvent = this.bindEvent.bind(this);

        this.initCache();
        this.renderToolBar();
        this.bindEvent();
    };

    VcClipBoard.prototype.renderToolBar = function(){

        if ($('.composer-switch').length == 0 && $('#wpb_visual_composer').length > 0 && $('#wpb_visual_composer').is(":visible")) {
            $('#titlediv').after('<div class="composer-switch"></div>');
        }
        var _control_template = '<span id="vcc_toolbar">';
        _control_template += ' <span id="vcc_toolbar_paste"><a href="#" class="vcc_paste_top" title="Paste">PASTE</a><span class="vc_spacer vc-spacer"></span><a href="#" class="vcc_number_reset_top" title="Click to clear clipboard">' + this.getClipboardCount() + '</a></span>';
        _control_template += ' <span id="vcc_toolbar_impex"><a href="#" class="vcc_export" title="Export">EXPORT</a> <input type="text" class="vcc_input"> <a href="#" class="vcc_import" title="Import">IMPORT</a></span>';
        _control_template += ' <span id="vcc_toolbar_gc_load" class="vcc_container"><a href="#" class="vcc_gc_load" title="Load from Storage">LOAD</a><div class="vcc_load_dialog vcc_dialog"><div class="vcc_list"></div><img class="vcc_loader" src="'+lastudio_settings.plugins_url+'assets/images/spinner.gif"/></div></span>';
        _control_template += ' <span id="vcc_toolbar_gc_save" class="vcc_container"><a href="#" class="vcc_gc_save" title="Save to Storage">SAVE</a><div class="vcc_save_dialog vcc_dialog">Name:<br><input type="text" id="vcc_name"><br><span class="vcc_dialog_buttons"><a href="#" class="vcc_save_submit" title="Submit">SUBMIT</a> <a href="#" class="vcc_save_cancel" title="Cancel">CANCEL</a><img class="vcc_loader" src="'+lastudio_settings.plugins_url+'assets/images/spinner.gif"/></span></div></span>';
        _control_template += ' <span class="vcc_container"><a href="#" class="vcc_prefs" title="Visual Composer Clipboard Preferences">PREFERENCES</a><div class="vcc_prefs_dialog vcc_dialog"><input id="vcc_prefs_short" type="checkbox"><label for="vcc_prefs_short">Short Commands</label><br><input id="vcc_prefs_toolbar" type="checkbox"><label for="vcc_prefs_toolbar">Toolbar Initially Closed</label><br><input id="vcc_prefs_paste" type="checkbox"><label for="vcc_prefs_paste">Hide Paste Button</label><br><input id="vcc_prefs_impex" type="checkbox"><label for="vcc_prefs_impex">Hide Export/Import</label><br><input id="vcc_prefs_gc" type="checkbox"><label for="vcc_prefs_gc">Hide GC Buttons</label><br></div></span>';
        _control_template += '</span>';
        _control_template += '<span class="vcc_oc_button o"></span>';
        _control_template += '<div id="vcc_paste_indicator">Pasting, please wait...</div>';

        $('.composer-switch').append(_control_template);

    };

    VcClipBoard.prototype.bindEvent = function(){

        var _self = this;

        $(document).on('mouseover', "#visual_composer_content", function () {
            $('div.controls_row').on('mouseenter', function () {
                if ($(this).find('.vc_clipboard').length == 0) {
                    var _count = _self.getClipboardCount();
                    $(this).append(
                        '<div class="vc_clipboard" style="font-size:13px !important;">' +
                        '<span class="vcc_copy" title="Copy this row">' + _self.i18n.copy + '</span> | ' +
                        '<span class="vcc_copy_plus" title="Add to clipboard stack">' + _self.i18n.copy_plus + '</span> | ' +
                        '<span class="vcc_paste" title="Paste after this row">' + _self.i18n.paste + '</span> | ' +
                        '<span class="vcc_number_reset" title="Click to clear clipboard">' + _count + "</span>" +
                        '</div>'
                    );
                    if ($(this).height() > 22) {
                        $("div.vc_clipboard").css("line-height", $(this).height() + "px")
                    }
                    _self.handlerClick();
                }
            });

            $('div.controls_row').on('mouseleave', function () {
                $(this).find(".vc_clipboard").remove()
            });

            $('.wpb_vc_column > .controls:first-child, .wpb_vc_column > .vc_controls:first-child').on('mouseenter', function () {
                if ($(this).find('.vc_clipboard_col').length == 0) {
                    var _count = _self.getClipboardCount();
                    $(this).append(
                        '<span class="vc_clipboard_col">' +
                        '<span class="vcc_copy vcc_col" title="Copy content of this column">' + _self.i18n.copy + '</span> | ' +
                        '<span class="vcc_paste" title="Paste inside this column">' + _self.i18n.paste + '</span> | ' +
                        '<span class="vcc_number_reset" title="Click to clear clipboard">' + _count + "</span>" +
                        '</span>'
                    );
                    _self.handlerClick();
                }
            });

            $('.wpb_vc_column .controls, .wpb_vc_column .vc_controls').on('mouseleave', function () {
                $(this).find('.vc_clipboard_col').remove()
            });

            $('#visual_composer_content').on('mouseenter', '.wpb_column_container > div', function () {
                var $el = $(this).find('.vc_controls > div');
                if ($el.find('.vcc_copy').length == 0 && $(this).closest("div[data-element_type]").attr("data-model-id") != undefined) {
                    var _count = _self.getClipboardCount();
                    $el.append(
                        '<span class="vc_element-name vc_clipboard_element_root">' +
                        '<span class="vc_btn-content vc_clipboard_element">' +
                        '<span class="vcc_copy" title="Copy this element">C</span> | ' +
                        '<span class="vcc_copy_plus" title="Add to clipboard stack">C+</span> | ' +
                        '<span class="vcc_paste" title="Paste after this element">P</span> | ' +
                        '<span class="vcc_number_reset" title="Click to clear clipboard">' + _count + '</span>' +
                        '</span>' +
                        '</span>'
                    );
                    _self.handlerClick();
                }
            });

            $('#visual_composer_content').on( 'mouseleave', '.wpb_column_container > div', function () {
                $(this).find('.vc_clipboard_element_root').remove()
            })

        });
    };

    VcClipBoard.prototype.handlerClick = function(){

        var _self = this;

        $('.vcc_copy').on('click', function () {
            if (_self.getClipboardCount() > 1) {
                _self.setCache('element_param', 2, '');
                _self.setCache('shortcode_name', 2, '');
                _self.setCache('elements', 2, '');
            }
            var id = $(this).closest("div[data-element_type]").attr("data-model-id"),
                model = window.vc.app.views[id].model,
                shortcode = model.get("shortcode");

            var elements = [];

            _self.setCache('shortcode_name', 1, shortcode);
            _self.setCache('element_param', 1, model.get("params"));

            _self.$vcc_elements = [];

            _.each(window.vc.shortcodes.where({parent_id: model.id}), function (e) {
                _self.getShortcode(e, 0);
            });
            _self.setCache('elements', 1, _self.$vcc_elements);
            _self.setCache('count', '', 1);

            _self.saveCache();

            $("span.vcc_number_reset").html("1");
            $(".vcc_number_reset_top").html("1");
        });

        $('.vcc_copy_plus').on('click', function () {

            var idx = parseInt(_self.getClipboardCount()) + 1,
                id = $(this).closest("div[data-element_type]").attr("data-model-id"),
                model = window.vc.app.views[id].model,
                shortcode = model.get("shortcode");

            var cc_name = _self.getClipboardElementName(1);
            if(cc_name){
                if (shortcode == "vc_row" && cc_name != "vc_row" || shortcode != "vc_row" && cc_name == "vc_row" || shortcode == "vc_column" && cc_name != "vc_column" || shortcode != "vc_column" && cc_name == "vc_column") {
                    alert("Can't mix " + shortcode + " with " + cc_name + "!");
                    return
                }
            }

            _self.$vcc_elements = [];
            _self.setCache('shortcode_name', idx, shortcode);
            _self.setCache('element_param', idx, model.get("params"));
            _.each(window.vc.shortcodes.where({parent_id: model.id}), function (e) {
                _self.getShortcode(e, 0);
            });
            _self.setCache('elements', idx, _self.$vcc_elements);
            idx = idx + 1;
            _self.setCache('count', '', idx);
            _self.saveCache();
            $("span.vcc_number_reset").html(idx);
            $(".vcc_number_reset_top").html(idx);
        });

        $('.vcc_paste').on('click', function () {
            var view_id = -1;
            if (_self.preparePatse(this)) {
                _self.vcc_total_count = 0;
                _self.vcc_current_count = 0;
                var elem = $(this).closest("div[data-element_type]");
                $("#vcc_paste_indicator").show();

                setTimeout(function () {
                    for (var i = 1; i <= _self.getClipboardCount(); i++) {
                        var vcc_elements = _self.getClipboardElements(i);
                        _self.vcc_total_count += vcc_elements.length
                    }
                    _self.vcc_total_count = _self.vcc_total_count + parseInt(_self.getClipboardCount());
                    for (var i = 1; i <= _self.getClipboardCount(); i++) {
                        view_id = _self.handlerPaste(elem, i, view_id);
                    }
                    $("#vcc_paste_indicator").hide()
                }, 100)
            }
        });

        $('.vcc_number_reset').on('click', function(){
            _self.resetClipBoard();
        });
    };

    VcClipBoard.prototype.initCache = function(){
        var $storage = {
            count : 0,
            preferences : {
                short_command : false,
                toolbar: false,
                hide_paste_btn : false,
                hide_export_import : false,
                hide_storage : false
            },
            element_param1 : '',
            element_param2 : '',
            shortcode_name1 : '',
            shortcode_name2 : '',
            elements1 : '',
            elements2 : ''
        };
        try{
            if(localStorage.getItem('VcClipBoard') !== null){
                $storage = JSON.parse(decodeURIComponent(localStorage.getItem('VcClipBoard')));
            }
        }catch (ex) {}

        this.$storage = $storage;
    };

    VcClipBoard.prototype.getClipboardCount = function(){
        return this.$storage.count;
    };

    VcClipBoard.prototype.getClipboardElementParam = function(idx){
        return typeof this.$storage['element_param' + idx] === 'object' ? this.$storage['element_param' + idx] : {} ;
    };

    VcClipBoard.prototype.getClipboardElementName = function(idx){
        return this.$storage['shortcode_name' + idx];
    };

    VcClipBoard.prototype.getClipboardElements = function(idx){
        return typeof this.$storage['elements' + idx] === 'object' ? this.$storage['elements' + idx] : {} ;
    };

    VcClipBoard.prototype.setCache = function( key, idx, value ){
        this.$storage[key + idx] = value;
    };

    VcClipBoard.prototype.saveCache = function(){
        localStorage.setItem('VcClipBoard', JSON.stringify(this.$storage));
    };

    VcClipBoard.prototype.resetClipBoard = function(){
        var _tmp = confirm("Clear clipboard?");
        if (_tmp == true) {
            if (this.getClipboardCount() > 0) {
                for (var i = 1; i <= this.getClipboardCount(); i++) {
                    this.setCache('element_param', i, '');
                    this.setCache('shortcode_name', i, '');
                    this.setCache('elements', i, '');
                }
            }
            this.setCache('count', '', 0);
            this.saveCache();
            $("span.vcc_number_reset").html("0");
            $(".vcc_number_reset_top").html("0");
        }
    };

    VcClipBoard.prototype.getShortcode = function( $shortcode, idx ){
        var _self = this;
        var parent_id = $shortcode.attributes.parent_id;
        if (idx == 0) {
            parent_id = 0
        }
        this.$vcc_elements.push({
            sc: $shortcode.get("shortcode"),
            params: JSON.stringify($shortcode.get("params")),
            parent_id: parent_id,
            id: $shortcode.id,
            order: $shortcode.get("order")
        });
        _.each(window.vc.shortcodes.where({parent_id: $shortcode.id}), function (e) {
            _self.getShortcode(e, -1);
        });
    };

    VcClipBoard.prototype.preparePatse = function( el ){
        var shortcode_this = '',
            sc = this.getClipboardElementName(1);
        if (el != null) {
            var id_this = $(el).closest("div[data-element_type]").attr("data-model-id");
            var model_this = window.vc.app.views[id_this].model;
            shortcode_this = model_this.get("shortcode")
        }

        if ( this.getClipboardCount() == 0){
            alert("Nothing to Patse");
            return false
        }

        if (shortcode_this == "vc_row" && sc != "vc_row") {
            alert("Can't paste " + sc + " after vc_row!");
            return false
        } else if (shortcode_this == "vc_row_inner" && sc == "vc_row") {
            alert("Can't paste vc_row after vc_row_inner!");
            return false
        } else if (shortcode_this == "vc_column" && sc == "vc_row") {
            alert("Can't paste vc_row inside vc_column!");
            return false
        } else if (shortcode_this == "" && sc == "vc_row_inner") {
            alert("Can't paste vc_row_inner as root element!\n\nYou can only paste it inside a column.");
            return false
        } else if (shortcode_this == "" && sc == "vc_column") {
            alert("Can't paste vc_column content to root!\n\nYou can only paste it inside a column.");
            return false
        } else if (shortcode_this == "" && sc != "vc_row") {
            alert("Can't paste " + sc + " to root!\n\nYou can only paste it inside a column.");
            return false
        } else if (shortcode_this == "vc_row" && sc == "vc_column") {
            alert("Can't paste vc_column content after vc_row!\n\nYou can only paste it inside a column.");
            return false
        } else if (shortcode_this == "vc_row_inner" && sc == "vc_column") {
            alert("Can't paste vc_column content after vc_row_inner!\n\nTry column Paste command.");
            return false
        }
        return true
    };

    VcClipBoard.prototype.handlerPaste = function (el, idx, view_id) {
        var _self = this;
        var parent_id = false;
        var shortcode_this = "";
        if (el != null) {
            var id_this = el.attr("data-model-id");
            var model_this = window.vc.app.views[id_this].model;
            shortcode_this = model_this.get("shortcode")
        }

        if (shortcode_this != "vc_row") {
            parent_id = $(el).closest('div[data-element_type="vc_column"]').attr("data-model-id")
        }

        var new_order = -1;

        if (el != null && shortcode_this != "vc_column") {
            vc.clone_index = vc.clone_index / 10;
            if (view_id === -1) view_id = $(el).closest('div[data-element_type="' + shortcode_this + '"]').attr("data-model-id");
            var model = window.vc.app.views[view_id].model;
            new_order = parseFloat(model.get("order")) + vc.clone_index
        }

        var sc = _self.getClipboardElementName(idx);

        var a = false,
            row_id = parent_id;

        if (!(shortcode_this == "vc_column" && idx == 1 && sc == "vc_column")) {
            var params = _self.getClipboardElementParam(idx);
            row_id = vc_guid();
            if (new_order == -1) {
                window.vc.shortcodes.create({
                    shortcode: sc,
                    id: row_id,
                    parent_id: parent_id,
                    cloned: false,
                    params: params
                })
            } else {
                window.vc.shortcodes.create({
                    shortcode: sc,
                    id: row_id,
                    parent_id: parent_id,
                    order: new_order,
                    cloned: false,
                    params: params
                })
            }
            _self.vcc_current_count++;
            if (sc != "vc_row" && sc != "vc_column" && sc != "vc_row_inner") {
                _.each(window.vc.shortcodes.where({parent_id: row_id}), function ( c_el ) {
                    window.vc.app.views[c_el.id].model.destroy();
                    if (c_el.attributes.shortcode == "vc_tab") {
                        $("[data-model-id=" + row_id + "] [href=#tab-" + c_el.attributes.params.tab_id + "]").parent().remove()
                    }
                })
            }
        } else {
            row_id = parent_id;
            a = true
        }


        var vcc_elements = _self.getClipboardElements(idx);

        var uid = (new Date).getTime() + idx;

        _.each(vcc_elements, function (e) {
            e.id = e.id + uid;
            if (e.parent_id == 0) {
                e.parent_id = row_id
            } else {
                e.parent_id = e.parent_id + uid
            }
        });

        _.each(vcc_elements, function (c) {
            var params = JSON.parse(c.params);
            if (params.tab_id != undefined) {
                params.tab_id = params.tab_id + uid
            }
            var new_sc;

            if (a) {
                new_sc = window.vc.shortcodes.create({
                    shortcode: c.sc,
                    id: c.id,
                    parent_id: c.parent_id,
                    cloned: false,
                    params: params
                })
            } else {
                new_sc = window.vc.shortcodes.create({
                    shortcode: c.sc,
                    id: c.id,
                    parent_id: c.parent_id,
                    order: c.order,
                    cloned: false,
                    params: params
                })
            }
            _.each(window.vc.shortcodes.where({parent_id: c.id}), function (c_el) {
                window.vc.app.views[c_el.id].model.destroy();
                if (c_el.attributes.shortcode == "vc_tab") {
                    $("[data-model-id=" + row_id + "] [href=#tab-" + c_el.attributes.params.tab_id + "]").parent().remove()
                }
            });
            _self.vcc_current_count++
        });

        $("ul.tabs_controls").each(function () {
            $(this).prependTo($(this).parent())
        });
        return row_id
    };

    //$(window).load(function(){
    //    window.LaVcClipBoard = new VcClipBoard();
    //});

})(jQuery);

(function (e) {
    window.vcc_label_copy = "Copy";
    window.vcc_label_copy_plus = "Copy+";
    window.vcc_label_paste = "Paste";
    e(document).on("mouseover", "#visual_composer_content", function () {
        e("div.controls_row").on("mouseenter", function () {
            if (e(this).find(".vc_clipboard").length == 0) {
                window.vcc_cb_count = localStorage.getItem("vcc_cb_count");
                if (window.vcc_cb_count === null)window.vcc_cb_count = 0;
                e(this).append('<div class="vc_clipboard" style="font-size:13px !important;"><span class="vcc_copy" title="Copy this row">' + window.vcc_label_copy + '</span> | <span class="vcc_copy_plus" title="Add to clipboard stack">' + window.vcc_label_copy_plus + '</span> | <span class="vcc_paste" title="Paste after this row">' + window.vcc_label_paste + '</span> | <span class="vcc_number_reset" title="Click to clear clipboard">' + window.vcc_cb_count + "</span></div>");
                h = e(this).height();
                if (h > 22) {
                    e("div.vc_clipboard").css("line-height", h + "px")
                }
                c()
            }
        });
        e("div.controls_row").on("mouseleave", function () {
            e(this).find(".vc_clipboard").remove()
        });
        e(".wpb_vc_column > .controls:first-child, .wpb_vc_column > .vc_controls:first-child").on("mouseenter", function () {
            if (e(this).find(".vc_clipboard_col").length == 0) {
                window.vcc_cb_count = localStorage.getItem("vcc_cb_count");
                if (window.vcc_cb_count === null)window.vcc_cb_count = 0;
                e(this).append('<span class="vc_clipboard_col"><span class="vcc_copy vcc_col" title="Copy content of this column">' + window.vcc_label_copy + '</span> | <span class="vcc_paste" title="Paste inside this column">' + window.vcc_label_paste + '</span> | <span class="vcc_number_reset" title="Click to clear clipboard">' + window.vcc_cb_count + "</span></span>");
                h = e(this).height();
                c()
            }
        });
        e(".wpb_vc_column .controls, .wpb_vc_column .vc_controls").on("mouseleave", function () {
            e(this).find(".vc_clipboard_col").remove()
        });
        e("#visual_composer_content").on("mouseenter", ".wpb_column_container > div", function () {
            var t = e(this).find(".vc_controls > div");
            if (t.find(".vcc_copy").length == 0 && e(this).closest("div[data-element_type]").attr("data-model-id") != undefined) {
                window.vcc_cb_count = localStorage.getItem("vcc_cb_count");
                if (window.vcc_cb_count === null)window.vcc_cb_count = 0;
                t.append('<span class="vc_element-name vc_clipboard_element_root"><span class="vc_btn-content vc_clipboard_element"><span class="vcc_copy" title="Copy this element">C</span> | <span class="vcc_copy_plus" title="Add to clipboard stack">C+</span> | <span class="vcc_paste" title="Paste after this element">P</span> | <span class="vcc_number_reset" title="Click to clear clipboard">' + window.vcc_cb_count + "</span></span></span>");
                c()
            }
        });
        e("#visual_composer_content").on("mouseleave", ".wpb_column_container > div", function () {
            e(this).find(".vc_clipboard_element_root").remove()
        })
    });
    e(document).ready(function () {
        setTimeout(function () {
            if (e(".vcc_paste_top").length == 0) {
                t()
            }
        }, 200)
    });
    e(window).load(function () {
        setTimeout(function () {
            if (e(".vcc_paste_top").length == 0) {
                t()
            }
        }, 200)
    });
    var c = function () {
        e(".vcc_copy").on("click", function () {
            if (window.vcc_cb_count > 1) {
                for (var c = 2; c <= window.vcc_cb_count; c++) {
                    localStorage.removeItem("vcc_element_sc" + c);
                    localStorage.removeItem("vcc_element_params" + c);
                    localStorage.removeItem("vcc_elements" + c)
                }
            }
            id = e(this).closest("div[data-element_type]").attr("data-model-id");
            model = window.vc.app.views[id].model;
            shortcode = model.get("shortcode");
            window.vcc_elements = [];
            localStorage.setItem("vcc_element_sc1", shortcode);
            localStorage.setItem("vcc_element_params1", JSON.stringify(model.get("params")));
            _.each(window.vc.shortcodes.where({parent_id: model.id}), function (e) {
                n(e, 0)
            });
            localStorage.setItem("vcc_elements1", JSON.stringify(window.vcc_elements));
            localStorage.setItem("vcc_cb_count", "1");
            e("span.vcc_number_reset").html("1");
            e(".vcc_number_reset_top").html("1");
            window.vcc_cb_count = 1
        });
        e(".vcc_copy_plus").on("click", function () {
            i = parseInt(window.vcc_cb_count) + 1;
            id = e(this).closest("div[data-element_type]").attr("data-model-id");
            model = window.vc.app.views[id].model;
            shortcode = model.get("shortcode");
            var c = localStorage.getItem("vcc_element_sc1");
            if (c !== null) {
                if (shortcode == "vc_row" && c != "vc_row" || shortcode != "vc_row" && c == "vc_row" || shortcode == "vc_column" && c != "vc_column" || shortcode != "vc_column" && c == "vc_column") {
                    alert("Can't mix " + shortcode + " with " + c + "!");
                    return
                }
            }
            window.vcc_elements = [];
            localStorage.setItem("vcc_element_sc" + i, shortcode);
            localStorage.setItem("vcc_element_params" + i, JSON.stringify(model.get("params")));
            _.each(window.vc.shortcodes.where({parent_id: model.id}), function (e) {
                n(e, 0)
            });
            localStorage.setItem("vcc_elements" + i, JSON.stringify(window.vcc_elements));
            window.vcc_cb_count++;
            localStorage.setItem("vcc_cb_count", "" + window.vcc_cb_count);
            e("span.vcc_number_reset").html(window.vcc_cb_count);
            e(".vcc_number_reset_top").html(window.vcc_cb_count)
        });
        e(".vcc_paste").on("click", function () {
            var c = -1;
            if (r(this)) {
                window.vcc_total_count = 0;
                window.vcc_current_count = 0;
                var t = e(this).closest("div[data-element_type]");
                e("#vcc_paste_indicator").show();
                setTimeout(function () {
                    for (var o = 1; o <= window.vcc_cb_count; o++) {
                        vcc_elements = JSON.parse(localStorage.getItem("vcc_elements" + o));
                        window.vcc_total_count += vcc_elements.length
                    }
                    window.vcc_total_count = window.vcc_total_count + parseInt(window.vcc_cb_count);
                    for (var o = 1; o <= window.vcc_cb_count; o++) {
                        c = v(t, o, c)
                    }
                    e("#vcc_paste_indicator").hide()
                }, 100)
            }
        });
        e(".vcc_number_reset").on("click", function () {
            l()
        })
    };
    var t = function () {
        window.vcc_cb_count = localStorage.getItem("vcc_cb_count");
        if (window.vcc_cb_count === null)window.vcc_cb_count = 0;
        if (e(".composer-switch").length == 0 && e("#wpb_visual_composer").length > 0 && e("#wpb_visual_composer").is(":visible")) {
            e("#titlediv").after('<div class="composer-switch"></div>')
        }
        e(".composer-switch").append(' <span id="vcc_toolbar"><span id="vcc_toolbar_paste"><a href="#" class="vcc_paste_top" title="Paste">PASTE</a><span class="vc_spacer vc-spacer"></span><a href="#" class="vcc_number_reset_top" title="Click to clear clipboard">' + window.vcc_cb_count + '</a></span> <span id="vcc_toolbar_impex"><a href="#" class="vcc_export" title="Export">EXPORT</a> <input type="text" class="vcc_input"> <a href="#" class="vcc_import" title="Import">IMPORT</a></span> ' + '<span id="vcc_toolbar_gc_load" class="vcc_container"><a href="#" class="vcc_gc_load" title="Load from Google Cloud">GC LOAD</a>' + '<div class="vcc_load_dialog vcc_dialog"><div class="vcc_list"></div>' + '<img class="vcc_loader" src="' + lastudio_settings.plugins_url+'assets/images/spinner.gif"></span></div></span> ' + '<span id="vcc_toolbar_gc_save" class="vcc_container"><a href="#" class="vcc_gc_save" title="Save to Google Cloud">GC SAVE</a>' + '<div class="vcc_save_dialog vcc_dialog">Name:<br><input type="text" id="vcc_name"><br><span class="vcc_dialog_buttons"><a href="#" class="vcc_save_submit" title="Submit">SUBMIT</a> <a href="#" class="vcc_save_cancel" title="Cancel">CANCEL</a><img class="vcc_loader" src="' + lastudio_settings.plugins_url+'assets/images/spinner.gif"></span></div></span> ' + '<span class="vcc_container"><a href="#" class="vcc_prefs" title="Visual Composer Clipboard Preferences">VCC PREFS</a>' + '<div class="vcc_prefs_dialog vcc_dialog"><input id="vcc_prefs_short" type="checkbox"><label for="vcc_prefs_short">Short Commands</label><br><input id="vcc_prefs_toolbar" type="checkbox"><label for="vcc_prefs_toolbar">Toolbar Initially Closed</label><br><input id="vcc_prefs_paste" type="checkbox"><label for="vcc_prefs_paste">Hide Paste Button</label><br><input id="vcc_prefs_impex" type="checkbox"><label for="vcc_prefs_impex">Hide Export/Import</label><br><input id="vcc_prefs_gc" type="checkbox"><label for="vcc_prefs_gc">Hide GC Buttons</label><br></div></span>' + '</span><span class="vcc_oc_button o"></span><div id="vcc_paste_indicator">Pasting, please wait...</span></div>');
        if (localStorage.getItem("vcc_prefs_short") == "1" && e("#vcc_prefs_short").length > 0) {
            e("#vcc_prefs_short")[0].checked = true;
            window.vcc_label_copy = "C";
            window.vcc_label_copy_plus = "C+";
            window.vcc_label_paste = "P"
        }
        if (localStorage.getItem("vcc_prefs_toolbar") == "1" && e("#vcc_prefs_toolbar").length > 0) {
            e("#vcc_prefs_toolbar")[0].checked = true;
            e(".vcc_oc_button").removeClass("o");
            e(".vcc_oc_button").addClass("c");
            e("#vcc_toolbar").hide()
        }
        if (localStorage.getItem("vcc_prefs_paste") == "1" && e("#vcc_prefs_paste").length > 0) {
            e("#vcc_prefs_paste")[0].checked = true;
            e("#vcc_toolbar_paste").hide()
        }
        if (localStorage.getItem("vcc_prefs_impex") == "1" && e("#vcc_prefs_impex").length > 0) {
            e("#vcc_prefs_impex")[0].checked = true;
            e("#vcc_toolbar_impex").hide()
        }
        if (localStorage.getItem("vcc_prefs_gc") == "1" && e("#vcc_prefs_gc").length > 0) {
            e("#vcc_prefs_gc")[0].checked = true;
            e("#vcc_toolbar_gc_load").hide();
            e("#vcc_toolbar_gc_save").hide()
        }

        e("#vcc_prefs_short").on("change", function (e) {
            if (this.checked) {
                localStorage.setItem("vcc_prefs_short", "1");
                window.vcc_label_copy = "C";
                window.vcc_label_copy_plus = "C+";
                window.vcc_label_paste = "P"
            } else {
                localStorage.setItem("vcc_prefs_short", "0");
                window.vcc_label_copy = "Copy";
                window.vcc_label_copy_plus = "Copy+";
                window.vcc_label_paste = "Paste"
            }
        });
        e("#vcc_prefs_toolbar").on("change", function (e) {
            if (this.checked) {
                localStorage.setItem("vcc_prefs_toolbar", "1")
            } else {
                localStorage.setItem("vcc_prefs_toolbar", "0")
            }
        });
        e("#vcc_prefs_paste").on("change", function (c) {
            if (this.checked) {
                localStorage.setItem("vcc_prefs_paste", "1");
                e("#vcc_toolbar_paste").hide()
            } else {
                localStorage.setItem("vcc_prefs_paste", "0");
                e("#vcc_toolbar_paste").show()
            }
        });
        e("#vcc_prefs_impex").on("change", function (c) {
            if (this.checked) {
                localStorage.setItem("vcc_prefs_impex", "1");
                e("#vcc_toolbar_impex").hide()
            } else {
                localStorage.setItem("vcc_prefs_impex", "0");
                e("#vcc_toolbar_impex").show()
            }
        });
        e("#vcc_prefs_gc").on("change", function (c) {
            if (this.checked) {
                localStorage.setItem("vcc_prefs_gc", "1");
                e("#vcc_toolbar_gc_load").hide();
                e("#vcc_toolbar_gc_save").hide()
            } else {
                localStorage.setItem("vcc_prefs_gc", "0");
                e("#vcc_toolbar_gc_load").show();
                e("#vcc_toolbar_gc_save").show()
            }
        });

        e(".vcc_oc_button").on("click", function (c) {
            if (e(this).hasClass("o")) {
                e(this).removeClass("o");
                e(this).addClass("c");
                e("#vcc_toolbar").hide()
            } else {
                e(this).removeClass("c");
                e(this).addClass("o");
                e("#vcc_toolbar").show()
            }
        });
        e(".vcc_paste_top").on("click", function (c) {
            c.preventDefault();
            var t = -1;
            if (r(null)) {
                window.vcc_total_count = 0;
                window.vcc_current_count = 0;
                e("#vcc_paste_indicator").show();
                setTimeout(function () {
                    for (var c = 1; c <= window.vcc_cb_count; c++) {
                        vcc_elements = JSON.parse(localStorage.getItem("vcc_elements" + c));
                        window.vcc_total_count += vcc_elements.length
                    }
                    window.vcc_total_count = window.vcc_total_count + parseInt(window.vcc_cb_count);
                    for (var c = 1; c <= window.vcc_cb_count; c++) {
                        t = v(null, c, t)
                    }
                    e("#vcc_paste_indicator").hide()
                }, 100)
            }
        });
        e(".vcc_input, #vcc_name, #vcc_username, #vcc_api_key, #vcc_license_key").keypress(function (c) {
            if (c && c.which === e.ui.keyCode.ENTER) {
                c.preventDefault()
            }
        });
        e(".vcc_number_reset_top").on("click", function (e) {
            e.preventDefault();
            l()
        });
        e(".vcc_import").on("click", function (c) {
            c.preventDefault();
            if (e(".vcc_input").val()) {
                s(e(".vcc_input").val())
            }
        });
        e(".vcc_export").on("click", function (c) {
            c.preventDefault();
            if (window.vcc_cb_count > 0) {
                var t = "[";
                for (var o = 1; o <= window.vcc_cb_count; o++) {
                    t += '{"vcc_element_sc":"' + window.btoa(encodeURIComponent(localStorage.getItem("vcc_element_sc" + o))) + '",';
                    t += '"vcc_element_params":"' + window.btoa(encodeURIComponent(localStorage.getItem("vcc_element_params" + o))) + '",';
                    t += '"vcc_elements":"' + window.btoa(encodeURIComponent(localStorage.getItem("vcc_elements" + o))) + '"},'
                }
                t = t.substring(0, t.length - 1);
                t += "]";
                e(".vcc_input").val(window.btoa(encodeURIComponent(t)));
                e(".vcc_input").select()
            }
        });
        e(".vcc_gc_load").on("click", function (c) {
            c.preventDefault();
            if (e(".vcc_load_dialog").css("visibility") == "visible") {
                e(".vcc_load_dialog").css("visibility", "hidden");
                return false
            }

            e(".vcc_dialog").css("visibility", "hidden");
            e(".vcc_load_dialog").css("visibility", "visible");
            e(".vcc_load_dialog .vcc_list").html("");
            e(".vcc_load_dialog .vcc_loader").show();
            e.ajax({
                url: lastudio_settings.ajax_url,
                data: {
                    action: 'la-vc-ajax',
                    la_vc_action: 'load_list'
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError);
                    e(".vcc_load_dialog").css("visibility", "hidden");
                    alert("Error 01. Please try later.")
                },
                dataType: "json",
                success: function (c) {
                    e(".vcc_load_dialog .vcc_loader").hide();
                    if (c.length == 0) {
                        e(".vcc_load_dialog").css("visibility", "hidden");
                        alert("No saved templates.")
                    } else {
                        for (i = 0; i < c.length; i++) {
                            e(".vcc_load_dialog .vcc_list").append('<div class="vcc_list_item_container"><div class="vcc_list_item" title="Load ' + c[i].name + ' "data-item="' + c[i].name + '">' + c[i].name + '</div><div class="vcc_list_item_delete" title="Delete ' + c[i].name + '" data-item="' + c[i].name + '"></div></div>')
                        }
                        a()
                    }
                },
                type: "GET"
            })

        });
        e(".vcc_gc_save").on("click", function (c) {
            c.preventDefault();
            if (e(".vcc_save_dialog").css("visibility") == "visible") {
                e(".vcc_save_dialog").css("visibility", "hidden");
                return false
            }
            e(".vcc_dialog").css("visibility", "hidden");
            e(".vcc_save_dialog").css("visibility", "visible")
        });
        e(".vcc_save_submit").on("click", function (c) {
            c.preventDefault();
            var name = e("#vcc_name").val();
            if (name == "") {
                alert("Enter template name.");
                return false
            }
            if (window.vcc_cb_count > 0) {
                var t = "[";
                for (var a = 1; a <= window.vcc_cb_count; a++) {
                    t += '{"vcc_element_sc":"' + window.btoa(encodeURIComponent(localStorage.getItem("vcc_element_sc" + a))) + '",';
                    t += '"vcc_element_params":"' + window.btoa(encodeURIComponent(localStorage.getItem("vcc_element_params" + a))) + '",';
                    t += '"vcc_elements":"' + window.btoa(encodeURIComponent(localStorage.getItem("vcc_elements" + a))) + '"},'
                }
                t = t.substring(0, t.length - 1);
                t += "]";
                var clipboard = window.btoa(encodeURIComponent(t));

                e(".vcc_save_submit").hide();
                e(".vcc_save_cancel").hide();
                e(".vcc_save_dialog .vcc_loader").show();

                e.ajax({
                    url: lastudio_settings.ajax_url,
                    data: {
                        'action': 'la-vc-ajax',
                        'la_vc_action': 'save',
                        'clipboard': clipboard,
                        'name': name
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        e(".vcc_save_submit").show();
                        e(".vcc_save_cancel").show();
                        e(".vcc_save_dialog .vcc_loader").hide();
                        alert("Error 03. Please try later.")
                    },
                    dataType: "json",
                    success: function (c) {
                        e(".vcc_save_submit").show();
                        e(".vcc_save_cancel").show();
                        e(".vcc_save_dialog .vcc_loader").hide();
                        if (c.message == "ok") {
                            e("#vcc_name").val("");
                            e(".vcc_save_dialog").css("visibility", "hidden");
                            alert("Clipboard template saved.")
                        } else if (c.message == "exists") {
                            alert("Name already taken.")
                        } else {
                            alert("Error 04. Please try later.")
                        }
                    },
                    type: "POST"
                })

            } else {
                alert("Nothing to save.")
            }
        });
        e(".vcc_save_cancel").on("click", function (c) {
            c.preventDefault();
            e(".vcc_save_dialog").css("visibility", "hidden")
        });
        e(".vcc_gc_settings").on("click", function (c) {
            c.preventDefault();
            o();
            e(".vcc_dialog").css("visibility", "hidden");
        });

        e(".vcc_gc_settings_cancel").on("click", function (c) {
            c.preventDefault();
        });
        e(".vcc_prefs").on("click", function (c) {
            c.preventDefault();
            if (e(".vcc_prefs_dialog").css("visibility") == "visible") {
                e(".vcc_prefs_dialog").css("visibility", "hidden");
                return false
            }
            o();
            e(".vcc_dialog").css("visibility", "hidden");
            e(".vcc_prefs_dialog").css("visibility", "visible")
        })
    };
    var o = function () {

    };
    var a = function () {
        e(".vcc_list_item").on("click", function (c) {
            var t = e(this).data("item");
            e(".vcc_dialog").css("visibility", "hidden");
            e(".vcc_load_dialog").css("visibility", "visible");
            e(".vcc_load_dialog .vcc_list").html("");
            e(".vcc_load_dialog .vcc_loader").show();
            e.ajax({
                url: lastudio_settings.ajax_url,
                data: {
                    action: 'la-vc-ajax',
                    la_vc_action: 'load_item',
                    item_to_load: t
                },
                error: function () {
                    e(".vcc_load_dialog").css("visibility", "hidden");
                    alert("Error 07. Please try later.")
                },
                dataType: "json",
                success: function (c) {
                    e(".vcc_load_dialog .vcc_loader").hide();
                    e(".vcc_load_dialog").css("visibility", "hidden");
                    if (c.message == "error") {
                        alert("Error 08. Please try later.")
                    } else {
                        s(c)
                    }
                },
                type: "GET"
            })
        });
        e(".vcc_list_item_delete").on("click", function (c) {
            var t = e(this).data("item");
            e(".vcc_dialog").css("visibility", "hidden");
            e(".vcc_load_dialog").css("visibility", "visible");
            e(".vcc_load_dialog .vcc_list").html("");
            e(".vcc_load_dialog .vcc_loader").show();
            e.ajax({
                url: lastudio_settings.ajax_url,
                data: {
                    action: 'la-vc-ajax',
                    la_vc_action: 'load_list',
                    item_to_delete: t
                },
                error: function () {
                    e(".vcc_load_dialog").css("visibility", "hidden");
                    alert("Error 09. Please try later.")
                },
                dataType: "json",
                success: function (c) {
                    e(".vcc_load_dialog .vcc_loader").hide();
                    if (c.length == 0) {
                        e(".vcc_load_dialog").css("visibility", "hidden")
                    } else {
                        for (i = 0; i < c.length; i++) {
                            e(".vcc_load_dialog .vcc_list").append('<div class="vcc_list_item_container"><div class="vcc_list_item" title="Load ' + c[i].name + ' "data-item="' + c[i].name + '">' + c[i].name + '</div><div class="vcc_list_item_delete" title="Delete ' + c[i].name + '" data-item="' + c[i].name + '"></div></div>')
                        }
                        a()
                    }
                },
                type: "GET"
            })
        })
    };
    var s = function (c) {
        var t = decodeURIComponent(window.atob(c));
        var o = JSON.parse(t);
        for (i = 1; i <= o.length; i++) {
            localStorage.setItem("vcc_element_sc" + i, decodeURIComponent(window.atob(o[i - 1].vcc_element_sc)));
            localStorage.setItem("vcc_element_params" + i, decodeURIComponent(window.atob(o[i - 1].vcc_element_params)));
            localStorage.setItem("vcc_elements" + i, decodeURIComponent(window.atob(o[i - 1].vcc_elements)));
            window.vcc_cb_count = o.length;
            localStorage.setItem("vcc_cb_count", "" + window.vcc_cb_count);
            e("span.vcc_number_reset").html(window.vcc_cb_count);
            e(".vcc_number_reset_top").html(window.vcc_cb_count)
        }
    };
    var l = function () {
        var c = confirm("Clear clipboard?");
        if (c == true) {
            if (window.vcc_cb_count > 0) {
                for (var t = 1; t <= window.vcc_cb_count; t++) {
                    localStorage.removeItem("vcc_element_sc" + t);
                    localStorage.removeItem("vcc_element_params" + t);
                    localStorage.removeItem("vcc_elements" + t)
                }
            }
            localStorage.setItem("vcc_cb_count", "0");
            e("span.vcc_number_reset").html("0");
            e(".vcc_number_reset_top").html("0");
            window.vcc_cb_count = 0
        }
    };
    var n = function (e, c) {
        pid = e.attributes.parent_id;
        if (c == 0) {
            pid = 0
        }
        window.vcc_elements.push({
            sc: e.get("shortcode"),
            params: JSON.stringify(e.get("params")),
            parent_id: pid,
            id: e.id,
            order: e.get("order")
        });
        _.each(window.vc.shortcodes.where({parent_id: e.id}), function (e) {
            n(e, -1)
        })
    };
    var r = function (c) {
        if (c != null) {
            id_this = e(c).closest("div[data-element_type]").attr("data-model-id");
            model_this = window.vc.app.views[id_this].model;
            shortcode_this = model_this.get("shortcode")
        } else {
            shortcode_this = ""
        }
        sc = localStorage.getItem("vcc_element_sc1");
        if (shortcode_this == "vc_row" && sc != "vc_row") {
            alert("Can't paste " + sc + " after vc_row!");
            return false
        } else if (shortcode_this == "vc_row_inner" && sc == "vc_row") {
            alert("Can't paste vc_row after vc_row_inner!");
            return false
        } else if (shortcode_this == "vc_column" && sc == "vc_row") {
            alert("Can't paste vc_row inside vc_column!");
            return false
        } else if (shortcode_this == "" && sc == "vc_row_inner") {
            alert("Can't paste vc_row_inner as root element!\n\nYou can only paste it inside a column.");
            return false
        } else if (shortcode_this == "" && sc == "vc_column") {
            alert("Can't paste vc_column content to root!\n\nYou can only paste it inside a column.");
            return false
        } else if (shortcode_this == "" && sc != "vc_row") {
            alert("Can't paste " + sc + " to root!\n\nYou can only paste it inside a column.");
            return false
        } else if (shortcode_this == "vc_row" && sc == "vc_column") {
            alert("Can't paste vc_column content after vc_row!\n\nYou can only paste it inside a column.");
            return false
        } else if (shortcode_this == "vc_row_inner" && sc == "vc_column") {
            alert("Can't paste vc_column content after vc_row_inner!\n\nTry column Paste command.");
            return false
        }
        return true
    };
    var v = function (c, t, o) {
        parent_id = false;
        if (c != null) {
            id_this = c.attr("data-model-id");
            model_this = window.vc.app.views[id_this].model;
            shortcode_this = model_this.get("shortcode")
        } else {
            shortcode_this = ""
        }
        if (shortcode_this != "vc_row") {
            parent_id = e(c).closest('div[data-element_type="vc_column"]').attr("data-model-id")
        }
        if (c != null && shortcode_this != "vc_column") {
            vc.clone_index = vc.clone_index / 10;
            if (o === -1)o = e(c).closest('div[data-element_type="' + shortcode_this + '"]').attr("data-model-id");
            model = window.vc.app.views[o].model;
            new_order = parseFloat(model.get("order")) + vc.clone_index
        } else {
            new_order = -1
        }
        sc = localStorage.getItem("vcc_element_sc" + t);
        var a = false;
        if (!(shortcode_this == "vc_column" && t == 1 && sc == "vc_column")) {
            params = JSON.parse(localStorage.getItem("vcc_element_params" + t));
            row_id = vc_guid();
            if (new_order == -1) {
                window.vc.shortcodes.create({
                    shortcode: sc,
                    id: row_id,
                    parent_id: parent_id,
                    cloned: false,
                    params: params
                })
            } else {
                window.vc.shortcodes.create({
                    shortcode: sc,
                    id: row_id,
                    parent_id: parent_id,
                    order: new_order,
                    cloned: false,
                    params: params
                })
            }
            window.vcc_current_count++;
            if (sc != "vc_row" && sc != "vc_column" && sc != "vc_row_inner") {
                _.each(window.vc.shortcodes.where({parent_id: row_id}), function (c) {
                    window.vc.app.views[c.id].model.destroy();
                    if (c.attributes.shortcode == "vc_tab") {
                        e("[data-model-id=" + row_id + "] [href=#tab-" + c.attributes.params.tab_id + "]").parent().remove()
                    }
                })
            }
        } else {
            row_id = parent_id;
            a = true
        }
        vcc_elements = JSON.parse(localStorage.getItem("vcc_elements" + t));
        uid = (new Date).getTime() + t;
        _.each(vcc_elements, function (e) {
            e.id = e.id + uid;
            if (e.parent_id == 0) {
                e.parent_id = row_id
            } else {
                e.parent_id = e.parent_id + uid
            }
        });
        _.each(vcc_elements, function (c) {
            params = JSON.parse(c.params);
            if (params.tab_id != undefined) {
                params.tab_id = params.tab_id + uid
            }
            if (a) {
                new_sc = window.vc.shortcodes.create({
                    shortcode: c.sc,
                    id: c.id,
                    parent_id: c.parent_id,
                    cloned: false,
                    params: params
                })
            } else {
                new_sc = window.vc.shortcodes.create({
                    shortcode: c.sc,
                    id: c.id,
                    parent_id: c.parent_id,
                    order: c.order,
                    cloned: false,
                    params: params
                })
            }
            _.each(window.vc.shortcodes.where({parent_id: c.id}), function (c) {
                window.vc.app.views[c.id].model.destroy();
                if (c.attributes.shortcode == "vc_tab") {
                    e("[data-model-id=" + row_id + "] [href=#tab-" + c.attributes.params.tab_id + "]").parent().remove()
                }
            });
            window.vcc_current_count++
        });
        e("ul.tabs_controls").each(function () {
            e(this).prependTo(e(this).parent())
        });
        return row_id
    }
})(jQuery);