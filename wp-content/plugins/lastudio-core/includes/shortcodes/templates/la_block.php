<?php

$id = $name = $el_class = '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );

extract($atts);

if ($id || $name) {
    $el_class = $this->getExtraClass( $el_class );
    $query_args = array( 'post_type' => 'la_block' );
    if ($id){
        $query_args['p'] = (int) $id;
    }
    if ($name){
        $query_args['name'] = $name;
    }

    $the_query = new WP_Query($query_args);

    if ($the_query->have_posts()) {
        while($the_query->have_posts()){
            $the_query->the_post();
            wp_enqueue_script('wpb_composer_front_js');
            $wpb_post_custom_css = get_post_meta( get_the_ID(), '_wpb_post_custom_css', true );
            $wpb_shortcodes_custom_css = get_post_meta( get_the_ID(), '_wpb_shortcodes_custom_css', true );
        ?>
        <div class="la-static-block<?php echo esc_attr($el_class)?>">
            <?php the_content();?>
        </div>
        <?php if($wpb_post_custom_css || $wpb_shortcodes_custom_css):?>
            <span data-la_component="InsertCustomCSS" class="js-el hidden"><?php echo $wpb_post_custom_css . $wpb_shortcodes_custom_css;?></span>
        <?php endif;?>
        <?php
        }
    }

    wp_reset_postdata();
}