<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_pisces_preset_demo_19(){
    return array(

        /**
         * Settings
         */

        array(
            'key' => 'header_layout',
            'value' => '7'
        ),
        array(
            'key' => 'header_full_width',
            'value' => 'yes'
        ),
        array(
            'key' => 'header_transparency',
            'value' => 'yes'
        ),
        array(
            'key' => 'enable_header_top',
            'value' => 'hide'
        ),
        array(
            'key' => 'header_access_icon',
            'value' => array()
        ),

        array(
            'key' => 'logo',
            'value' => '1581'
        ),
        array(
            'key' => 'logo_2x',
            'value' => '1582'
        ),
        array(
            'key' => 'logo_transparency',
            'value' => '1581'
        ),
        array(
            'key' => 'logo_transparency_2x',
            'value' => '1582'
        ),
        array(
            'key' => 'logo_mobile',
            'value' => '1581'
        ),
        array(
            'key' => 'logo_mobile_2x',
            'value' => '1582'
        ),

        array(
            'key' => 'main_font',
            'value' => array(
                'family' => 'Roboto Slab',
                'variant' => array(
                    300,
                    400,
                    700
                ),
                'font' => 'google'
            )
        ),
        array(
            'key' => 'secondary_font',
            'value' => array(
                'family' => 'Meddon',
                'variant' => array(
                    400
                ),
                'font' => 'google'
            )
        ),

        /**
         * Filters
         */

        array(
            'filter_name' => 'pisces/filter/header_sidebar_widget_bottom',
            'value' => 'demo-19-header-bottom'
        ),

        /**
         * Colors
         */


        array(
            'key' => 'offcanvas_background|header_mb_background|mb_background',
            'value' => '#232324'
        ),
        array(
            'key' => 'offcanvas_text_color|offcanvas_link_color|header_mb_text_color',
            'value' => '#909090'
        ),
        array(
            'key' => 'offcanvas_heading_color',
            'value' => '#fff'
        ),
        array(
            'key' => 'primary_color|offcanvas_link_hover_color',
            'value' => '#c19c83'
        ),
        array(
            'key' => 'mb_lv_1_color|mb_lv_1_hover_bg_color|mb_lv_2_color|mb_lv_2_hover_bg_color',
            'value' => '#c19c83'
        ),
        array(
            'key' => 'mb_lv_1_hover_color|mb_lv_2_hover_color',
            'value' => '#fff'
        )
    );
}