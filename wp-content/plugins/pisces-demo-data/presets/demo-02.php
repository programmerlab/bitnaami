<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_pisces_preset_demo_02(){
    return array(

        /**
         * Settings
         */

        array(
            'key' => 'header_transparency',
            'value' => 'yes'
        ),


        /**
         * Filters
         */



        /**
         * Colors
         */


    );
}