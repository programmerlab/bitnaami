<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_pisces_preset_demo_12(){
    return array(

        /**
         * Settings
         */

        array(
            'key' => 'header_layout',
            'value' => '5'
        ),
        array(
            'key' => 'header_full_width',
            'value' => 'yes'
        ),
        array(
            'key' => 'header_transparency',
            'value' => 'no'
        ),

        array(
            'key' => 'header_access_icon',
            'value' => array()
        ),

        array(
            'key' => 'enable_header_top',
            'value' => 'hide'
        ),


        /**
         * Filters
         */



        /**
         * Colors
         */


        array(
            'key' => 'la_custom_css',
            'value' => '

            '
        ),

    );
}