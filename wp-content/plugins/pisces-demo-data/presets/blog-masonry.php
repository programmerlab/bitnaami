<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_pisces_preset_blog_masonry(){
    return array(

        /**
         * Settings
         */

        array(
            'key' => 'layout_blog',
            'value' => 'col-1c'
        ),
        array(
            'key' => 'main_full_width',
            'value' => 'yes'
        ),


        array(
            'key' => 'main_space',
            'value' => array(
                'top' => 40,
                'bottom' => 100
            )
        ),

        array(
            'key' => 'blog_design',
            'value' => 'grid_6'
        ),
        array(
            'key' => 'blog_post_column',
            'value' => array(
                'xlg'   => '4',
                'lg'    => '4',
                'md'    => '3',
                'sm'    => '2',
                'xs'    => '2',
                'mb'    => '1'
            )
        ),
        array(
            'key' => 'blog_masonry',
            'value' => 'on'
        ),

        array(
            'key' => 'blog_item_space',
            'value' => '20'
        ),

        array(
            'key' => 'blog_thumbnail_size',
            'value' => '425x0'
        ),
        array(
            'key' => 'blog_excerpt_length',
            'value' => '15'
        ),
        array(
            'key' => 'blog_pagination_type',
            'value' => 'load_more'
        ),

        /**
         * Filters
         */

        array(
            'filter_name' => 'pisces/filter/page_title',
            'value' => '<header><h3 class="page-title">Blog Masonry</h3></header>'
        ),

        array(
            'filter_name' => 'pisces/setting/get_setting_by_context',
            'filter_func' => function( $value, $key, $context ){
                if($key == 'page_title_bar_background'){
                    $value = array(
                        'image'     => 'http://pisces.la-studioweb.com/wp-content/uploads/2017/09/blog-page-header-05.jpg',
                        'repeat'    => 'no-repeat',
                        'size'      => 'cover',
                        'position'  => 'center center'
                    );
                }
                return $value;
            },
            'filter_priority'  => 10,
            'filter_args'  => 3
        ),
        /**
         * Colors
         */

        array(
            'key' => 'la_custom_css',
            'value' => '
                .site-main{
                    background-color: #f7f7f7
                }
                #blog_content_container .la-loop{
                    margin-bottom: 80px;
                }
                @media(min-width: 1300px) {
                    .site-main > .container {
                        padding-left: 40px;
                        padding-right: 40px
                    }
                }
                @media(min-width: 1500px) {
                    .site-main > .container {
                        padding-left: 80px;
                        padding-right: 80px
                    }
                }
            '
        )
    );
}