<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_pisces_preset_demo_04(){
    return array(

        /**
         * Settings
         */

        array(
            'key' => 'header_layout',
            'value' => '4'
        ),
        array(
            'key' => 'header_full_width',
            'value' => 'yes'
        ),
        array(
            'key' => 'header_transparency',
            'value' => 'yes'
        ),

        array(
            'key' => 'logo_transparency',
            'value' => '1431'
        ),
        array(
            'key' => 'logo_transparency_2x',
            'value' => '1432'
        ),

        array(
            'key' => 'header_access_icon',
            'value' => array(
                array(
                    'type' => 'aside_header',
                    'icon' => 'pisces-icon-menu2',
                    'el_class' => ''
                )
            )
        ),

        /**
         * Filters
         */

        array(
            'filter_name' => 'pisces/filter/footer_column_4',
            'value' => 'demo-03-footer-column-4'
        ),

        /**
         * Colors
         */


        array(
            'key' => 'la_custom_css',
            'value' => '
                .site-header .la_com_action--aside_header > .component-target {
                    color: #fff !important;
                    background-color: #343538 !important;
                    border-radius: 50%;
                    width: 54px !important;
                    height: 54px !important;
                    line-height: 54px !important;
                    font-size: 24px !important;
                    margin-top: -7px;
                    margin-bottom: -7px;
                }
                .site-header .la_com_action--aside_header:hover > .component-target{
                    color: #fff !important;
                    background-color: #6b56e2 !important;
                }
                @media (min-width: 1024px) {
                    .footer-column.footer-column-2 {
                        width: 16.66667%;
                    }
                    .footer-column.footer-column-4 {
                        width: 33.33333%;
                    }
                }
            '
        )
    );
}