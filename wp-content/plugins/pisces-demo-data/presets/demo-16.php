<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_pisces_preset_demo_16(){
    return array(

        /**
         * Settings
         */

        array(
            'key' => 'header_layout',
            'value' => '4'
        ),
        array(
            'key' => 'header_full_width',
            'value' => 'no'
        ),
        array(
            'key' => 'header_transparency',
            'value' => 'no'
        ),
        array(
            'key' => 'header_sticky',
            'value' => 'no'
        ),
        array(
            'key' => 'enable_header_top',
            'value' => 'hide'
        ),
        array(
            'key' => 'header_access_icon',
            'value' => array(
                array(
                    'type' => 'aside_header',
                    'icon' => 'pisces-icon-menu',
                    'el_class' => ''
                )
            )
        ),

        /**
         * Filters
         */


        /**
         * Colors
         */

        array(
            'key' => 'transparency_header_text_color|transparency_header_link_color|transparency_mm_lv_1_color',
            'value' => '#fff'
        ),
        array(
            'key' => 'transparency_header_link_hover_color|transparency_mm_lv_1_hover_color',
            'value' => '#fff'
        ),
    );
}