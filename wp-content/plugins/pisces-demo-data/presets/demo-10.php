<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_pisces_preset_demo_10(){
    return array(

        /**
         * Settings
         */

        array(
            'key' => 'header_layout',
            'value' => '4'
        ),
        array(
            'key' => 'header_full_width',
            'value' => 'yes'
        ),
        array(
            'key' => 'header_transparency',
            'value' => 'yes'
        ),


        array(
            'key' => 'header_access_icon',
            'value' => array(
                array(
                    'type' => 'aside_header',
                    'icon' => 'pisces-icon-menu2',
                    'el_class' => ''
                )
            )
        ),

        array(
            'key' => 'footer_layout',
            'value' => '1col'
        ),
        array(
            'key' => 'enable_footer_copyright',
            'value' => 'no'
        ),

        /**
         * Filters
         */

        array(
            'filter_name' => 'pisces/filter/footer_column_1',
            'value' => 'demo-10-footer-column-1'
        ),

        /**
         * Colors
         */


        array(
            'key' => 'la_custom_css',
            'value' => '
                .site-header .la_com_action--aside_header > .component-target {
                    font-size: 24px !important;
                }
            '
        ),

        array(
            'key' => 'footer_space',
            'value' => array(
                'padding_top' => '50px',
                'padding_bottom' => '35px'
            )
        ),

        array(
            'key' => 'footer_background',
            'value' => array(
                'color' => '#fff'
            )
        ),
        array(
            'key' => 'footer_text_color',
            'value' => '#656565'
        ),
        array(
            'key' => 'footer_link_color',
            'value' => '#232324'
        ),
        array(
            'key' => 'footer_link_hover_color',
            'value' => '#6b56e2'
        )
    );
}