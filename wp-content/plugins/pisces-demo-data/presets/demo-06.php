<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_pisces_preset_demo_06(){
    return array(

        /**
         * Settings
         */

        array(
            'key' => 'header_layout',
            'value' => '2'
        ),
        array(
            'key' => 'header_full_width',
            'value' => 'yes'
        ),
        array(
            'key' => 'header_transparency',
            'value' => 'yes'
        ),


        /**
         * Filters
         */

        array(
            'filter_name' => 'pisces/filter/footer_column_4',
            'value' => 'demo-03-footer-column-4'
        ),

        /**
         * Colors
         */

        array(
            'key' => 'mm_lv_1_hover_color|header_link_hover_color|primary_color',
            'value' => '#66c8f7'
        ),

        array(
            'key' => 'la_custom_css',
            'value' => '
                @media (min-width: 1024px) {
                    .footer-column.footer-column-2 {
                        width: 16.66667%;
                    }
                    .footer-column.footer-column-4 {
                        width: 33.33333%;
                    }
                }
            '
        )
    );
}