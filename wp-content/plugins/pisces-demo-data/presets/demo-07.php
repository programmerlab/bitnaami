<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_pisces_preset_demo_07(){
    return array(

        /**
         * Settings
         */

        array(
            'key' => 'header_layout',
            'value' => '3'
        ),
        array(
            'key' => 'header_full_width',
            'value' => 'yes'
        ),
        array(
            'key' => 'header_transparency',
            'value' => 'yes'
        ),


        /**
         * Filters
         */


        /**
         * Colors
         */

        array(
            'key' => 'transparency_header_link_hover_color|mm_lv_1_hover_color|header_link_hover_color|primary_color',
            'value' => '#66c8f7'
        )
    );
}