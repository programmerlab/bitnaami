<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_pisces_preset_demo_03(){
    return array(

        /**
         * Settings
         */

        array(
            'key' => 'header_layout',
            'value' => '1'
        ),
        array(
            'key' => 'header_full_width',
            'value' => 'no'
        ),
        array(
            'key' => 'header_transparency',
            'value' => 'no'
        ),


        /**
         * Filters
         */

        array(
            'filter_name' => 'pisces/filter/footer_column_4',
            'value' => 'demo-03-footer-column-4'
        ),

        /**
         * Colors
         */

        array(
            'key' => 'mm_lv_1_hover_color',
            'value' => '#fff'
        ),
        array(
            'key' => 'mm_lv_1_hover_bg_color|header_link_hover_color|primary_color',
            'value' => '#526df9'
        ),

        array(
            'key' => 'la_custom_css',
            'value' => '
                .site-main-nav .main-menu > li > a:after{
                    display: none;
                }
                @media (min-width: 1024px) {
                    .footer-column.footer-column-2 {
                        width: 16.66667%;
                    }
                    .footer-column.footer-column-4 {
                        width: 33.33333%;
                    }
                }
            '
        )
    );
}