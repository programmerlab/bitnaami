<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_pisces_preset_shop_02_columns(){
    return array(

        /**
         * Settings
         */

        array(
            'key' => 'layout_archive_product',
            'value' => 'col-1c'
        ),

        array(
            'key' => 'main_full_width',
            'value' => 'no'
        ),


        array(
            'key'   => 'woocommerce_shop_page_columns',
            'value' => array(
                'xlg' => 2,
                'lg' => 2,
                'md' => 2,
                'sm' => 2,
                'xs' => 2,
                'mb' => 1
            )
        ),

        array(
            'key' => 'product_per_page_allow',
            'value' => '6,12,18'
        ),
        array(
            'key' => 'product_per_page_default',
            'value' => 6
        ),

        array(
            'key' => 'enable_page_title_subtext',
            'value' => 'yes'
        ),

        array(
            'key' => 'page_title_custom_subtext',
            'value' => 'Nullam varius porttitor augue id rutrum duis vehicula'
        ),
        /**
         * Filters
         */

        array(
            'filter_name' => 'pisces/filter/page_title',
            'value' => '<header><h3 class="page-title">Shop 02 Columns</h3></header>'
        ),

        array(
            'filter_name' => 'pisces/filter/product_per_page_array',
            'filter_func' => function( $value ){
                return '6,12,18';
            },
            'filter_priority'  => 99,
            'filter_args'  => 1
        ),

        /**
         * Colors
         */
    );
}