<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_pisces_preset_shop_masonry_01(){
    return array(

        /**
         * Settings
         */

        array(
            'key' => 'layout_archive_product',
            'value' => 'col-1c'
        ),

        array(
            'key' => 'active_shop_filter',
            'value' => 'on'
        ),

        array(
            'key' => 'woocommerce_toggle_grid_list',
            'value' => 'off'
        ),

        array(
            'key' => 'active_shop_masonry',
            'value' => 'on'
        ),

        array(
            'key'   => 'woocommerce_shop_masonry_columns',
            'value' => array(
                'xlg' => 3,
                'lg' => 3,
                'md' => 3,
                'sm' => 2,
                'xs' => 2,
                'mb' => 1
            )
        ),


        array(
            'key' => 'enable_page_title_subtext',
            'value' => 'yes'
        ),

        array(
            'key' => 'page_title_custom_subtext',
            'value' => 'Nullam varius porttitor augue id rutrum duis vehicula'
        ),
        /**
         * Filters
         */

        array(
            'filter_name' => 'pisces/filter/page_title',
            'value' => '<header><h3 class="page-title">Shop Masonry</h3></header>'
        ),

        /**
         * Colors
         */

    );
}