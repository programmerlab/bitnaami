<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_pisces_preset_shop_masonry_02(){
    return array(

        /**
         * Settings
         */

        array(
            'key' => 'layout_archive_product',
            'value' => 'col-1c'
        ),

        array(
            'key' => 'active_shop_filter',
            'value' => 'on'
        ),

        array(
            'key' => 'main_full_width',
            'value' => 'yes'
        ),

        array(
            'key' => 'woocommerce_toggle_grid_list',
            'value' => 'off'
        ),
        array(
            'key' => 'woocommerce_enable_crossfade_effect',
            'value' => 'off'
        ),

        array(
            'key' => 'active_shop_masonry',
            'value' => 'on'
        ),

        array(
            'key' => 'shop_item_space',
            'value' => '20'
        ),

        array(
            'key' => 'shop_catalog_grid_style',
            'value' => '2'
        ),

        array(
            'key'   => 'woocommerce_shop_masonry_columns',
            'value' => array(
                'xlg' => 4,
                'lg' => 4,
                'md' => 3,
                'sm' => 2,
                'xs' => 2,
                'mb' => 1
            )
        ),

        array(
            'key' => 'enable_page_title_subtext',
            'value' => 'yes'
        ),

        array(
            'key' => 'page_title_custom_subtext',
            'value' => 'Nullam varius porttitor augue id rutrum duis vehicula'
        ),

        /**
         * Filters
         */

        array(
            'filter_name' => 'pisces/filter/page_title',
            'value' => '<header><h3 class="page-title">Shop Masonry</h3></header>'
        ),

        /**
         * Colors
         */

        array(
            'key' => 'la_custom_css',
            'value' => '
                @media (min-width: 1300px) {
                    .site-main > .container{
                        padding-left: 40px;
                        padding-right: 40px;
                    }
                }
                @media (min-width: 1400px) {
                    .site-main > .container{
                        padding-left: 60px;
                        padding-right: 60px;
                    }
                }
                @media (min-width: 1500px) {
                    .site-main > .container{
                        padding-left: 80px;
                        padding-right: 80px;
                    }
                }
            '
        )
    );
}