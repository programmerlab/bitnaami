<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_pisces_preset_demo_08(){
    return array(

        /**
         * Settings
         */

        array(
            'key' => 'header_layout',
            'value' => '2'
        ),
        array(
            'key' => 'header_full_width',
            'value' => 'yes'
        ),
        array(
            'key' => 'header_transparency',
            'value' => 'no'
        ),

        array(
            'key' => 'logo',
            'value' => '1571'
        ),
        array(
            'key' => 'logo_2x',
            'value' => '1572'
        ),
        array(
            'key' => 'logo_transparency',
            'value' => '1571'
        ),
        array(
            'key' => 'logo_transparency_2x',
            'value' => '1572'
        ),
        array(
            'key' => 'logo_mobile',
            'value' => '1571'
        ),
        array(
            'key' => 'logo_mobile_2x',
            'value' => '1572'
        ),

        array(
            'key' => 'header_access_icon',
            'value' => array(
                array(
                    'type' => 'aside_header',
                    'el_class' => ''
                ),
                array(
                    'type' => 'search_1',
                    'el_class' => ''
                ),
                array(
                    'type' => 'link_text',
                    'icon' => '',
                    'text' => 'GET A QUOTE',
                    'link' => '#',
                    'el_class' => 'header-custom-btn'
                )
            )
        ),

        /**
         * Filters
         */

        array(
            'filter_name' => 'pisces/filter/footer_column_4',
            'value' => 'demo-08-footer-column-4'
        ),
        /**
         * Colors
         */

        array(
            'key' => 'transparency_header_link_hover_color|mm_lv_1_hover_color|header_link_hover_color|primary_color',
            'value' => '#ef5619'
        ),

        array(
            'key' => 'la_custom_css',
            'value' => '
                .site-header .header_component.header-custom-btn{
                    margin-right: -20px
                }
                .site-header .header_component.header-custom-btn > .component-target {
                    background-color: #ef5619;
                    color: #fff;
                }
                .site-header .header_component.header-custom-btn:hover > .component-target{
                    background-color: #343538;
                    color: #fff;
                }
                @media (min-width: 1024px) {
                    .footer-column.footer-column-2 {
                        width: 16.66667%;
                    }
                    .footer-column.footer-column-4 {
                        width: 33.33333%;
                    }
                }
            '
        )
    );
}