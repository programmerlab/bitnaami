<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_pisces_preset_blog_left_sidebar(){
    return array(

        /**
         * Settings
         */


        array(
            'key' => 'layout_blog',
            'value' => 'col-2cl'
        ),
        array(
            'key' => 'blog_design',
            'value' => 'grid_4'
        ),
        array(
            'key' => 'header_transparency',
            'value' => 'yes'
        ),
        array(
            'key' => 'blog_post_column',
            'value' => array(
                'xlg'   => '1',
                'lg'    => '1',
                'md'    => '1',
                'sm'    => '1',
                'xs'    => '1',
                'mb'    => '1'
            )
        ),

        array(
            'key' => 'enable_page_title_subtext',
            'value' => 'yes'
        ),

        array(
            'key' => 'page_title_custom_subtext',
            'value' => 'Nullam varius porttitor augue id rutrum. Duis vehicula m'
        ),

        array(
            'key' => 'blog_thumbnail_size',
            'value' => '870x370'
        ),


        /**
         * Filters
         */

        array(
            'filter_name' => 'pisces/filter/page_title',
            'value' => '<header><h3 class="page-title">Blog Left Sidebar</h3></header>'
        ),

        /**
         * Colors
         */

        array(
            'key' => 'la_custom_css',
            'value' => '
                @media (min-width: 1199px) {
                    .site-main {
                        padding-top: 100px;
                        padding-bottom: 70px;
                    }
                }
            '
        )

    );
}