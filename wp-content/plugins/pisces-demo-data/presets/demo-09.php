<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_pisces_preset_demo_09(){
    return array(

        /**
         * Settings
         */

        array(
            'key' => 'header_layout',
            'value' => '2'
        ),
        array(
            'key' => 'header_full_width',
            'value' => 'yes'
        ),
        array(
            'key' => 'header_transparency',
            'value' => 'no'
        ),

        array(
            'key' => 'logo',
            'value' => '1573'
        ),
        array(
            'key' => 'logo_2x',
            'value' => '1574'
        ),
        array(
            'key' => 'logo_transparency',
            'value' => '1573'
        ),
        array(
            'key' => 'logo_transparency_2x',
            'value' => '1574'
        ),
        array(
            'key' => 'logo_mobile',
            'value' => '1573'
        ),
        array(
            'key' => 'logo_mobile_2x',
            'value' => '1574'
        ),

        array(
            'key' => 'header_access_icon',
            'value' => array(
                array(
                    'type' => 'search_1',
                    'el_class' => ''
                ),
                array(
                    'type' => 'link_text',
                    'icon' => '',
                    'text' => 'GET A QUOTE',
                    'link' => '#',
                    'el_class' => 'header-custom-btn'
                )
            )
        ),

        array(
            'key' => 'enable_header_top',
            'value' => 'custom'
        ),

        array(
            'key' => 'use_custom_header_top',
            'value' => '
<div class="header-top-elements clearfix">
    <div class="header-top-left">
        <div class="header_component header_component--text la_compt_iem la_com_action--text margin-right-20">
            <span class="component-target">
                <span class="component-target-text">Welcome to Pisces</span>
            </span>
        </div>
        <div class="header_component header_component--social la_compt_iem la_com_action--social hidden-xs">
            [la_social_link]
        </div>
    </div>
    <div class="header-top-right">
        <div class="header_component header_component--linktext la_compt_iem la_com_action--linktext hidden-xs">
            <a class="component-target" href="http://pisces.la-studioweb.com/contact-us-02/">
                <i class="fa fa-map-marker"></i>
                <span class="component-target-text">Get Direction</span>
            </a>
        </div>
        <div class="header_component header_component--linktext la_compt_iem la_com_action--linktext hidden-xs">
            <a class="component-target" href="tel:+586 536 325">
                <i class="fa fa-phone"></i>
                <span class="component-target-text">+586 536 325</span>
            </a>
        </div>
        <div class="header_component header_component--linktext la_compt_iem la_com_action--linktext hidden-xs">
            <a class="component-target" href="maito:info@domain.com">
                <i class="fa fa-envelope-o"></i>
                <span class="component-target-text">info@domain.com</span>
            </a>
        </div>
        <div class="header_component header_component--dropdown-menu la_compt_iem la_com_action--dropdownmenu la_com_action--dropdownmenu-text">
            <a class="component-target" href="javascript:;"><span class="component-target-text">LANGUAGE</span></a>
            [wp_nav_menu menu_id="97"]
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="header-top-slider clearfix">
[rev_slider alias="demo-09"]
</div>
            '
        ),

        array(
            'key' => 'header_height',
            'value' => '110px'
        ),
        array(
            'key' => 'header_sticky_height',
            'value' => '110px'
        ),

        /**
         * Filters
         */

        array(
            'filter_name' => 'pisces/filter/footer_column_4',
            'value' => 'demo-08-footer-column-4'
        ),
        /**
         * Colors
         */

        array(
            'key' => 'transparency_header_link_hover_color|mm_lv_1_hover_color|header_link_hover_color|primary_color',
            'value' => '#17d0ec'
        ),

        array(
            'key' => 'la_custom_css',
            'value' => '
                @media(min-width: 1200px){
                    .site-header .site-header-top .header-top-elements {
                        padding-top: 18px;
                        padding-bottom: 18px;
                    }
                }
                .site-header .header_component.header-custom-btn > .component-target {
                    background-color: #17d0ec;
                    color: #fff;
                }
                .site-header .header_component.header-custom-btn:hover > .component-target{
                    background-color: #343538;
                    color: #fff;
                }
                .header-top-elements .la_com_action--linktext i {
                    color: #17d0ec;
                }
                @media (min-width: 1024px) {
                    .footer-column.footer-column-2 {
                        width: 16.66667%;
                    }
                    .footer-column.footer-column-4 {
                        width: 33.33333%;
                    }
                }
            '
        )
    );
}