<?php

function la_pisces_get_demo_array($dir_url, $dir_path){

    $demo = array(
        'demo-01' => array(
            'title' 		=> 'Demo 01',
            'category'      => 'demo',
            'slider' 		=> $dir_path . 'Slider/demo-01.zip',
            'content' 		=> $dir_path . 'Content/data-sample.xml',
            'widget' 		=> $dir_path . 'Widget/widget-01.json',
            'option' 		=> $dir_path . 'Setting/demo-01.json',
            'preview'		=> $dir_url  . 'demo-01.jpg',
            'menu-locations'=> array(
                'main-nav'      => 'Primary Navigation',
                'mobile-nav'    => 'Aside Primary Navigation',
                'aside-nav'     => 'Aside Primary Navigation'
            ),
            'pages'			=> array(
                'page_on_front' 	            => 'Demo 01',
                'page_for_posts' 	            => 'Blog',
                'woocommerce_shop_page_id'      => 'Shop',
                'woocommerce_cart_page_id'      => 'Cart',
                'woocommerce_checkout_page_id'  => 'Checkout',
                'woocommerce_myaccount_page_id' => 'My Account',
                'yith_wcwl_wishlist_page_id'    => 'Wishlist'
            ),
            'other_setting' => array(
                'shop_catalog_image_size' => array(
                    'width' => 370,
                    'height' => 490,
                    'crop' => true
                ),
                'shop_single_image_size' => array(
                    'width' => 670,
                    'height' => 740,
                    'crop' => true
                ),
                'shop_thumbnail_image_size' => array(
                    'width' => 90,
                    'height' => 115,
                    'crop' => true
                ),
                'thumbnail_size_w' => 0,
                'thumbnail_size_h' => 0,
                'medium_size_w' => 0,
                'medium_size_h' => 0,
                'large_size_w' => 0,
                'large_size_h' => 0
            ),
            'demo_preset'   => 'demo-01',
            'demo_url'      => 'http://pisces.la-studioweb.com/demo-01/'
        ),
        'demo-02' => array(
            'title' 		=> 'Demo 02',
            'category'      => 'demo',
            'slider' 		=> $dir_path . 'Slider/demo-02.zip',
            'content' 		=> $dir_path . 'Content/data-sample.xml',
            'widget' 		=> $dir_path . 'Widget/widget-02.json',
            'option' 		=> $dir_path . 'Setting/demo-02.json',
            'preview'		=> $dir_url  . 'demo-02.jpg',
            'menu-locations'=> array(
                'main-nav'      => 'Primary Navigation',
                'mobile-nav'    => 'Aside Primary Navigation',
                'aside-nav'     => 'Aside Primary Navigation'
            ),
            'pages'			=> array(
                'page_on_front' 	            => 'Demo 02',
                'page_for_posts' 	            => 'Blog',
                'woocommerce_shop_page_id'      => 'Shop',
                'woocommerce_cart_page_id'      => 'Cart',
                'woocommerce_checkout_page_id'  => 'Checkout',
                'woocommerce_myaccount_page_id' => 'My Account',
                'yith_wcwl_wishlist_page_id'    => 'Wishlist'
            ),
            'other_setting' => array(
                'shop_catalog_image_size' => array(
                    'width' => 370,
                    'height' => 490,
                    'crop' => true
                ),
                'shop_single_image_size' => array(
                    'width' => 670,
                    'height' => 740,
                    'crop' => true
                ),
                'shop_thumbnail_image_size' => array(
                    'width' => 90,
                    'height' => 115,
                    'crop' => true
                ),
                'thumbnail_size_w' => 0,
                'thumbnail_size_h' => 0,
                'medium_size_w' => 0,
                'medium_size_h' => 0,
                'large_size_w' => 0,
                'large_size_h' => 0
            ),
            'demo_preset'   => 'demo-02',
            'demo_url'      => 'http://pisces.la-studioweb.com/demo-02/'
        ),
        'demo-03' => array(
            'title' 		=> 'Demo 03',
            'category'      => 'demo',
            'slider' 		=> $dir_path . 'Slider/demo-03.zip',
            'content' 		=> $dir_path . 'Content/data-sample.xml',
            'widget' 		=> $dir_path . 'Widget/widget-03.json',
            'option' 		=> $dir_path . 'Setting/demo-03.json',
            'preview'		=> $dir_url  . 'demo-03.jpg',
            'menu-locations'=> array(
                'main-nav'      => 'Primary Navigation',
                'mobile-nav'    => 'Aside Primary Navigation',
                'aside-nav'     => 'Aside Primary Navigation'
            ),
            'pages'			=> array(
                'page_on_front' 	            => 'Demo 03',
                'page_for_posts' 	            => 'Blog',
                'woocommerce_shop_page_id'      => 'Shop',
                'woocommerce_cart_page_id'      => 'Cart',
                'woocommerce_checkout_page_id'  => 'Checkout',
                'woocommerce_myaccount_page_id' => 'My Account',
                'yith_wcwl_wishlist_page_id'    => 'Wishlist'
            ),
            'other_setting' => array(
                'shop_catalog_image_size' => array(
                    'width' => 370,
                    'height' => 490,
                    'crop' => true
                ),
                'shop_single_image_size' => array(
                    'width' => 670,
                    'height' => 740,
                    'crop' => true
                ),
                'shop_thumbnail_image_size' => array(
                    'width' => 90,
                    'height' => 115,
                    'crop' => true
                ),
                'thumbnail_size_w' => 0,
                'thumbnail_size_h' => 0,
                'medium_size_w' => 0,
                'medium_size_h' => 0,
                'large_size_w' => 0,
                'large_size_h' => 0
            ),
            'demo_preset'   => 'demo-03',
            'demo_url'      => 'http://pisces.la-studioweb.com/demo-03/'
        ),
        'demo-04' => array(
            'title' 		=> 'Demo 04',
            'category'      => 'demo',
            'content' 		=> $dir_path . 'Content/data-sample.xml',
            'widget' 		=> $dir_path . 'Widget/widget-04.json',
            'option' 		=> $dir_path . 'Setting/demo-04.json',
            'preview'		=> $dir_url  . 'demo-04.jpg',
            'menu-locations'=> array(
                'main-nav'      => 'Primary Navigation',
                'mobile-nav'    => 'Aside Primary Navigation',
                'aside-nav'     => 'Aside Primary Navigation'
            ),
            'pages'			=> array(
                'page_on_front' 	            => 'Demo 04',
                'page_for_posts' 	            => 'Blog',
                'woocommerce_shop_page_id'      => 'Shop',
                'woocommerce_cart_page_id'      => 'Cart',
                'woocommerce_checkout_page_id'  => 'Checkout',
                'woocommerce_myaccount_page_id' => 'My Account',
                'yith_wcwl_wishlist_page_id'    => 'Wishlist'
            ),
            'other_setting' => array(
                'shop_catalog_image_size' => array(
                    'width' => 370,
                    'height' => 490,
                    'crop' => true
                ),
                'shop_single_image_size' => array(
                    'width' => 670,
                    'height' => 740,
                    'crop' => true
                ),
                'shop_thumbnail_image_size' => array(
                    'width' => 90,
                    'height' => 115,
                    'crop' => true
                ),
                'thumbnail_size_w' => 0,
                'thumbnail_size_h' => 0,
                'medium_size_w' => 0,
                'medium_size_h' => 0,
                'large_size_w' => 0,
                'large_size_h' => 0
            ),
            'demo_preset'   => 'demo-04',
            'demo_url'      => 'http://pisces.la-studioweb.com/demo-04/'
        ),
        'demo-05' => array(
            'title' 		=> 'Demo 05',
            'category'      => 'demo',
            'content' 		=> $dir_path . 'Content/data-sample.xml',
            'option' 		=> $dir_path . 'Setting/demo-05.json',
            'preview'		=> $dir_url  . 'demo-05.jpg',
            'menu-locations'=> array(
                'main-nav'      => 'Primary Navigation',
                'mobile-nav'    => 'Aside Primary Navigation',
                'aside-nav'     => 'Aside Primary Navigation'
            ),
            'pages'			=> array(
                'page_on_front' 	            => 'Demo 05',
                'page_for_posts' 	            => 'Blog',
                'woocommerce_shop_page_id'      => 'Shop',
                'woocommerce_cart_page_id'      => 'Cart',
                'woocommerce_checkout_page_id'  => 'Checkout',
                'woocommerce_myaccount_page_id' => 'My Account',
                'yith_wcwl_wishlist_page_id'    => 'Wishlist'
            ),
            'other_setting' => array(
                'shop_catalog_image_size' => array(
                    'width' => 370,
                    'height' => 490,
                    'crop' => true
                ),
                'shop_single_image_size' => array(
                    'width' => 670,
                    'height' => 740,
                    'crop' => true
                ),
                'shop_thumbnail_image_size' => array(
                    'width' => 90,
                    'height' => 115,
                    'crop' => true
                ),
                'thumbnail_size_w' => 0,
                'thumbnail_size_h' => 0,
                'medium_size_w' => 0,
                'medium_size_h' => 0,
                'large_size_w' => 0,
                'large_size_h' => 0
            ),
            'demo_preset'   => 'demo-05',
            'demo_url'      => 'http://pisces.la-studioweb.com/demo-05/'
        ),
        'demo-06' => array(
            'title' 		=> 'Demo 06',
            'category'      => 'demo',
            'slider' 		=> $dir_path . 'Slider/demo-06.zip',
            'content' 		=> $dir_path . 'Content/data-sample.xml',
            'widget' 		=> $dir_path . 'Widget/widget-06.json',
            'option' 		=> $dir_path . 'Setting/demo-06.json',
            'preview'		=> $dir_url  . 'demo-06.jpg',
            'menu-locations'=> array(
                'main-nav'      => 'Primary Navigation',
                'mobile-nav'    => 'Aside Primary Navigation',
                'aside-nav'     => 'Aside Primary Navigation'
            ),
            'pages'			=> array(
                'page_on_front' 	            => 'Demo 06',
                'page_for_posts' 	            => 'Blog',
                'woocommerce_shop_page_id'      => 'Shop',
                'woocommerce_cart_page_id'      => 'Cart',
                'woocommerce_checkout_page_id'  => 'Checkout',
                'woocommerce_myaccount_page_id' => 'My Account',
                'yith_wcwl_wishlist_page_id'    => 'Wishlist'
            ),
            'other_setting' => array(
                'shop_catalog_image_size' => array(
                    'width' => 370,
                    'height' => 490,
                    'crop' => true
                ),
                'shop_single_image_size' => array(
                    'width' => 670,
                    'height' => 740,
                    'crop' => true
                ),
                'shop_thumbnail_image_size' => array(
                    'width' => 90,
                    'height' => 115,
                    'crop' => true
                ),
                'thumbnail_size_w' => 0,
                'thumbnail_size_h' => 0,
                'medium_size_w' => 0,
                'medium_size_h' => 0,
                'large_size_w' => 0,
                'large_size_h' => 0
            ),
            'demo_preset'   => 'demo-06',
            'demo_url'      => 'http://pisces.la-studioweb.com/demo-06/'
        ),
        'demo-07' => array(
            'title' 		=> 'Demo 07',
            'category'      => 'demo',
            'content' 		=> $dir_path . 'Content/data-sample.xml',
            'widget' 		=> $dir_path . 'Widget/widget-07.json',
            'option' 		=> $dir_path . 'Setting/demo-07.json',
            'preview'		=> $dir_url  . 'demo-07.jpg',
            'menu-locations'=> array(
                'main-nav'      => 'Primary Navigation',
                'mobile-nav'    => 'Aside Primary Navigation',
                'aside-nav'     => 'Aside Primary Navigation'
            ),
            'pages'			=> array(
                'page_on_front' 	            => 'Demo 07',
                'page_for_posts' 	            => 'Blog',
                'woocommerce_shop_page_id'      => 'Shop',
                'woocommerce_cart_page_id'      => 'Cart',
                'woocommerce_checkout_page_id'  => 'Checkout',
                'woocommerce_myaccount_page_id' => 'My Account',
                'yith_wcwl_wishlist_page_id'    => 'Wishlist'
            ),
            'other_setting' => array(
                'shop_catalog_image_size' => array(
                    'width' => 370,
                    'height' => 490,
                    'crop' => true
                ),
                'shop_single_image_size' => array(
                    'width' => 670,
                    'height' => 740,
                    'crop' => true
                ),
                'shop_thumbnail_image_size' => array(
                    'width' => 90,
                    'height' => 115,
                    'crop' => true
                ),
                'thumbnail_size_w' => 0,
                'thumbnail_size_h' => 0,
                'medium_size_w' => 0,
                'medium_size_h' => 0,
                'large_size_w' => 0,
                'large_size_h' => 0
            ),
            'demo_preset'   => 'demo-07',
            'demo_url'      => 'http://pisces.la-studioweb.com/demo-07/'
        ),
        'demo-08' => array(
            'title' 		=> 'Demo 08',
            'category'      => 'demo',
            'slider' 		=> $dir_path . 'Slider/demo-08.zip',
            'content' 		=> $dir_path . 'Content/data-sample.xml',
            'widget' 		=> $dir_path . 'Widget/widget-08.json',
            'option' 		=> $dir_path . 'Setting/demo-08.json',
            'preview'		=> $dir_url  . 'demo-08.jpg',
            'menu-locations'=> array(
                'main-nav'      => 'Primary Navigation',
                'mobile-nav'    => 'Aside Primary Navigation',
                'aside-nav'     => 'Aside Primary Navigation'
            ),
            'pages'			=> array(
                'page_on_front' 	            => 'Demo 08',
                'page_for_posts' 	            => 'Blog',
                'woocommerce_shop_page_id'      => 'Shop',
                'woocommerce_cart_page_id'      => 'Cart',
                'woocommerce_checkout_page_id'  => 'Checkout',
                'woocommerce_myaccount_page_id' => 'My Account',
                'yith_wcwl_wishlist_page_id'    => 'Wishlist'
            ),
            'other_setting' => array(
                'shop_catalog_image_size' => array(
                    'width' => 370,
                    'height' => 490,
                    'crop' => true
                ),
                'shop_single_image_size' => array(
                    'width' => 670,
                    'height' => 740,
                    'crop' => true
                ),
                'shop_thumbnail_image_size' => array(
                    'width' => 90,
                    'height' => 115,
                    'crop' => true
                ),
                'thumbnail_size_w' => 0,
                'thumbnail_size_h' => 0,
                'medium_size_w' => 0,
                'medium_size_h' => 0,
                'large_size_w' => 0,
                'large_size_h' => 0
            ),
            'demo_preset'   => 'demo-08',
            'demo_url'      => 'http://pisces.la-studioweb.com/demo-08/'
        ),
        'demo-09' => array(
            'title' 		=> 'Demo 09',
            'category'      => 'demo',
            'slider' 		=> $dir_path . 'Slider/demo-09.zip',
            'content' 		=> $dir_path . 'Content/data-sample.xml',
            'widget' 		=> $dir_path . 'Widget/widget-01.json',
            'option' 		=> $dir_path . 'Setting/demo-09.json',
            'preview'		=> $dir_url  . 'demo-09.jpg',
            'menu-locations'=> array(
                'main-nav'      => 'Primary Navigation',
                'mobile-nav'    => 'Aside Primary Navigation',
                'aside-nav'     => 'Aside Primary Navigation'
            ),
            'pages'			=> array(
                'page_on_front' 	            => 'Demo 09',
                'page_for_posts' 	            => 'Blog',
                'woocommerce_shop_page_id'      => 'Shop',
                'woocommerce_cart_page_id'      => 'Cart',
                'woocommerce_checkout_page_id'  => 'Checkout',
                'woocommerce_myaccount_page_id' => 'My Account',
                'yith_wcwl_wishlist_page_id'    => 'Wishlist'
            ),
            'other_setting' => array(
                'shop_catalog_image_size' => array(
                    'width' => 370,
                    'height' => 490,
                    'crop' => true
                ),
                'shop_single_image_size' => array(
                    'width' => 670,
                    'height' => 740,
                    'crop' => true
                ),
                'shop_thumbnail_image_size' => array(
                    'width' => 90,
                    'height' => 115,
                    'crop' => true
                ),
                'thumbnail_size_w' => 0,
                'thumbnail_size_h' => 0,
                'medium_size_w' => 0,
                'medium_size_h' => 0,
                'large_size_w' => 0,
                'large_size_h' => 0
            ),
            'demo_preset'   => 'demo-09',
            'demo_url'      => 'http://pisces.la-studioweb.com/demo-09/'
        ),
        'demo-10' => array(
            'title' 		=> 'Demo 10',
            'category'      => 'demo',
            'slider' 		=> $dir_path . 'Slider/demo-10.zip',
            'content' 		=> $dir_path . 'Content/data-sample.xml',
            'widget' 		=> $dir_path . 'Widget/widget-10.json',
            'option' 		=> $dir_path . 'Setting/demo-10.json',
            'preview'		=> $dir_url  . 'demo-10.jpg',
            'menu-locations'=> array(
                'main-nav'      => 'Primary Navigation',
                'mobile-nav'    => 'Aside Primary Navigation',
                'aside-nav'     => 'Aside Primary Navigation'
            ),
            'pages'			=> array(
                'page_on_front' 	            => 'Demo 10',
                'page_for_posts' 	            => 'Blog',
                'woocommerce_shop_page_id'      => 'Shop',
                'woocommerce_cart_page_id'      => 'Cart',
                'woocommerce_checkout_page_id'  => 'Checkout',
                'woocommerce_myaccount_page_id' => 'My Account',
                'yith_wcwl_wishlist_page_id'    => 'Wishlist'
            ),
            'other_setting' => array(
                'shop_catalog_image_size' => array(
                    'width' => 370,
                    'height' => 490,
                    'crop' => true
                ),
                'shop_single_image_size' => array(
                    'width' => 670,
                    'height' => 740,
                    'crop' => true
                ),
                'shop_thumbnail_image_size' => array(
                    'width' => 90,
                    'height' => 115,
                    'crop' => true
                ),
                'thumbnail_size_w' => 0,
                'thumbnail_size_h' => 0,
                'medium_size_w' => 0,
                'medium_size_h' => 0,
                'large_size_w' => 0,
                'large_size_h' => 0
            ),
            'demo_preset'   => 'demo-10',
            'demo_url'      => 'http://pisces.la-studioweb.com/demo-10/'
        ),
        'demo-11' => array(
            'title' 		=> 'Demo 11',
            'category'      => 'demo',
            'content' 		=> $dir_path . 'Content/data-sample.xml',
            'widget' 		=> $dir_path . 'Widget/widget-11.json',
            'option' 		=> $dir_path . 'Setting/demo-11.json',
            'preview'		=> $dir_url  . 'demo-11.jpg',
            'menu-locations'=> array(
                'main-nav'      => 'Primary Navigation',
                'mobile-nav'    => 'Aside Primary Navigation',
                'aside-nav'     => 'Aside Primary Navigation'
            ),
            'pages'			=> array(
                'page_on_front' 	            => 'Demo 11',
                'page_for_posts' 	            => 'Blog',
                'woocommerce_shop_page_id'      => 'Shop',
                'woocommerce_cart_page_id'      => 'Cart',
                'woocommerce_checkout_page_id'  => 'Checkout',
                'woocommerce_myaccount_page_id' => 'My Account',
                'yith_wcwl_wishlist_page_id'    => 'Wishlist'
            ),
            'other_setting' => array(
                'shop_catalog_image_size' => array(
                    'width' => 370,
                    'height' => 490,
                    'crop' => true
                ),
                'shop_single_image_size' => array(
                    'width' => 670,
                    'height' => 740,
                    'crop' => true
                ),
                'shop_thumbnail_image_size' => array(
                    'width' => 90,
                    'height' => 115,
                    'crop' => true
                ),
                'thumbnail_size_w' => 0,
                'thumbnail_size_h' => 0,
                'medium_size_w' => 0,
                'medium_size_h' => 0,
                'large_size_w' => 0,
                'large_size_h' => 0
            ),
            'demo_preset'   => 'demo-11',
            'demo_url'      => 'http://pisces.la-studioweb.com/demo-11/'
        ),
        'demo-12' => array(
            'title' 		=> 'Demo 12',
            'category'      => 'demo',
            'content' 		=> $dir_path . 'Content/data-sample.xml',
            'widget' 		=> $dir_path . 'Widget/widget-12.json',
            'option' 		=> $dir_path . 'Setting/demo-12.json',
            'preview'		=> $dir_url  . 'demo-12.jpg',
            'menu-locations'=> array(
                'main-nav'      => 'Primary Navigation',
                'mobile-nav'    => 'Aside Primary Navigation',
                'aside-nav'     => 'Aside Primary Navigation'
            ),
            'pages'			=> array(
                'page_on_front' 	            => 'Demo 12',
                'page_for_posts' 	            => 'Blog',
                'woocommerce_shop_page_id'      => 'Shop',
                'woocommerce_cart_page_id'      => 'Cart',
                'woocommerce_checkout_page_id'  => 'Checkout',
                'woocommerce_myaccount_page_id' => 'My Account',
                'yith_wcwl_wishlist_page_id'    => 'Wishlist'
            ),
            'other_setting' => array(
                'shop_catalog_image_size' => array(
                    'width' => 370,
                    'height' => 490,
                    'crop' => true
                ),
                'shop_single_image_size' => array(
                    'width' => 670,
                    'height' => 740,
                    'crop' => true
                ),
                'shop_thumbnail_image_size' => array(
                    'width' => 90,
                    'height' => 115,
                    'crop' => true
                ),
                'thumbnail_size_w' => 0,
                'thumbnail_size_h' => 0,
                'medium_size_w' => 0,
                'medium_size_h' => 0,
                'large_size_w' => 0,
                'large_size_h' => 0
            ),
            'demo_preset'   => 'demo-12',
            'demo_url'      => 'http://pisces.la-studioweb.com/demo-12/'
        ),
        'demo-13' => array(
            'title' 		=> 'Demo 13',
            'category'      => 'demo',
            'content' 		=> $dir_path . 'Content/data-sample.xml',
            'widget' 		=> $dir_path . 'Widget/widget-13.json',
            'option' 		=> $dir_path . 'Setting/demo-13.json',
            'preview'		=> $dir_url  . 'demo-13.jpg',
            'menu-locations'=> array(
                'main-nav'      => 'Primary Navigation',
                'mobile-nav'    => 'Aside Primary Navigation',
                'aside-nav'     => 'Aside Primary Navigation'
            ),
            'pages'			=> array(
                'page_on_front' 	            => 'Demo 13',
                'page_for_posts' 	            => 'Blog',
                'woocommerce_shop_page_id'      => 'Shop',
                'woocommerce_cart_page_id'      => 'Cart',
                'woocommerce_checkout_page_id'  => 'Checkout',
                'woocommerce_myaccount_page_id' => 'My Account',
                'yith_wcwl_wishlist_page_id'    => 'Wishlist'
            ),
            'other_setting' => array(
                'shop_catalog_image_size' => array(
                    'width' => 370,
                    'height' => 490,
                    'crop' => true
                ),
                'shop_single_image_size' => array(
                    'width' => 670,
                    'height' => 740,
                    'crop' => true
                ),
                'shop_thumbnail_image_size' => array(
                    'width' => 90,
                    'height' => 115,
                    'crop' => true
                ),
                'thumbnail_size_w' => 0,
                'thumbnail_size_h' => 0,
                'medium_size_w' => 0,
                'medium_size_h' => 0,
                'large_size_w' => 0,
                'large_size_h' => 0
            ),
            'demo_preset'   => 'demo-13',
            'demo_url'      => 'http://pisces.la-studioweb.com/demo-13/'
        ),
        'demo-14' => array(
            'title' 		=> 'Demo 14',
            'category'      => 'demo',
            'content' 		=> $dir_path . 'Content/data-sample.xml',
            'widget' 		=> $dir_path . 'Widget/widget-14.json',
            'option' 		=> $dir_path . 'Setting/demo-14.json',
            'preview'		=> $dir_url  . 'demo-14.jpg',
            'menu-locations'=> array(
                'main-nav'      => 'Primary Navigation',
                'mobile-nav'    => 'Aside Primary Navigation',
                'aside-nav'     => 'Aside Primary Navigation'
            ),
            'pages'			=> array(
                'page_on_front' 	            => 'Demo 14',
                'page_for_posts' 	            => 'Blog',
                'woocommerce_shop_page_id'      => 'Shop',
                'woocommerce_cart_page_id'      => 'Cart',
                'woocommerce_checkout_page_id'  => 'Checkout',
                'woocommerce_myaccount_page_id' => 'My Account',
                'yith_wcwl_wishlist_page_id'    => 'Wishlist'
            ),
            'other_setting' => array(
                'shop_catalog_image_size' => array(
                    'width' => 370,
                    'height' => 490,
                    'crop' => true
                ),
                'shop_single_image_size' => array(
                    'width' => 670,
                    'height' => 740,
                    'crop' => true
                ),
                'shop_thumbnail_image_size' => array(
                    'width' => 90,
                    'height' => 115,
                    'crop' => true
                ),
                'thumbnail_size_w' => 0,
                'thumbnail_size_h' => 0,
                'medium_size_w' => 0,
                'medium_size_h' => 0,
                'large_size_w' => 0,
                'large_size_h' => 0
            ),
            'demo_preset'   => 'demo-14',
            'demo_url'      => 'http://pisces.la-studioweb.com/demo-14/'
        ),
        'demo-15' => array(
            'title' 		=> 'Demo 15',
            'category'      => 'demo',
            'content' 		=> $dir_path . 'Content/data-sample.xml',
            'widget' 		=> $dir_path . 'Widget/widget-15.json',
            'option' 		=> $dir_path . 'Setting/demo-15.json',
            'preview'		=> $dir_url  . 'demo-15.jpg',
            'menu-locations'=> array(
                'main-nav'      => 'Primary Navigation',
                'mobile-nav'    => 'Aside Primary Navigation',
                'aside-nav'     => 'Aside Primary Navigation'
            ),
            'pages'			=> array(
                'page_on_front' 	            => 'Demo 15',
                'page_for_posts' 	            => 'Blog',
                'woocommerce_shop_page_id'      => 'Shop',
                'woocommerce_cart_page_id'      => 'Cart',
                'woocommerce_checkout_page_id'  => 'Checkout',
                'woocommerce_myaccount_page_id' => 'My Account',
                'yith_wcwl_wishlist_page_id'    => 'Wishlist'
            ),
            'other_setting' => array(
                'shop_catalog_image_size' => array(
                    'width' => 370,
                    'height' => 490,
                    'crop' => true
                ),
                'shop_single_image_size' => array(
                    'width' => 670,
                    'height' => 740,
                    'crop' => true
                ),
                'shop_thumbnail_image_size' => array(
                    'width' => 90,
                    'height' => 115,
                    'crop' => true
                ),
                'thumbnail_size_w' => 0,
                'thumbnail_size_h' => 0,
                'medium_size_w' => 0,
                'medium_size_h' => 0,
                'large_size_w' => 0,
                'large_size_h' => 0
            ),
            'demo_preset'   => 'demo-15',
            'demo_url'      => 'http://pisces.la-studioweb.com/demo-15/'
        ),
        'demo-16' => array(
            'title' 		=> 'Demo 16',
            'category'      => 'demo',
            'content' 		=> $dir_path . 'Content/data-sample.xml',
            'widget' 		=> $dir_path . 'Widget/widget-16.json',
            'option' 		=> $dir_path . 'Setting/demo-16.json',
            'preview'		=> $dir_url  . 'demo-16.jpg',
            'menu-locations'=> array(
                'main-nav'      => 'Primary Navigation',
                'mobile-nav'    => 'Aside Primary Navigation',
                'aside-nav'     => 'Aside Primary Navigation'
            ),
            'pages'			=> array(
                'page_on_front' 	            => 'Demo 16',
                'page_for_posts' 	            => 'Blog',
                'woocommerce_shop_page_id'      => 'Shop',
                'woocommerce_cart_page_id'      => 'Cart',
                'woocommerce_checkout_page_id'  => 'Checkout',
                'woocommerce_myaccount_page_id' => 'My Account',
                'yith_wcwl_wishlist_page_id'    => 'Wishlist'
            ),
            'other_setting' => array(
                'shop_catalog_image_size' => array(
                    'width' => 370,
                    'height' => 490,
                    'crop' => true
                ),
                'shop_single_image_size' => array(
                    'width' => 670,
                    'height' => 740,
                    'crop' => true
                ),
                'shop_thumbnail_image_size' => array(
                    'width' => 90,
                    'height' => 115,
                    'crop' => true
                ),
                'thumbnail_size_w' => 0,
                'thumbnail_size_h' => 0,
                'medium_size_w' => 0,
                'medium_size_h' => 0,
                'large_size_w' => 0,
                'large_size_h' => 0
            ),
            'demo_preset'   => 'demo-16',
            'demo_url'      => 'http://pisces.la-studioweb.com/demo-16/'
        ),
        'demo-17' => array(
            'title' 		=> 'Demo 17',
            'category'      => 'demo',
            'slider' 		=> $dir_path . 'Slider/demo-17.zip',
            'content' 		=> $dir_path . 'Content/data-sample.xml',
            'widget' 		=> $dir_path . 'Widget/widget-17.json',
            'option' 		=> $dir_path . 'Setting/demo-17.json',
            'preview'		=> $dir_url  . 'demo-17.jpg',
            'menu-locations'=> array(
                'main-nav'      => 'Primary Navigation',
                'mobile-nav'    => 'Aside Primary Navigation',
                'aside-nav'     => 'Aside Primary Navigation'
            ),
            'pages'			=> array(
                'page_on_front' 	            => 'Demo 17',
                'page_for_posts' 	            => 'Blog',
                'woocommerce_shop_page_id'      => 'Shop',
                'woocommerce_cart_page_id'      => 'Cart',
                'woocommerce_checkout_page_id'  => 'Checkout',
                'woocommerce_myaccount_page_id' => 'My Account',
                'yith_wcwl_wishlist_page_id'    => 'Wishlist'
            ),
            'other_setting' => array(
                'shop_catalog_image_size' => array(
                    'width' => 370,
                    'height' => 490,
                    'crop' => true
                ),
                'shop_single_image_size' => array(
                    'width' => 670,
                    'height' => 740,
                    'crop' => true
                ),
                'shop_thumbnail_image_size' => array(
                    'width' => 90,
                    'height' => 115,
                    'crop' => true
                ),
                'thumbnail_size_w' => 0,
                'thumbnail_size_h' => 0,
                'medium_size_w' => 0,
                'medium_size_h' => 0,
                'large_size_w' => 0,
                'large_size_h' => 0
            ),
            'demo_preset'   => 'demo-17',
            'demo_url'      => 'http://pisces.la-studioweb.com/demo-17/'
        ),
        'demo-18' => array(
            'title' 		=> 'Demo 18',
            'category'      => 'demo',
            'slider' 		=> $dir_path . 'Slider/demo-18.zip',
            'content' 		=> $dir_path . 'Content/data-sample.xml',
            'widget' 		=> $dir_path . 'Widget/widget-18.json',
            'option' 		=> $dir_path . 'Setting/demo-18.json',
            'preview'		=> $dir_url  . 'demo-18.jpg',
            'menu-locations'=> array(
                'main-nav'      => 'Primary Navigation',
                'mobile-nav'    => 'Aside Primary Navigation',
                'aside-nav'     => 'Aside Primary Navigation'
            ),
            'pages'			=> array(
                'page_on_front' 	            => 'Demo 18',
                'page_for_posts' 	            => 'Blog',
                'woocommerce_shop_page_id'      => 'Shop',
                'woocommerce_cart_page_id'      => 'Cart',
                'woocommerce_checkout_page_id'  => 'Checkout',
                'woocommerce_myaccount_page_id' => 'My Account',
                'yith_wcwl_wishlist_page_id'    => 'Wishlist'
            ),
            'other_setting' => array(
                'shop_catalog_image_size' => array(
                    'width' => 370,
                    'height' => 490,
                    'crop' => true
                ),
                'shop_single_image_size' => array(
                    'width' => 670,
                    'height' => 740,
                    'crop' => true
                ),
                'shop_thumbnail_image_size' => array(
                    'width' => 90,
                    'height' => 115,
                    'crop' => true
                ),
                'thumbnail_size_w' => 0,
                'thumbnail_size_h' => 0,
                'medium_size_w' => 0,
                'medium_size_h' => 0,
                'large_size_w' => 0,
                'large_size_h' => 0
            ),
            'demo_preset'   => 'demo-18',
            'demo_url'      => 'http://pisces.la-studioweb.com/demo-18/'
        ),
        'demo-19' => array(
            'title' 		=> 'Demo 19',
            'category'      => 'demo',
            'slider' 		=> $dir_path . 'Slider/demo-19.zip',
            'content' 		=> $dir_path . 'Content/data-sample.xml',
            'widget' 		=> $dir_path . 'Widget/widget-19.json',
            'option' 		=> $dir_path . 'Setting/demo-19.json',
            'preview'		=> $dir_url  . 'demo-19.jpg',
            'menu-locations'=> array(
                'main-nav'      => 'Primary Navigation',
                'mobile-nav'    => 'Aside Primary Navigation',
                'aside-nav'     => 'Aside Primary Navigation'
            ),
            'pages'			=> array(
                'page_on_front' 	            => 'Demo 19',
                'page_for_posts' 	            => 'Blog',
                'woocommerce_shop_page_id'      => 'Shop',
                'woocommerce_cart_page_id'      => 'Cart',
                'woocommerce_checkout_page_id'  => 'Checkout',
                'woocommerce_myaccount_page_id' => 'My Account',
                'yith_wcwl_wishlist_page_id'    => 'Wishlist'
            ),
            'other_setting' => array(
                'shop_catalog_image_size' => array(
                    'width' => 370,
                    'height' => 490,
                    'crop' => true
                ),
                'shop_single_image_size' => array(
                    'width' => 670,
                    'height' => 740,
                    'crop' => true
                ),
                'shop_thumbnail_image_size' => array(
                    'width' => 90,
                    'height' => 115,
                    'crop' => true
                ),
                'thumbnail_size_w' => 0,
                'thumbnail_size_h' => 0,
                'medium_size_w' => 0,
                'medium_size_h' => 0,
                'large_size_w' => 0,
                'large_size_h' => 0
            ),
            'demo_preset'   => 'demo-19',
            'demo_url'      => 'http://pisces.la-studioweb.com/demo-19/'
        ),
        'demo-20' => array(
            'title' 		=> 'Demo 20',
            'category'      => 'demo',
            'slider' 		=> $dir_path . 'Slider/demo-20.zip',
            'content' 		=> $dir_path . 'Content/data-sample.xml',
            'widget' 		=> $dir_path . 'Widget/widget-20.json',
            'option' 		=> $dir_path . 'Setting/demo-20.json',
            'preview'		=> $dir_url  . 'demo-20.jpg',
            'menu-locations'=> array(
                'main-nav'      => 'Primary Navigation',
                'mobile-nav'    => 'Aside Primary Navigation',
                'aside-nav'     => 'Aside Primary Navigation'
            ),
            'pages'			=> array(
                'page_on_front' 	            => 'Demo 20',
                'page_for_posts' 	            => 'Blog',
                'woocommerce_shop_page_id'      => 'Shop',
                'woocommerce_cart_page_id'      => 'Cart',
                'woocommerce_checkout_page_id'  => 'Checkout',
                'woocommerce_myaccount_page_id' => 'My Account',
                'yith_wcwl_wishlist_page_id'    => 'Wishlist'
            ),
            'other_setting' => array(
                'shop_catalog_image_size' => array(
                    'width' => 370,
                    'height' => 490,
                    'crop' => true
                ),
                'shop_single_image_size' => array(
                    'width' => 670,
                    'height' => 740,
                    'crop' => true
                ),
                'shop_thumbnail_image_size' => array(
                    'width' => 90,
                    'height' => 115,
                    'crop' => true
                ),
                'thumbnail_size_w' => 0,
                'thumbnail_size_h' => 0,
                'medium_size_w' => 0,
                'medium_size_h' => 0,
                'large_size_w' => 0,
                'large_size_h' => 0
            ),
            'demo_url'      => 'http://pisces.la-studioweb.com/demo-20/'
        ),
    );

    return $demo;
}