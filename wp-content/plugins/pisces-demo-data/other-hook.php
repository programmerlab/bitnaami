<?php

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}


add_filter('woocommerce_product_related_posts_relate_by_category', '__return_false');
add_filter('woocommerce_product_related_posts_relate_by_tag', '__return_false');
add_filter('woocommerce_product_related_posts_force_display', '__return_true');


add_filter('body_class', 'pisces_preset_add_body_classes');

function pisces_preset_add_body_classes( $class ){
    $class[] = 'isLaWebRoot';
    return $class;
}

add_filter( 'get_avatar_url', 'pisces_preset_new_gravatar', 10, 3);
function pisces_preset_new_gravatar ( $url, $id_or_email, $args ) {
    if ( is_object( $id_or_email ) && isset( $id_or_email->comment_author_email ) ) {
        if ( $id_or_email->comment_author_email == 'info1@localhost.com' ){
            return Pisces::$template_dir_url . '/assets/images/avatar-1.jpg';
        }
        if ( $id_or_email->comment_author_email == 'info2@localhost.com' ){
            return Pisces::$template_dir_url . '/assets/images/avatar-2.jpg';
        }
        if ( $id_or_email->comment_author_email == 'info3@localhost.com' ){
            return Pisces::$template_dir_url . '/assets/images/avatar-3.jpg';
        }
        if ( $id_or_email->comment_author_email == 'blog1@localhost.com' ){
            return Pisces::$template_dir_url . '/assets/images/blog-avatar-1.jpg';
        }
        if ( $id_or_email->comment_author_email == 'blog2@localhost.com' ){
            return Pisces::$template_dir_url . '/assets/images/blog-avatar-2.jpg';
        }
        if ( $id_or_email->comment_author_email == 'blog3@localhost.com' ){
            return Pisces::$template_dir_url . '/assets/images/blog-avatar-3.jpg';
        }
    }
    if ( $id_or_email == 'dpv.0990@gmail.com' || $id_or_email == 'info@la-studioweb.com' ){
        return Pisces::$template_dir_url . '/assets/images/blog-avatar-1.jpg';
    }
    return $url;
}

add_action( 'pre_get_posts', 'pisces_preset_change_blog_posts' );
function pisces_preset_change_blog_posts( $query ){

    if($query->is_home() && $query->is_main_query()){

        if(
            ( isset($_GET['la_preset']) && ($_GET['la_preset'] == 'blog-01-column' || $_GET['la_preset'] == 'blog-left-sidebar') ) ||
            !isset($_GET['la_preset'])
        ){
            $query->set( 'post__in', array( 1005,1001,998,995,992,502,1454,1457,1460,64,1368,1365,980,977,862,859,576,573,570,504,497,312,307,304,73,70,67 ) );
            $query->set( 'posts_per_page', 5 );
            $query->set( 'orderby', 'post__in' );
        }
        else{
            if(isset($_GET['la_preset'])){
                if($_GET['la_preset'] == 'blog-02-columns'){
                    $query->set( 'post__in', array( 502,1454,1457,1005,1460,998,64,1001,1368,1365,995,992,980,977,862,859,576,573,570,504,497,312,307,304,73,70,67 ) );
                    $query->set( 'posts_per_page', 8 );
                    $query->set( 'orderby', 'post__in' );
                }
                if($_GET['la_preset'] == 'blog-03-columns'){
                    $query->set( 'post__in', array( 502,1454,1457,1005,1460,998,64,1001,1368,1365,995,992,980,977,862,859,576,573,570,504,497,312,307,304,73,70,67 ) );
                    $query->set( 'posts_per_page', 9 );
                    $query->set( 'orderby', 'post__in' );
                }
                if($_GET['la_preset'] == 'blog-masonry'){
                    $query->set( 'posts_per_page', 14 );
                }
            }
        }
    }

}

add_filter('single_product_archive_thumbnail_size', 'pisces_preset_modify_thumbnail_for_shop_masonry', 99 );
function pisces_preset_modify_thumbnail_for_shop_masonry( $size ){
    global $pisces_loop, $woocommerce_loop;

    if(isset($_GET['la_preset']) && $_GET['la_preset'] == 'shop-masonry-01'){
        $idx = absint($woocommerce_loop['loop']) - 1;
        if($idx < 0){
            $idx = 0;
        }
        $ref = array(
            array(370,540),
            array(370,430),
            array(370,300),
            array(370,430),
            array(370,430),
            array(370,430),
            array(370,300),
            array(370,540),
            array(370,430),
            array(370,430),
            array(370,300),
            array(370,430)
        );
        if(isset($ref[$idx])){
            $pisces_loop['image_size'] = $ref[$idx];
            return $ref[$idx];
        }
    }
    if(isset($_GET['la_preset']) && $_GET['la_preset'] == 'shop-masonry-02'){
        $idx = absint($woocommerce_loop['loop']) - 1;
        if($idx < 0){
            $idx = 0;
        }
        $ref = array(
            array(425,450),
            array(425,330),
            array(425,400),
            array(425,380),
            array(425,400),
            array(425,380),
            array(425,650),
            array(425,450),
            array(425,330),
            array(425,400),
            array(425,380),
            array(425,400),
            array(425,380),
            array(425,650)
        );
        if(isset($ref[$idx])){
            $pisces_loop['image_size'] = $ref[$idx];
            return $ref[$idx];
        }
    }
    if(isset($_GET['la_preset']) && $_GET['la_preset'] == 'shop-02-columns'){
        $size = array(570,550);
        $pisces_loop['image_size'] = $size;
    }

    return $size;
}

add_action('woocommerce_before_shop_loop_item_title', 'pisces_preset_before_modify_thumbnail_for_shop_masonry', 4);
add_action('woocommerce_before_shop_loop_item_title', 'pisces_preset_after_modify_thumbnail_for_shop_masonry', 37);
function pisces_preset_before_modify_thumbnail_for_shop_masonry(){
    add_filter('pisces/filter/image_helper/crop', function($crop, $id, $size){
        $ignore = array(
            1388,
            1386,
            1384,
            1382,
            926,
            927,
            908,
            909,
            890,
            891,
        );
        if(!empty($ignore) && in_array($id, $ignore)){
            return $crop;
        }
        return array('center', 'top');
    }, 101, 3);
}
function pisces_preset_after_modify_thumbnail_for_shop_masonry(){
    remove_all_filters('pisces/filter/image_helper/crop', 101);
}

add_filter('is_active_sidebar', 'pisces_modify_single_custom_sidebar', 10, 2);
function pisces_modify_single_custom_sidebar( $is_active_sidebar, $index ){
    if(isset($_GET['la_preset']) && $_GET['la_preset'] == 'shop-detail-02' && $index == 'la-p-s-block-1'){
        return false;
    }
    return $is_active_sidebar;
}

add_filter('woocommerce_output_related_products_args', 'pisces_modify_woocommerce_output_related_products_args');
function pisces_modify_woocommerce_output_related_products_args( $args ){
    if(isset($_GET['la_preset']) && $_GET['la_preset'] == 'shop-detail-02'){
        return array(
            'posts_per_page' 	=> 3,
            'columns' 			=> 3,
            'orderby'           => 'rand'
        );
    }
    return $args;
}


add_filter('loop_shop_per_page', 'pisces_demo_change_per_page_default', 99 );
function pisces_demo_change_per_page_default($cols){
    if(isset($_GET['la_preset']) && $_GET['la_preset'] == 'shop-02-columns'){
        if(!empty($_GET['per_page']) && in_array($_GET['per_page'], array(6,12,18))){
            return $_GET['per_page'];
        }else{
            return 6;
        }
    }
    return $cols;
}

add_filter('nav_menu_css_class', 'pisces_demo_remove_force_100width_demo_19', 20, 4);
function pisces_demo_remove_force_100width_demo_19($classes, $item, $args, $depth){
    if(is_page('demo-19')){
        if( $depth == 0 ){
            if(($key = array_search('mm-popup-force-fullwidth', $classes)) !== false) {
                unset($classes[$key]);
            }
        }
    }
    return $classes;
}