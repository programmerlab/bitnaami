<?php
if ( ! defined ( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists ( 'YITH_COG_WPML_Module' ) ) {
	
	/**
	 * @class   YITH_COG_WPML_Module
	 */
	class YITH_COG_WPML_Module{

        /**
         * Single instance of the class
         */
        protected static $instance;

        /**
         * Shop's base currency. Used for caching.
         */
        protected static $base_currency;

        /**
         * Returns single instance of the class
         */
        public static function get_instance() {
            if ( is_null ( self::$instance ) ) {
                self::$instance = new self();
            }

            return self::$instance;
        }

        public function __construct(){

            add_filter('yith_cog_convert_amounts', array( $this, 'convert_to_base_currency' ), 10, 2 );

        }

        /**
         * Convenience method. Returns WooCommerce base currency.
         */
        public static function base_currency() {

            if ( empty( self::$base_currency ) ) {
                self::$base_currency = get_option ( 'woocommerce_currency' );
            }

            return self::$base_currency;
        }

        public function convert_to_base_currency( $amount, $item_id ) {
            global $woocommerce_wpml;

            $order_id = wc_get_order_id_by_order_item_id( $item_id );
            $order = wc_get_order( $order_id );

            $from_currency = apply_filters( 'yith_cog_wpml_from_currency', $order->get_currency() );
            $to_currency = apply_filters( 'yith_cog_wpml_to_currency', self::base_currency() );

            if (!is_numeric($amount)) {
                return $amount;
            }

            if ($amount == 0) {
                return $amount;
            }

            if ($from_currency == $to_currency) {
                return $amount;
            }

            $from_currency_rate = $woocommerce_wpml->settings['currency_options'][$from_currency]['rate'];
            $to_currency_rate = 1;
            
            $exchange_rate = $to_currency_rate / $from_currency_rate;

            return $amount * $exchange_rate;

        }

    }

}

YITH_COG_WPML_Module::get_instance ();