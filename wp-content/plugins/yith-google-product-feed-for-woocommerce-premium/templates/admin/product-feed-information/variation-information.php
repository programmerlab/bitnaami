<?php

/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

$function_product = YITH_Google_Product_Feed()->product_function;
$product = yit_get_prop($variation,'yith_wcgpf_product_feed_configuration',true);
$shipping = get_post_meta($variation->ID,'yith_wcgpf_shipping_feed_configuration',true);

?>

<div>
    <div>
        <h4><?php echo __( 'Google feed fields for variations', 'yith-google-product-feed-for-woocommerce' ); ?> </h4>
    </div>
    <div>
        <p style="font-weight: bold;"><?php _e('Basic product settings:','yith-google-product-feed-for-woocommerce')?></p>

        <div class="yith-wcgpf-product-feed-information">
            <label for="yith-wcgpf-brand-label"><?php _e('Brand:', 'yith-google-product-feed-for-woocommerce');?></label>
            <?php echo yith_wcgpf_get_input(array(
                'id' => 'yith-wcgpf-brand-product',
                'type' => 'text',
                'name' => 'yith-wcgpf-product-feed-configuration['.$variation->ID.'][brand]',
                'class' => 'yith-wcgpf-brand-product yith-wcgpf-information',
                'value' => isset($product['brand']) ? $product['brand'] : ''
            )); ?>
        </div>
        <div class="yith-wcgpf-product-feed-information">
            <label for="yith-wcgpf-gtin-label"><?php _e('Global Trade Item Number (GTIN):', 'yith-google-product-feed-for-woocommerce');?></label>
            <?php echo yith_wcgpf_get_input(array(
                'id' => 'yith-wcgpf-gtin-product',
                'type' => 'text',
                'name' => 'yith-wcgpf-product-feed-configuration['.$variation->ID.'][gtin]',
                'class' => 'yith-wcgpf-gtin-product yith-wcgpf-information',
                'value' => isset($product['gtin']) ? $product['gtin'] : ''
            )); ?>
        </div>
        <div class="yith-wcgpf-product-feed-information">
            <label for="yith-wcgpf-mpn-label"><?php _e('Manufacturer Part Number (MPN):', 'yith-google-product-feed-for-woocommerce');?></label>
            <?php echo yith_wcgpf_get_input(array(
                'id' => 'yith-wcgpf-mpn-product',
                'type' => 'text',
                'name' => 'yith-wcgpf-product-feed-configuration['.$variation->ID.'][mpn]',
                'class' => 'yith-wcgpf-mpn-product yith-wcgpf-information',
                'value' => isset($product['mpn']) ? $product['mpn'] : ''
            )); ?>
        </div>
        <div class="yith-wcgpf-product-feed-information">
            <label for="yith-wcgpf-condition-label"><?php _e('Condition:', 'yith-google-product-feed-for-woocommerce'); ?></label>
            <?php echo yith_wcgpf_get_dropdown(array(
                'name'      =>  'yith-wcgpf-product-feed-configuration['.$variation->ID.'][condition]',
                'id'        =>  'yith-wcgpf-condition-product',
                'class'     =>  'yith-wcgpf-condition-product yith-wcgpf-select yith-wcgpf-information',
                'options'   =>  $function_product->condition(),
                'value'     =>  isset($product['condition']) ? $product['condition'] : '',
            )); ?>
        </div>
        <div class=yith-wcgpf-product-feed-information>
            <label for="yith-wcgpf-google-category-label"><?php _e('Google Category:', 'yith-google-product-feed-for-woocommerce'); ?></label>
            <?php echo yith_wcgpf_get_dropdown(array(
                'name'      =>  'yith-wcgpf-product-feed-configuration['.$variation->ID.'][google_product_category]',
                'id'        =>  'yith-wcgpf-google-category',
                'class'     =>  'yith-wcgpf-google-category  yith-wcgpf-select yith-wcgpf-information',
                'options'   =>  $function_product->google_category('local'),
                'value'     =>  isset($product['google_product_category']) ? $product['google_product_category'] : '',
            )); ?>
        </div>

        <?php if( apply_filters('yith_wcgpf_show_product_information_on_product_page',true) ) { ?>

            <div class="yith-wcgpf-product-feed-information">
                <label for="yith-wcgpf-adult-label"><?php _e('Adult:', 'yith-google-product-feed-for-woocommerce'); ?></label>
                <?php echo yith_wcgpf_get_dropdown(array(
                    'name'      =>  'yith-wcgpf-product-feed-configuration['.$variation->ID.'][adult]',
                    'id'        =>  'yith-wcgpf-adult-product',
                    'class'     =>  'yith-wcgpf-adult-product yith-wcgpf-select yith-wcgpf-information',
                    'options'   =>  $function_product->adult(),
                    'value'     =>  isset($product['adult']) ? $product['adult'] : '',
                )); ?>
            </div>
            <div class="yith-wcgpf-product-feed-information">
                <label for="yith-wcgpf-energy-label"><?php _e('Energy efficiency class:', 'yith-google-product-feed-for-woocommerce'); ?></label>
                <?php echo yith_wcgpf_get_dropdown(array(
                    'name'      =>  'yith-wcgpf-product-feed-configuration['.$variation->ID.'][energy_efficiency_class]',
                    'id'        =>  'yith-wcgpf-adult-product',
                    'class'     =>  'yith-wcgpf-adult-product yith-wcgpf-select yith-wcgpf-information',
                    'options'   =>  $function_product->energy_efficiency(),
                    'value'     =>  isset($product['energy_efficiency_class']) ? $product['energy_efficiency_class'] : '',
                )); ?>
            </div>

        <?php } ?>

            <div class="yith-wcgpf-product-feed-information">
                <label for="yith-wcgpf-gender-label"><?php _e('Gender:', 'yith-google-product-feed-for-woocommerce'); ?></label>
                <?php echo yith_wcgpf_get_dropdown(array(
                    'name'      =>  'yith-wcgpf-product-feed-configuration['.$variation->ID.'][gender]',
                    'id'        =>  'yith-wcgpf-gender-product',
                    'class'     =>  'yith-wcgpf-gender-product yith-wcgpf-select yith-wcgpf-information',
                    'options'   =>  $function_product->gender(),
                    'value'     =>  isset($product['gender']) ? $product['gender'] : '',
                )); ?>
            </div>
            <div class="yith-wcgpf-product-feed-information">
                <label for="yith-wcgpf-age-group-label"><?php _e('Age group:', 'yith-google-product-feed-for-woocommerce'); ?></label>
                <?php echo yith_wcgpf_get_dropdown(array(
                    'name'      =>  'yith-wcgpf-product-feed-configuration['.$variation->ID.'][age_group]',
                    'id'        =>  'yith-wcgpf-age-group-product',
                    'class'     =>  'yith-wcgpf-age-group-product yith-wcgpf-select yith-wcgpf-information',
                    'options'   =>  $function_product->age_group(),
                    'value'     =>  isset($product['age_group']) ? $product['age_group'] : '',
                )); ?>
            </div>
            <div class="yith-wcgpf-product-feed-information">
                <label for="yith-wcgpf-material-label"><?php _e('Material:', 'yith-google-product-feed-for-woocommerce');?></label>
                <?php echo yith_wcgpf_get_input(array(
                    'id' => 'yith-wcgpf-material-product',
                    'type' => 'text',
                    'name' => 'yith-wcgpf-product-feed-configuration['.$variation->ID.'][material]',
                    'class' => 'yith-wcgpf-material-product  yith-wcgpf-information',
                    'value' => isset($product['material']) ? $product['material'] : ''
                )); ?>
            </div>
            <div class="yith-wcgpf-product-feed-information">
                <label for="yith-wcgpf-pattern-label"><?php _e('Pattern:', 'yith-google-product-feed-for-woocommerce');?></label>
                <?php echo yith_wcgpf_get_input(array(
                    'id' => 'yith-wcgpf-pattern-product',
                    'type' => 'text',
                    'name' => 'yith-wcgpf-product-feed-configuration['.$variation->ID.'][pattern]',
                    'class' => 'yith-wcgpf-pattern-product  yith-wcgpf-information',
                    'value' => isset($product['pattern']) ? $product['pattern'] : ''
                )); ?>
            </div>
            <div class="yith-wcgpf-product-feed-information">
                <label for="yith-wcgpf-size-label"><?php _e('Size:', 'yith-google-product-feed-for-woocommerce');?></label>
                <?php echo yith_wcgpf_get_input(array(
                    'id' => 'yith-wcgpf-size-product',
                    'type' => 'text',
                    'name' => 'yith-wcgpf-product-feed-configuration['.$variation->ID.'][size]',
                    'class' => 'yith-wcgpf-size-product  yith-wcgpf-information',
                    'value' => isset($product['size']) ? $product['size'] : ''
                )); ?>
            </div>

        <?php if( apply_filters('yith_wcgpf_show_product_information_on_product_page',true) ) { ?>

            <div class="yith-wcgpf-product-feed-information">
                <label for="yith-wcgpf-size-type-label"><?php _e('Size type:', 'yith-google-product-feed-for-woocommerce'); ?></label>
                <?php echo yith_wcgpf_get_dropdown(array(
                    'name'      =>  'yith-wcgpf-product-feed-configuration['.$variation->ID.'][size_type]',
                    'id'        =>  'yith-wcgpf-size-type-product',
                    'class'     =>  'yith-wcgpf-size-type-product yith-wcgpf-select yith-wcgpf-information',
                    'options'   =>  $function_product->size_type(),
                    'value'     =>  isset($product['size_type']) ? $product['size_type'] : '',
                )); ?>
            </div>
            <div class="yith-wcgpf-product-feed-information">
                <label for="yith-wcgpf-size-system-label"><?php _e('Size system:', 'yith-google-product-feed-for-woocommerce'); ?></label>
                <?php echo yith_wcgpf_get_dropdown(array(
                    'name'      =>  'yith-wcgpf-product-feed-configuration['.$variation->ID.'][size_system]',
                    'id'        =>  'yith-wcgpf-size-system-product',
                    'class'     =>  'yith-wcgpf-size-system-product yith-wcgpf-select yith-wcgpf-information',
                    'options'   =>  $function_product->size_system(),
                    'value'     =>  isset($product['size_system']) ? $product['size_system'] : '',
                )); ?>
            </div>

        <?php } ?>

    </div>
    <?php if (apply_filters( 'yith_wcgpf_show_shipping_information_on_product_page',true ) ) { ?>
        <div>
        <p style="font-weight: bold;"><?php _e('Shipping settings:','yith-google-product-feed-for-woocommerce')?></p>
        <div>
            <div class="yith-wcgpf-product-feed-information">
                <label for="yith-wcgpf-shiping-price"><?php _e('Shipping price', 'yith-google-product-feed-for-woocommerce');?></label>
                <?php echo yith_wcgpf_get_input(array(
                    'id' => 'yith-wcgpf-shipping-price-product',
                    'type' => 'text',
                    'name' => 'yith-wcgpf-shipping-feed-configuration['.$variation->ID.'][price]',
                    'class' => 'yith-wcgpf-shipping-price-product  yith-wcgpf-information',
                    'value' => isset($shipping['price']) ? $shipping['price'] : ''
                )); ?>
            </div>
            <div class="yith-wcgpf-product-feed-information">
                <label for="yith-wcgpf-shiping-country"><?php _e('Shipping country', 'yith-google-product-feed-for-woocommerce');?></label>
                <?php echo yith_wcgpf_get_input(array(
                    'id' => 'yith-wcgpf-shipping-country-product',
                    'type' => 'text',
                    'name' => 'yith-wcgpf-shippping-feed-configuration['.$variation->ID.'][country]',
                    'class' => 'yith-wcgpf-shipping-country-product  yith-wcgpf-information',
                    'value' => isset($shipping['country']) ? $shipping['country'] : ''
                )); ?>
            </div>
            <div class="yith-wcgpf-product-feed-information">
                <label for="yith-wcgpf-shiping-region"><?php _e('Shipping region', 'yith-google-product-feed-for-woocommerce');?></label>
                <?php echo yith_wcgpf_get_input(array(
                    'id' => 'yith-wcgpf-shipping-price-product',
                    'type' => 'text',
                    'name' => 'yith-wcgpf-shipping-feed-configuration['.$variation->ID.'][region]',
                    'class' => 'yith-wcgpf-shipping-region-product  yith-wcgpf-information',
                    'value' => isset($shipping['region']) ? $shipping['region'] : ''
                )); ?>
            </div>
            <div class="yith-wcgpf-product-feed-information">
                <label for="yith-wcgpf-shiping-service"><?php _e('Shipping service', 'yith-google-product-feed-for-woocommerce');?></label>
                <?php echo yith_wcgpf_get_input(array(
                    'id' => 'yith-wcgpf-shipping-price-product',
                    'type' => 'text',
                    'name' => 'yith-wcgpf-shipping-feed-configuration['.$variation->ID.'][service]',
                    'class' => 'yith-wcgpf-shipping-price-product  yith-wcgpf-information',
                    'value' => isset($shipping['service']) ? $shipping['service'] : ''
                )); ?>
            </div>
        </div>
        <div class="yith-wcgpf-product-feed-information">
            <label for="yith-wcgpf-shiping-label"><?php _e('Shipping label', 'yith-google-product-feed-for-woocommerce');?></label>
            <?php echo yith_wcgpf_get_input(array(
                'id' => 'yith-wcgpf-shipping-label-product',
                'type' => 'text',
                'name' => 'yith-wcgpf-product-feed-configuration['.$variation->ID.'][shipping_label]',
                'class' => 'yith-wcgpf-shipping-label-product  yith-wcgpf-information',
                'value' => isset($product['shipping_label']) ? $product['shipping_label'] : ''
            )); ?>
        </div>
        <div class="yith-wcgpf-product-feed-information">
            <label for="yith-wcgpf-shiping-weight"><?php _e('Shipping weight', 'yith-google-product-feed-for-woocommerce');?></label>
            <?php echo yith_wcgpf_get_input(array(
                'id' => 'yith-wcgpf-shipping-weight-product',
                'type' => 'text',
                'name' => 'yith-wcgpf-product-feed-configuration['.$variation->ID.'][shipping_weight]',
                'class' => 'yith-wcgpf-shipping-weight-product  yith-wcgpf-information',
                'value' => isset($product['shipping_weight']) ? $product['shipping_weight'] : ''
            )); ?>
        </div>
        <div class="yith-wcgpf-product-feed-information">
            <label for="yith-wcgpf-shiping-length"><?php _e('Shipping length', 'yith-google-product-feed-for-woocommerce');?></label>
            <?php echo yith_wcgpf_get_input(array(
                'id' => 'yith-wcgpf-shipping-length-product',
                'type' => 'text',
                'name' => 'yith-wcgpf-product-feed-configuration['.$variation->ID.'][shipping_length]',
                'class' => 'yith-wcgpf-shipping-length-product  yith-wcgpf-information',
                'value' => isset($product['shipping_length']) ? $product['shipping_length'] : ''
            )); ?>
        </div>
        <div class="yith-wcgpf-product-feed-information">
            <label for="yith-wcgpf-shiping-width"><?php _e('Shipping width', 'yith-google-product-feed-for-woocommerce');?></label>
            <?php echo yith_wcgpf_get_input(array(
                'id' => 'yith-wcgpf-shipping-width-product',
                'type' => 'text',
                'name' => 'yith-wcgpf-product-feed-configuration['.$variation->ID.'][shipping_width]',
                'class' => 'yith-wcgpf-shipping-width-product  yith-wcgpf-information',
                'value' => isset($product['shipping_width']) ? $product['shipping_width'] : ''
            )); ?>
        </div>
        <div class="yith-wcgpf-product-feed-information">
            <label for="yith-wcgpf-shiping-height"><?php _e('Shipping height', 'yith-google-product-feed-for-woocommerce');?></label>
            <?php echo yith_wcgpf_get_input(array(
                'id' => 'yith-wcgpf-shipping-height-product',
                'type' => 'text',
                'name' => 'yith-wcgpf-product-feed-configuration['.$variation->ID.'][shipping_height]',
                'class' => 'yith-wcgpf-shipping-height-product  yith-wcgpf-information',
                'value' => isset($product['shipping_height']) ? $product['shipping_height'] : ''
            )); ?>
        </div>
    </div>
    <?php } ?>

</div>