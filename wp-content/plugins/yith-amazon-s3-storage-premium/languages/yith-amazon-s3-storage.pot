# Copyright (C) 2018 YITH Amazon S3 Storage for WooCommerce Premium
# This file is distributed under the same license as the YITH Amazon S3 Storage for WooCommerce Premium package.
msgid ""
msgstr ""
"Project-Id-Version: YITH Amazon S3 Storage for WooCommerce Premium 1.0.11\n"
"Report-Msgid-Bugs-To: http://wordpress.org/tag/init\n"
"POT-Creation-Date: 2018-12-13 14:58:52+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2018-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"

#: includes/class.yith-wc-amazon-s3-storage-admin.php:264
#: templates/general_settings_tab.php:204
msgid "Copy files also to S3"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-admin.php:286
#: templates/woocommerce_settings_tab.php:95
msgid "Private "
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-admin.php:297
#: templates/woocommerce_settings_tab.php:104
msgid "Public "
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-admin.php:322
#: templates/general_settings_tab.php:250
msgid "Remove from the server"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-admin.php:331
msgid ""
"(after removing from the server the file is NOT going to be shown in the "
"media library)"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-admin.php:684
msgid "Amazon S3 Storage"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-admin.php:688
msgid "Connect to your S3 Amazon account"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-admin.php:693
msgid "Settings"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-admin.php:695
msgid "WooCommerce settings for digital goods"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-ajax-admin.php:357
msgid "Copied to S3"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-ajax-admin.php:369
msgid "Removed from S3"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-ajax-admin.php:380
msgid "Copied to server from S3"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-ajax-admin.php:392
msgid "Removed from server"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-ajax-admin.php:904
msgid "Connection to Amazon S3 Storage successful"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-ajax-admin.php:921
#: templates/connects3_tab.php:153 templates/connects3_tab.php:192
msgid ""
"An error occurred while accessing, the credentials (access key or secret "
"key) are NOT correct"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-aws-s3-client.php:148
msgid "Choose a bucket"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-aws-s3-client.php:186
msgid ""
"An error occurred while accessing, the credentials (access key and secret "
"key) are NOT correct"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-aws-s3-client.php:228
msgid "Private"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-aws-s3-client.php:232
msgid "Public"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-aws-s3-client.php:258
msgctxt "S3 File Manager"
msgid "Bucket"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-aws-s3-client.php:268
msgctxt "S3 File Manager"
msgid "Current folder"
msgstr ""

#: includes/class.yith-wc-amazon-s3-storage-aws-s3-client.php:536
msgid "The bucket couldn't be created"
msgstr ""

#: includes/functions.yith-wc-amazon-s3-storage.php:170
msgctxt "yith-amazon-s3-storage"
msgid "Copying to S3"
msgstr ""

#: includes/functions.yith-wc-amazon-s3-storage.php:176
msgctxt "Button action mode grid"
msgid "Removing from S3"
msgstr ""

#: includes/functions.yith-wc-amazon-s3-storage.php:182
msgctxt "Button action mode grid"
msgid "Copying to server from S3"
msgstr ""

#: includes/functions.yith-wc-amazon-s3-storage.php:188
msgctxt "Button action mode grid"
msgid "Removing from server"
msgstr ""

#: includes/functions.yith-wc-amazon-s3-storage.php:290
msgid ""
"You have to configure your Access Key and Secret Access Key correctly in the "
"\"Connect to your s3 amazon account\" tab"
msgstr ""

#: includes/functions.yith-wc-amazon-s3-storage.php:320
msgid ""
"You have to choose a bucket in the \"Setting\" tab in the Amazon S3 admin "
"panel"
msgstr ""

#: templates/connects3_tab.php:40 templates/general_settings_tab.php:75
#: templates/woocommerce_settings_tab.php:42
msgid "Settings saved."
msgstr ""

#: templates/connects3_tab.php:75
msgid "Access key"
msgstr ""

#: templates/connects3_tab.php:79
msgid "Set the access key"
msgstr ""

#: templates/connects3_tab.php:89
msgid "Secret access key"
msgstr ""

#: templates/connects3_tab.php:93
msgid "Set the secret access key"
msgstr ""

#: templates/connects3_tab.php:97
msgid "If you don't know where to search for your S3 credentials, "
msgstr ""

#: templates/connects3_tab.php:99
msgid "you can find them here"
msgstr ""

#: templates/connects3_tab.php:108 templates/general_settings_tab.php:262
#: templates/woocommerce_settings_tab.php:165
msgid "Save Changes"
msgstr ""

#: templates/connects3_tab.php:136 templates/connects3_tab.php:175
msgid "Connection to Amazon S3 Storage was successful"
msgstr ""

#: templates/general_settings_tab.php:113
msgid "Set a bucket name"
msgstr ""

#: templates/general_settings_tab.php:125
msgid "No Buckets Found!!"
msgstr ""

#: templates/general_settings_tab.php:182
msgid ""
"Select the S3 bucket name where to upload all the files. If you don't know "
"how to create a bucket, "
msgstr ""

#: templates/general_settings_tab.php:185
msgid "you can click here"
msgstr ""

#: templates/general_settings_tab.php:198
msgid "Copy file to S3"
msgstr ""

#: templates/general_settings_tab.php:209
msgid ""
"The files uploaded to the media library will be added automatically to S3. "
"The files that are already in the media library\n"
"                will NOT be uploaded to S3"
msgstr ""

#: templates/general_settings_tab.php:221
msgid "Replace URL"
msgstr ""

#: templates/general_settings_tab.php:227
msgid "Files will be served by S3"
msgstr ""

#: templates/general_settings_tab.php:232
msgid ""
"The URLs of the files will be automatically replaced to be downloaded from "
"S3 and not from your WordPress installation"
msgstr ""

#: templates/general_settings_tab.php:244
msgid "Remove from server"
msgstr ""

#: templates/general_settings_tab.php:255
msgid "The files uploaded to S3 amazon will be deleted from your server"
msgstr ""

#: templates/woocommerce_settings_tab.php:54
msgctxt "S3 File Manager"
msgid ""
"The above link is temporary, so in case it is not working you can click here "
"to go to your order under your account and download the file"
msgstr ""

#: templates/woocommerce_settings_tab.php:88
msgid "Permissions"
msgstr ""

#: templates/woocommerce_settings_tab.php:108
msgid ""
"By setting the files as public, anyone who knows the S3 URL will\n"
"\t\thave complete access to it"
msgstr ""

#: templates/woocommerce_settings_tab.php:117
msgid "URL validity period"
msgstr ""

#: templates/woocommerce_settings_tab.php:121
msgid "Set the time (minutes) during which the URL is valid for download"
msgstr ""

#: templates/woocommerce_settings_tab.php:132
msgid "Send order link"
msgstr ""

#: templates/woocommerce_settings_tab.php:138
msgid "link of the order under the user's account"
msgstr ""

#: templates/woocommerce_settings_tab.php:143
msgid ""
"This link shows up in the email sent after purchasing and leads to the order "
"under the account of the user. If the client does not have an account the "
"link will not be sent"
msgstr ""

#: templates/woocommerce_settings_tab.php:154
msgid "Link of the email sent"
msgstr ""

#: templates/woocommerce_settings_tab.php:158
msgid "Edit the text of the link."
msgstr ""

#. Plugin Name of the plugin/theme
msgid "YITH Amazon S3 Storage for WooCommerce Premium"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "https://yithemes.com/themes/plugins/yith-amazon-s3-storage/"
msgstr ""

#. Description of the plugin/theme
msgid ""
"<code><strong>YITH Amazon S3 Storage</strong></code> allows you to store all "
"your media library and your downloadable products for WooCommerce in Amazon "
"S3. <a href=\"https://yithemes.com/\" target=\"_blank\">Get more plugins for "
"your e-commerce shop on <strong>YITH</strong></a>."
msgstr ""

#. Author of the plugin/theme
msgid "YITH"
msgstr ""

#. Author URI of the plugin/theme
msgid "https://yithemes.com/"
msgstr ""
